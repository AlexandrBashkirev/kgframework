
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;

void main()
{ 
    
    vec4 textureColor = texture2D(CC_Texture0, v_texCoord);
    gl_FragColor = vec4(clamp(textureColor.r+v_fragmentColor.r,0.0,1.0), clamp(textureColor.g+v_fragmentColor.g,0.0,1.0), clamp(textureColor.b+v_fragmentColor.b,0.0,1.0), textureColor.w*v_fragmentColor.w);

}
