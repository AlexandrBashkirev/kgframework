//
//  kgSocial.cpp
//  PVO
//
//  Created by flaber on 12.04.14.
//
//

#include "kgSocial.h"
#include "platform/android/jni/JniHelper.h"
#include "kgUtils.h"

kgSocial::kgSocial()
{
    
}

void kgSocial::loginFB()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "loginFB", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}
void kgSocial::logoutFB()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "logoutFB", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}
void kgSocial::postFB(string text, string caption)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "postFB", "(Ljava/lang/String;Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(text.c_str());
        jstring stringArg2 = t.env->NewStringUTF(caption.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1, stringArg2);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
        t.env->DeleteLocalRef(stringArg2);
    }
}
bool kgSocial::isLoginFB()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "isLoginFB", "()Z"))
    {
        jboolean retV = t.env->CallStaticBooleanMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
        
        return (bool)retV;
    }
    
    return false;
}

void kgSocial::loginTW()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "loginTW", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}
void kgSocial::logoutTW()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "logoutTW", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}
void kgSocial::postTW(string text)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "postTW", "(Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(text.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
    }
}
bool kgSocial::isLoginTW()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "isLoginTW", "()Z"))
    {
        jboolean retV = t.env->CallStaticBooleanMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
        
        return (bool)retV;
    }
    
    return false;
}

void kgSocial::loginVK()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "loginVK", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}
void kgSocial::logoutVK()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "logoutVK", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}
void kgSocial::postVK(string text)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "postVK", "(Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(text.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
    }
}
bool kgSocial::isLoginVK()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKSocialConnector", "isLoginVK", "()Z"))
    {
        jboolean retV = t.env->CallStaticBooleanMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
        
        return (bool)retV;
    }
    
    return false;
}