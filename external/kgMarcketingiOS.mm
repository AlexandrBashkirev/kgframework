//
//  kgMarcketing.cpp
//  PVO
//
//  Created by flaber on 07.04.14.
//
//

#include "kgMarcketing.h"
#ifdef _GAITrackingId_
    #import "GAI.h"
    #import "GAIFields.h"
    #import "GAIDictionaryBuilder.h"
#endif
#import "iRate.h"
#ifdef _ChartboostAppId_
    #import "Chartboost.h"
#endif

#ifdef _GameAnalyticsGameKey_
    #import "GameAnalytics.h"
#endif

void kgMarcketing::didFinishLaunching()
{
    [iRate sharedInstance].applicationBundleID = [[NSBundle mainBundle] bundleIdentifier];
	[iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    
    //set events count (default is 10)
    [iRate sharedInstance].eventsUntilPrompt = 5;
    
    //disable minimum day limit and reminder periods
    [iRate sharedInstance].daysUntilPrompt = 0;
    [iRate sharedInstance].remindPeriod = 0;
    // Override point for customization after application launch.
    
    
#ifdef _GAITrackingId_
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    [[GAI sharedInstance] trackerWithTrackingId:_GAITrackingId_];
#endif
    
#ifdef _GameAnalyticsGameKey_
    [GameAnalytics setGameKey:_GameAnalyticsGameKey_ secretKey:_GameAnalyticsGameSecret_ build:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
#endif
}
void kgMarcketing::didBecomeActive()
{
#ifdef _GameAnalyticsGameKey_
    [GameAnalytics updateSessionID];
#endif
#ifdef _ChartboostAppId_
    Chartboost *cb = [Chartboost sharedChartboost];
    
    cb.appId = _ChartboostAppId_;
    cb.appSignature = _ChartboostAppSignature_;
    
    [cb startSession];
    [cb cacheInterstitial];
    [cb cacheInterstitial:@"GameOver"];
    [cb cacheInterstitial:@"StartApp"];
    
    [cb cacheMoreApps];
#endif
}

void kgMarcketing::logEvent(string cutegory, string event)
{
#ifdef _GAITrackingId_
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:[NSString stringWithCString:cutegory.c_str() encoding:[NSString defaultCStringEncoding]]
                                                                                        action:[NSString stringWithCString:event.c_str() encoding:[NSString defaultCStringEncoding]]
                                                                                         label:nil
                                                                                         value:nil] build]];
#endif
    
#ifdef _GameAnalyticsGameKey_
    [GameAnalytics logGameDesignDataEvent:[NSString stringWithCString:event.c_str() encoding:[NSString defaultCStringEncoding]]];
#endif
    
}
void kgMarcketing::logEvent(string cutegory, string event, string label, int value)
{
#ifdef _GAITrackingId_
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:[NSString stringWithCString:cutegory.c_str() encoding:[NSString defaultCStringEncoding]]
                                                                                        action:[NSString stringWithCString:event.c_str() encoding:[NSString defaultCStringEncoding]]
                                                                                         label:[NSString stringWithCString:label.c_str() encoding:[NSString defaultCStringEncoding]]
                                                                                         value:[NSNumber numberWithInt:value]] build]];
#endif
    
#ifdef _GameAnalyticsGameKey_
    [GameAnalytics logGameDesignDataEvent:[NSString stringWithCString:event.c_str() encoding:[NSString defaultCStringEncoding]] value:[NSNumber numberWithInt:value]];
#endif
}
void kgMarcketing::logViewShowed(string viewName)
{
#ifdef _GAITrackingId_
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker set:kGAIScreenName value:[NSString stringWithCString:viewName.c_str() encoding:[NSString defaultCStringEncoding]]];
    
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
#endif
    NSLog(@"logViewShowed %s", viewName.c_str());
}
void kgMarcketing::logPurchase(string pruductId, string category, string transactionId, string currencyCode, float cost)
{
#ifdef _GAITrackingId_
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createTransactionWithId:[NSString stringWithCString:transactionId.c_str() encoding:[NSString defaultCStringEncoding]]
                                                     affiliation:@"In-app Store"
                                                         revenue:[NSNumber numberWithFloat:cost*0.7f]
                                                             tax:[NSNumber numberWithFloat:cost*0.3f]
                                                        shipping:@0
                                                    currencyCode:[NSString stringWithCString:currencyCode.c_str() encoding:[NSString defaultCStringEncoding]]] build]];
    
    
    [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId:[NSString stringWithCString:transactionId.c_str() encoding:[NSString defaultCStringEncoding]]
                                                                name:[NSString stringWithCString:pruductId.c_str() encoding:[NSString defaultCStringEncoding]]
                                                                 sku:[NSString stringWithCString:pruductId.c_str() encoding:[NSString defaultCStringEncoding]]
                                                            category:[NSString stringWithCString:category.c_str() encoding:[NSString defaultCStringEncoding]]
                                                               price:[NSNumber numberWithFloat:cost]
                                                            quantity:@1
                                                        currencyCode:[NSString stringWithCString:currencyCode.c_str() encoding:[NSString defaultCStringEncoding]]] build]];
    
#endif

#ifdef _GameAnalyticsGameKey_
    [GameAnalytics logBusinessDataEvent:[NSString stringWithCString:pruductId.c_str() encoding:[NSString defaultCStringEncoding]] currencyString:[NSString stringWithCString:currencyCode.c_str() encoding:[NSString defaultCStringEncoding]] amountNumber:[NSNumber numberWithFloat:cost]];
#endif
}
void kgMarcketing::showAds(string adsName)
{
#ifdef _ChartboostAppId_
    [[Chartboost sharedChartboost] showInterstitial:[NSString stringWithCString:adsName.c_str() encoding:[NSString defaultCStringEncoding]]];
#endif
}
void kgMarcketing::showMoreGames()
{
#ifdef _ChartboostAppId_
    [[Chartboost sharedChartboost] showMoreApps];
#endif
}