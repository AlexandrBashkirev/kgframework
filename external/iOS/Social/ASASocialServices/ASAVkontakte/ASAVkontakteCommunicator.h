//
//  ASAVkontakteCommunicator.h
//
//  Created by Andrew Shmig on 18.12.12.
//

#import <Foundation/Foundation.h>

@class ASAVkontakteUserAccount;



@interface ASAVkontakteCommunicator : NSObject <UIWebViewDelegate>

- (id)initWithWebView:(UIWebView *)webView;

- (void)startOnCancelBlock:(void (^)(void))cancelBlock
            onSuccessBlock:(void (^)(ASAVkontakteUserAccount *))acceptedBlock;

@end
