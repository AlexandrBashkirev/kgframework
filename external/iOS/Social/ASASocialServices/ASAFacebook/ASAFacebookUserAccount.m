//
//  ASAFacebookUserAccount.m
//
//  Created by AndrewShmig on 14.12.12.
//

#import "ASAFacebookUserAccount.h"
#import "ASAFacebookCommunicator.h"
#import "NSString+encodeURL.h"

#define LOG_ON 1

#if LOG_ON == 1
# define DEBUG_CURRENT_METHOD() NSLog(@"%s", __FUNCTION__)
#endif

@implementation ASAFacebookUserAccount
{
    NSString *_accessToken;
    NSUInteger _expirationTime;
}

@synthesize accessToken = _accessToken;
@synthesize expirationTime = _expirationTime;

#pragma mark - Init methods

- (void)save
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:_accessToken forKey:@"fbAccessToken"];
    [defaults setInteger:_expirationTime forKey:@"fbExpirationTime"];
    
    [defaults synchronize];
}
- (void)logout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults removeObjectForKey:@"fbAccessToken"];
    [defaults removeObjectForKey:@"fbExpirationTime"];
    
    [defaults synchronize];
    

    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* vkCookies1 = [cookies cookiesForURL:
                           [NSURL URLWithString:kFACEBOOK_GRAPH_URL]];
    NSArray* vkCookies2 = [cookies cookiesForURL:
                           [NSURL URLWithString:kFACEBOOK_USER_URL]];
    NSArray* vkCookies3 = [cookies cookiesForURL:
                           [NSURL URLWithString:kFACEBOOK_USER_FRIENDS_URL]];
    NSArray* vkCookies4 = [cookies cookiesForURL:
                           [NSURL URLWithString:kFACEBOOK_USER_INBOX_URL]];
    NSArray* vkCookies5 = [cookies cookiesForURL:
                           [NSURL URLWithString:kFACEBOOK_USER_OUTBOX_URL]];
    NSArray* vkCookies6 = [cookies cookiesForURL:
                           [NSURL URLWithString:kFACEBOOK_USER_PHOTO_URL]];
    NSArray* vkCookies7 = [cookies cookiesForURL:
                           [NSURL URLWithString:kFACEBOOK_USER_FEED_URL]];
    
    for (NSHTTPCookie* cookie in vkCookies1)
    {
        [cookies deleteCookie:cookie];
    }
    for (NSHTTPCookie* cookie in vkCookies2)
    {
        [cookies deleteCookie:cookie];
    }
    for (NSHTTPCookie* cookie in vkCookies3)
    {
        [cookies deleteCookie:cookie];
    }
    for (NSHTTPCookie* cookie in vkCookies4)
    {
        [cookies deleteCookie:cookie];
    }
    for (NSHTTPCookie* cookie in vkCookies5)
    {
        [cookies deleteCookie:cookie];
    }
    for (NSHTTPCookie* cookie in vkCookies6)
    {
        [cookies deleteCookie:cookie];
    }
    for (NSHTTPCookie* cookie in vkCookies7)
    {
        [cookies deleteCookie:cookie];
    }
}
+ (ASAFacebookUserAccount*)loadAccaunt
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* at = [defaults stringForKey:@"fbAccessToken"];
    int et = [defaults integerForKey:@"fbExpirationTime"];
    
    if([at isEqualToString:@""] || at == nil)
        return nil;
    
    ASAFacebookUserAccount* ac = [[ASAFacebookUserAccount alloc] initWithAccessToken:at expirationTime:et];
    
    [ac setSuccessBlock:^(NSDictionary *dictionary)
     {
         NSLog(@"fb obtainInfo response: %@", dictionary);
     }];
    [ac setErrorBlock:^(NSError *error) {
        NSLog(@"fb obtainInfo Error: %@", error);
    }];
    
    [ac obtainInfo];
    return ac;
}
- (id)initWithAccessToken:(NSString *)accessToken
           expirationTime:(NSUInteger)expirationTime
{
    DEBUG_CURRENT_METHOD();

    self = [super init];

    if (self) {

        if(accessToken == nil)
            @throw [NSException exceptionWithName:@"Access token can not be nil."
                                           reason:@"accessToken is nil."
                                         userInfo:@{}];

        _accessToken = [accessToken copy];
        _expirationTime = expirationTime;
    }

    return self;
}

- (id)initWithAccessToken:(NSString *)accessToken
{
    return [self initWithAccessToken:accessToken
                      expirationTime:0];
}


#pragma mark - Public methods

- (void)obtainInfo
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *personalInfo = [[NSString stringWithFormat:kFACEBOOK_USER_URL]
                                               mutableCopy];
    [personalInfo appendFormat:@"?access_token=%@", _accessToken];
    NSURL *url = [NSURL URLWithString:personalInfo];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}

- (void)obtainInfoFields:(NSArray *)fields
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *personalInfo = [[NSString stringWithFormat:kFACEBOOK_USER_URL]
                                               mutableCopy];
    [personalInfo appendFormat:@"?access_token=%@", _accessToken];

    if ([fields count] != 0) {
        [personalInfo appendString:@"&fields="];

        for (NSString *field in fields) {
            [personalInfo appendFormat:@"%@,", field];
        }

        NSUInteger location = [personalInfo length] - 1;
        NSUInteger length = 1;
        NSRange range = NSMakeRange(location, length);
        [personalInfo deleteCharactersInRange:range]; // remove last unneeded ',' character
    }

    NSURL *url = [NSURL URLWithString:personalInfo];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}

- (void)obtainPhoto
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [[NSString stringWithFormat:kFACEBOOK_USER_PHOTO_URL]
                                               mutableCopy];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];
    [urlAsString appendFormat:@"&redirect=false"];
    NSURL *url = [NSURL URLWithString:urlAsString];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}

- (void)publishToFeed:(NSString *)text
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_USER_FEED_URL];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];
    [urlAsString appendFormat:@"&message=%@", [text encodeURL]];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];

    _successBlock([self sendRequest:request]);
}

- (void)publishToFeedCustomOptions:(NSDictionary *)options
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_USER_FEED_URL];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];

    [options enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
    {
        [urlAsString appendFormat:@"&%@=%@", key, [obj encodeURL]];
    }];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];

    _successBlock([self sendRequest:request]);
}


- (void)obtainFriends
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_USER_FRIENDS_URL];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}

- (void)obtainFriendsCustomFields:(NSArray *)fields
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_USER_FRIENDS_URL];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];

    if([fields count] != 0) {
        [urlAsString appendString:@"&fields="];

        for(NSString *field in fields) {
            [urlAsString appendFormat:@"%@,", field];
        }

        NSUInteger location = [urlAsString length] - 1;
        NSUInteger length = 1;
        NSRange range = NSMakeRange(location, length);
        [urlAsString deleteCharactersInRange:range]; // remove last unneeded ',' character
    }

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}

- (void)obtainDialogWithId:(NSString *)dialogId
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_GRAPH_URL];
    [urlAsString appendFormat:@"%@", dialogId];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}

- (void)obtainDialogCommentsWithId:(NSString *)dialogId
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_GRAPH_URL];
    [urlAsString appendFormat:@"%@", dialogId];
    [urlAsString appendFormat:@"/comments"];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}

- (void)obtainDialogCommentsWithId:(NSString *)dialogId count:(NSUInteger)count
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_GRAPH_URL];
    [urlAsString appendFormat:@"%@", dialogId];
    [urlAsString appendFormat:@"/comments"];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];
    [urlAsString appendFormat:@"&limit=%u", count];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}

- (void)obtainDialogCommentsWithId:(NSString *)dialogId
                             count:(NSUInteger)count
                            offset:(NSUInteger)offset
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_GRAPH_URL];
    [urlAsString appendFormat:@"%@", dialogId];
    [urlAsString appendFormat:@"/comments"];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];
    [urlAsString appendFormat:@"&limit=%u", count];
    [urlAsString appendFormat:@"&offset=%u", offset];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}


- (void)obtainInboxDialogs
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_USER_INBOX_URL];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}

- (void)obtainInboxDialogsCount:(NSUInteger)count
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_USER_INBOX_URL];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];
    [urlAsString appendFormat:@"&limit=%u", count];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}


- (void)obtainOutboxDialogs
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_USER_OUTBOX_URL];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}

- (void)obtainOutboxDialogsCount:(NSUInteger)count
{
    DEBUG_CURRENT_METHOD();

    NSMutableString *urlAsString = [NSMutableString string];
    [urlAsString appendFormat:@"%@", kFACEBOOK_USER_OUTBOX_URL];
    [urlAsString appendFormat:@"?access_token=%@", _accessToken];
    [urlAsString appendFormat:@"&limit=%u", count];

    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];

    _successBlock([self sendRequest:request]);
}


- (NSString *)description
{
    DEBUG_CURRENT_METHOD();

    return [NSString stringWithFormat:@"Access token: %@, expires in: %u",
                                      _accessToken,
                                      _expirationTime];
}

#pragma mark - Private methods

- (NSDictionary *)sendRequest:(NSMutableURLRequest *)request
{
    DEBUG_CURRENT_METHOD();

    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];

    NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
    
    if ([responseData bytes] == nil) {
        NSLog(@"fb error responseData == nil error code = %d", statusCode);
        return nil;
    }
    NSLog(@"===>%@", [NSString stringWithCString:[responseData bytes]
                                        encoding:NSUTF8StringEncoding]);

    if(error != nil) {
        _errorBlock(error);
        return nil;
    }

    
    if(statusCode != 200) {
        error = [NSError errorWithDomain:@"DPFacebookUserAccountDomain"
                                    code:-1
                                userInfo:@{@"Status code"          : [NSString stringWithFormat:@"%d",
                                                                                                statusCode],
                                           @"Server response body" : [NSString stringWithCString:[responseData bytes]
                                                                                        encoding:NSUTF8StringEncoding]}];

        _errorBlock(error);
        return nil;
    }

    error = nil;
    NSDictionary *responseAsJSON = [NSJSONSerialization JSONObjectWithData:responseData
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&error];

    if(error != nil) {
        _errorBlock(error);
        return nil;
    }

    return responseAsJSON;
}

@end
