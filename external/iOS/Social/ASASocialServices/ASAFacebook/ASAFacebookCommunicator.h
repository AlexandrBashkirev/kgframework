//
//  ASAFacebookCommunicator.h
//
//  Created by AndrewShmig on 14.12.12.
//

#import <Foundation/Foundation.h>



@class ASAFacebookUserAccount;


@interface ASAFacebookCommunicator : NSObject <UIWebViewDelegate>

- (id)initWithWebView:(UIWebView *)webView;

- (void)startOnCancelBlock:(void (^)(void))cancelBlock
              onErrorBlock:(void (^)(NSError *))errorBlock
            onSuccessBlock:(void (^)(ASAFacebookUserAccount *))acceptedBlock;

@end
