//
//  GameCenterView.h
//  PVO
//
//  Created by flaber on 12.04.13.
//  Copyright (c) 2012 kerGamesStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>

@interface GameCenterView : UIViewController<GKLeaderboardViewControllerDelegate, GKAchievementViewControllerDelegate>
{
}

+ (void) showLeaderboard:(NSString*)category;
+ (void) showAchivments;

@end
