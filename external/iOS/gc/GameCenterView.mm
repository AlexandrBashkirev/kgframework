//
//  GameCenterView.m
//  PVO
//
//  Created by flaber on 12.04.13.
//  Copyright (c) 2012 kerGamesStudio. All rights reserved.
//

#import "GameCenterView.h"

@implementation GameCenterView

+ (void) showLeaderboard:(NSString*)category
{
    GKLeaderboardViewController* leaderboardController = [[[GKLeaderboardViewController alloc] init] autorelease] ;
    
	if (leaderboardController != NULL)
	{
        GameCenterView* gameCenterView = [[GameCenterView alloc] init];
        
        
		leaderboardController.category = [NSString stringWithFormat:@"%@.%@", [[NSBundle mainBundle] bundleIdentifier], category ];
		leaderboardController.timeScope = GKLeaderboardTimeScopeAllTime;
		leaderboardController.leaderboardDelegate = gameCenterView;
        
        UIViewController* vc = [UIApplication sharedApplication].keyWindow.rootViewController;
        [vc presentViewController: leaderboardController animated: YES completion:nil];
	}
}
+ (void) showAchivments
{
    GKAchievementViewController *achievements = [[[GKAchievementViewController alloc] init] autorelease];
    if (achievements != NULL)
    {
        GameCenterView* gameCenterView = [[GameCenterView alloc] init];
        achievements.achievementDelegate = gameCenterView;
        UIViewController* vc = [UIApplication sharedApplication].keyWindow.rootViewController;
        [vc presentViewController: achievements animated: YES completion:nil];
    }
}


-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
    [viewController dismissViewControllerAnimated: YES completion:nil];
}
- (void)achievementViewControllerDidFinish:(GKAchievementViewController *)viewController;
{
    [viewController dismissViewControllerAnimated: YES completion:nil];
    //[viewController release];
    [self release];
}
- (void)dealloc
{
    [super dealloc];
}


@end
