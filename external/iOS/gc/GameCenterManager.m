//
//  GameCenterManager.m
//  PVO
//
//  Created by flaber on 12.04.13.
//  Copyright (c) 2012 kerGamesStudio. All rights reserved.
//

#import "GameCenterManager.h"


@implementation GameCenterManager


@synthesize earnedAchievementCache;
@synthesize delegate;

static GameCenterManager* _inst = nil;
+ (GameCenterManager*) instance
{
    if(_inst == nil)
        _inst = [[GameCenterManager alloc] init];
    
    return _inst;
}
- (id) init
{
	self = [super init];
	if(self!= NULL)
	{
		earnedAchievementCache= NULL;
		delegate = NULL;
        
        [self authenticateLocalUser];
	}
	return self;
}

- (void) dealloc
{
	self.delegate = NULL;
	self.earnedAchievementCache= NULL;
	[super dealloc];
}

- (void) callDelegate: (SEL) selector withArg: (id) arg error: (NSError*) err
{
	assert([NSThread isMainThread]);
	if([delegate respondsToSelector: selector])
	{
		if(arg != NULL)
		{
			[delegate performSelector: selector withObject: arg withObject: err];
            NSLog(@"GC: arg");
		}
		else
		{
			[delegate performSelector: selector withObject: err];
            NSLog(@"GC: arg = nil");
		}
	}
	else
	{
		NSLog(@"GC: Missed Method");
	}
}


- (void) callDelegateOnMainThread: (SEL) selector withArg: (id) arg error: (NSError*) err
{
	dispatch_async(dispatch_get_main_queue(), ^(void)
                   {
                       [self callDelegate: selector withArg: arg error: err];
                       NSLog(@"GC: dispatch_async");
                   });
}

+ (BOOL) isGameCenterAvailable
{
	// check for presence of GKLocalPlayer API
	Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
    
	// check if the device is running iOS 4.1 or later
	NSString *reqSysVer = @"4.1";
	NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
	BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
	
	return (gcClass && osVersionSupported);
}


- (void) authenticateLocalUser
{
	if([GKLocalPlayer localPlayer].authenticated == NO)
	{
        if ([[[UIDevice currentDevice] systemVersion] intValue] >= 6.0f) {

			GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
			localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error)
            {
                if (viewController != nil)
                {
                    [self performSelector:@selector(presentAuthentication:) withObject:viewController afterDelay:0.1];
                }
                else if (localPlayer.isAuthenticated)
                {
                    
                }
                else if (error)
                {
                    if([[error domain] isEqualToString:GKErrorDomain] && [error code] == GKErrorCancelled)
                        NSLog(@"GameCenter Disabled - Sign in with Game Center application to enable");
                    else
                        NSLog(@"GC authenticate error %d, %@, %@", error.code, error.localizedDescription, error.localizedFailureReason);
				}
			};
		}
	}
}
-(void) presentAuthentication:(UIViewController*)vc
{
    UIViewController* rootvc = [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootvc presentViewController:vc animated: YES completion:nil];
}
- (void) reloadHighScoresForCategory: (NSString*) _category
{
    NSString* category = [NSString stringWithFormat:@"%@.%@", [[NSBundle mainBundle] bundleIdentifier], _category ];
    
	GKLeaderboard* leaderBoard= [[[GKLeaderboard alloc] init] autorelease];
	leaderBoard.category= category;
	leaderBoard.timeScope= GKLeaderboardTimeScopeAllTime;
	leaderBoard.range= NSMakeRange(1, 1);
    
	[leaderBoard loadScoresWithCompletionHandler:  ^(NSArray *scores, NSError *error)
     {
         [self callDelegateOnMainThread: @selector(reloadScoresComplete:error:) withArg: leaderBoard error: error];
     }];
}

- (void) reportScore: (int64_t) score forCategory: (NSString*) _category
{
    NSString* category = [NSString stringWithFormat:@"%@.%@", [[NSBundle mainBundle] bundleIdentifier], _category ];
    
	GKScore *scoreReporter = [[[GKScore alloc] initWithCategory:category] autorelease];
	scoreReporter.value = score;
	[scoreReporter reportScoreWithCompletionHandler: ^(NSError *error)
	 {
         if (error != nil)
         {
             NSLog(@"GC: Failed:");
             
         } else {
             NSLog(@"GC: Succes:");
             NSLog(@"GC: error=%@",[error localizedDescription]);
             
             [self callDelegateOnMainThread: @selector(scoreReported:) withArg: NULL error: error];
         }
	 }];
}

- (void) retrieveAchievmentMetadata: (NSString*)_identifier
{
    //NSBundle* mainBundle = [NSBundle mainBundle];
    NSString* identifier = [NSString stringWithFormat:@"%@.%@", [[NSBundle mainBundle] bundleIdentifier], _identifier ];
    
    [GKAchievementDescription loadAchievementDescriptionsWithCompletionHandler: ^(NSArray *descriptions, NSError *error)
     {
         NSEnumerator *iter = [descriptions objectEnumerator];
         GKAchievementDescription *description = nil;
         
         while ((description = [iter nextObject]))
         {
             if ([description.identifier isEqualToString: identifier])
             {
                 //   [[GKAchievementHandler defaultHandler] notifyAchievement: description];
             }
         }
     }];
}

- (void) submitAchievement: (NSString*) _identifier percentComplete: (double) percentComplete
{
    NSString* identifier = [NSString stringWithFormat:@"%@.%@", [[NSBundle mainBundle] bundleIdentifier], _identifier ];
    
	if(self.earnedAchievementCache == nil)
	{
		[GKAchievement loadAchievementsWithCompletionHandler: ^(NSArray *scores, NSError *error)
         {
             if(error == nil)
             {
                 NSMutableDictionary* tempCache= [NSMutableDictionary dictionaryWithCapacity: [scores count]];
                 for (GKAchievement* score in scores)
                 {
                     [tempCache setObject: score forKey: score.identifier];
                 }
                 self.earnedAchievementCache = tempCache;
                 [self submitAchievement: identifier percentComplete: percentComplete];
             }
             else
             {
                 NSLog(@"GC submitAchievement: error=%@",[error localizedDescription]);
                 
                 //Something broke loading the achievement list.  Error out, and we'll try again the next time achievements submit.
                 [self callDelegateOnMainThread: @selector(achievementSubmitted:error:) withArg: NULL error: error];
             }
             
         }];
	}
	else
	{
        //Search the list for the ID we're using...
		GKAchievement* achievement = [self.earnedAchievementCache objectForKey: identifier];
        
		if(achievement != nil)
		{
			if(achievement.percentComplete >= 100.0)
				achievement= nil;//Achievement has already been earned so we're done.
			else
                achievement.percentComplete = achievement.percentComplete + percentComplete;
		}
		else
		{
			achievement= [[[GKAchievement alloc] initWithIdentifier: identifier] autorelease];
			achievement.percentComplete = percentComplete;
			//Add achievement to achievement cache...
			[self.earnedAchievementCache setObject: achievement forKey: achievement.identifier];
		}
		if(achievement!= nil)
		{
            achievement.showsCompletionBanner = true;
			//Submit the Achievement...
            if ([[[UIDevice currentDevice] systemVersion] intValue] >= 7.0f)
                [GKNotificationBanner showBannerWithTitle:@"Achievement" message:@"Completed!" completionHandler:^{}];
            
			[achievement reportAchievementWithCompletionHandler: ^(NSError *error)
             {
                 if(error != nil)
                     NSLog(@"GC submitAchievement: error=%@",[error localizedDescription]);
                 [self callDelegateOnMainThread: @selector(achievementSubmitted:error:) withArg: achievement error: error];
                 [self retrieveAchievmentMetadata: identifier];
             }];
		}
	}
}

- (void) resetAchievements
{
	self.earnedAchievementCache= nil;
	[GKAchievement resetAchievementsWithCompletionHandler: ^(NSError *error)
     {
		 [self callDelegateOnMainThread: @selector(achievementResetResult:) withArg: NULL error: error];
     }];
}

- (void) mapPlayerIDtoPlayer: (NSString*) playerID
{
	[GKPlayer loadPlayersForIdentifiers: [NSArray arrayWithObject: playerID] withCompletionHandler:^(NSArray *playerArray, NSError *error)
     {
         GKPlayer* player= nil;
         for (GKPlayer* tempPlayer in playerArray)
         {
             if([tempPlayer.playerID isEqualToString: playerID])
             {
                 player= tempPlayer;
                 break;
             }
         }
         [self callDelegateOnMainThread: @selector(mappedPlayerIDToPlayer:error:) withArg: player error: error];
     }];
	
}

@end
 
