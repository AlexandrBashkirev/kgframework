//
//  InAppPurchaseManager.m
//  PVO
//
//  Created by flaber on 12.04.13.
//  Copyright (c) 2012 kerGamesStudio. All rights reserved.
//

#import "InAppPurchaseManager.h"
#include "JSON.h"
#import "Base64.h"
#include "kgPurchase.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Random)

+ (NSString *)randomStringWithLength:(NSInteger)length
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
    
    for (int i = 0; i < length; i++) {
        [randomString appendFormat:@"%C", [letters characterAtIndex:arc4random() % [letters length]]];
    }
    
    return randomString;
}

@end

@implementation InAppPurchaseManager

static InAppPurchaseManager *instance_;

static void singleton_remover() {
    [instance_ release];
}

+ (InAppPurchaseManager*)instance
{
    @synchronized(self)
    {
        if( instance_ == nil )
        {
            [[self alloc] init];
        }
    }
    
    return instance_;
}

- (id)init
{
    if( (self=[super init]))
    {
        instance_ = self;
        products = nil;
        itemsIDs = nil;
        
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
        products = [NSMutableDictionary dictionary];
        [products retain];
        
        [self setItemsIdForValidation];
        
        atexit(singleton_remover);
    }
    
    return self;
}

- (void)dealloc
{
    [products release];
    [super dealloc];
}
- (void) requestProductsData:(NSArray*) _itemsId
{
    NSMutableArray* t = [NSMutableArray array];
    for (int i = 0; i < [_itemsId count]; i++)
    {
        NSString* identifer = [NSString stringWithFormat:@"%@.%@", [[NSBundle mainBundle] bundleIdentifier],[_itemsId objectAtIndex:i]];
        if([products objectForKey:identifer] == nil)
            [t addObject:identifer];
    }
    if([t count] > 0)
    {
        productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers: [NSSet setWithArray:t]];
        productsRequest.delegate = self;
        [productsRequest start];
        
        if (pPurchaseDel != nullptr)
            pPurchaseDel->showWaitOverlay();

        NSLog(@"requestProductData");
    }
    else
    {
        if (pPurchaseDel != nullptr)
        {
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            
            vector<kgProduct> cProdacts;
            for (NSString* prId in products)
            {
                SKProduct* pr = [products objectForKey:prId];
                [numberFormatter setLocale:pr.priceLocale];
                cProdacts.push_back(kgProduct(string([pr.productIdentifier UTF8String]), string([[numberFormatter stringFromNumber:pr.price] UTF8String]), string([(NSString*)CFLocaleGetValue((CFLocaleRef)pr.priceLocale, kCFLocaleCurrencyCode) UTF8String]), pr.price.doubleValue));
            }
            
            pPurchaseDel->purchacesInfoGated(cProdacts);
        }
    }
}
- (NSMutableDictionary*) getProducts
{
    return products;
}
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSLog(@"requestProductData didReceiveResponse");
    
    NSArray *_products = response.products;
    if([_products count] > 0)
    {
        for (SKProduct* pr in _products)
        {
            [products setObject:pr forKey:pr.productIdentifier];
        }
        
        if (pPurchaseDel != nullptr)
        {
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            
            vector<kgProduct> cProdacts;
            for (NSString* prId in products)
            {
                SKProduct* pr = [products objectForKey:prId];
                [numberFormatter setLocale:pr.priceLocale];
                cProdacts.push_back(kgProduct(string([pr.productIdentifier UTF8String]), string([[numberFormatter stringFromNumber:pr.price] UTF8String]), string([(NSString*)CFLocaleGetValue((CFLocaleRef)pr.priceLocale, kCFLocaleCurrencyCode) UTF8String]), pr.price.doubleValue));
            }
            
            pPurchaseDel->purchacesInfoGated(cProdacts);
        }
    }
    else
    {
        if (pPurchaseDel != nullptr)
            pPurchaseDel->purchacesInfoError();
    }
    
    for (NSString *invalidProductId in response.invalidProductIdentifiers)
        NSLog(@"Invalid product id: %@" , invalidProductId);
    
    [request release];
    
    if (pPurchaseDel != nullptr)
        pPurchaseDel->hideWaitOverlay();
}

- (void)requestDidFinish:(SKRequest *)request
{
    NSLog(@"requestDidFinish");
}
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError %@", error.localizedDescription);
    if (pPurchaseDel != nullptr)
        pPurchaseDel->purchacesInfoError();
    
    if (pPurchaseDel != nullptr)
        pPurchaseDel->hideWaitOverlay();
}


/////////////// Purchace
- (void)requestPurchace:(NSString*) _itemId
{
    if (pPurchaseDel != nullptr)
        pPurchaseDel->showWaitOverlay();
    
    if ([SKPaymentQueue canMakePayments] )
    {
        SKProduct* pr = [products objectForKey:[NSString stringWithFormat:@"%@.%@", [[NSBundle mainBundle] bundleIdentifier], _itemId]];
        if(pr == nil)
        {
            if (pPurchaseDel != nullptr)
                pPurchaseDel->purchaseFailed(std::string([@"can't make purchase" UTF8String]) );
            if (pPurchaseDel != nullptr)
                pPurchaseDel->hideWaitOverlay();
            NSLog(@"can't find purchase %@", _itemId);
            return;
        }
        SKPayment *payment = [SKPayment paymentWithProduct:pr];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    else
    {
        if (pPurchaseDel != nullptr)
            pPurchaseDel->purchaseFailed(std::string([@"can't make purchase" UTF8String]) );
        if (pPurchaseDel != nullptr)
            pPurchaseDel->hideWaitOverlay();
    }
}

- (void)triggerPaymentEvent:(SKPaymentTransaction *)transaction
{
}

- (void)recordTransaction: (SKPaymentTransaction *)transaction
{
    
}

- (void)completeTransaction: (SKPaymentTransaction *)transaction
{
    NSLog(@"completeTransaction");
    
    NSString *receiptStr = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
    
    if(transaction.originalTransaction == nil)
    {
        if ([self verifyReceipt:transaction.transactionReceipt productIdentifier:transaction.payment.productIdentifier])
        {
            if (pPurchaseDel != nullptr)
                pPurchaseDel->purchaseCompleted(std::string([transaction.payment.productIdentifier UTF8String]), std::string([receiptStr UTF8String]));
            
            [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
            
            if (pPurchaseDel != nullptr)
                pPurchaseDel->hideWaitOverlay();
        }else
        {
            [self failedTransaction:transaction];
        }
    }
    else
    {
        if ([self verifyReceipt:transaction.transactionReceipt productIdentifier:transaction.originalTransaction.payment.productIdentifier])
        {
            if (pPurchaseDel != nullptr)
                pPurchaseDel->restoreCompleted(std::string([transaction.originalTransaction.payment.productIdentifier UTF8String]), std::string([receiptStr UTF8String]));
            
            [[SKPaymentQueue defaultQueue] finishTransaction: transaction];

            if (pPurchaseDel != nullptr)
                pPurchaseDel->hideWaitOverlay();
        }
        else
        {
            [self failedTransaction:transaction];
        }
    }
}

- (void)restoreTransaction: (SKPaymentTransaction *)transaction
{
    NSLog(@"restoreTransaction");
    
    NSString *receiptStr = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
    
    if ([self verifyReceipt:transaction.transactionReceipt productIdentifier:transaction.originalTransaction.payment.productIdentifier])
    {
        if (pPurchaseDel != nullptr)
            pPurchaseDel->restoreCompleted(std::string([transaction.originalTransaction.payment.productIdentifier UTF8String]), std::string([receiptStr UTF8String]));
        
        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
        
        if (pPurchaseDel != nullptr)
            pPurchaseDel->hideWaitOverlay();
    }
    else
    {
        [self failedTransaction:transaction];
    }
}

- (void)failedTransaction: (SKPaymentTransaction *)transaction
{
    NSLog(@"failedTransaction error = %@, %ld", transaction.error.localizedDescription, (long)transaction.error.code);
    
    if (pPurchaseDel != nullptr)
    {
        if(transaction.error != nil)
            pPurchaseDel->purchaseFailed(std::string([transaction.error.localizedDescription UTF8String]));
        else
            pPurchaseDel->purchaseFailed(std::string("vertyfiError"));
    }

    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    if (pPurchaseDel != nullptr)
        pPurchaseDel->hideWaitOverlay();
}

// Sent when the transaction array has changed (additions or state changes).  Client should check state of transactions and finish as appropriate.
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    NSLog(@"updatedTransactions");
    for (SKPaymentTransaction *transaction in transactions)
    {
        [self triggerPaymentEvent:transaction];
        
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            default:
                //[self hideLoading];
                break;
        }
    }
}

// Sent when transactions are removed from the queue (via finishTransaction:).
- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions
{
    NSLog(@"removedTransactions");
}


///////// restore

- (void)restoreCompletedTransactions
{
    NSLog(@"restoreCompletedTransactions");
    if ([SKPaymentQueue canMakePayments])
    {
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
        if (pPurchaseDel != nullptr)
            pPurchaseDel->showWaitOverlay();
    }
}
// Sent when an error is encountered while adding transactions from the user's purchase history back to the queue.
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    if (pPurchaseDel != nullptr)
        pPurchaseDel->closeTransaction();
    
    NSLog(@"restoreCompletedTransactionsFailedWithError");
    for (SKPaymentTransaction *transaction in queue.transactions)
    {
        [self failedTransaction:transaction];
    }
    if (pPurchaseDel != nullptr)
        pPurchaseDel->hideWaitOverlay();
}

// Sent when all transactions from the user's purchase history have successfully been added back to the queue.
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    if (pPurchaseDel != nullptr)
        pPurchaseDel->closeTransaction();
    if (pPurchaseDel != nullptr)
        pPurchaseDel->hideWaitOverlay();
    NSLog(@"paymentQueueRestoreCompletedTransactionsFinished");
}
+ (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (unsigned int)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}
- (BOOL)verifyReceipt:(NSData*)receiptData productIdentifier:(NSString*)productIdentifier
{
    NSString* s = [[NSString alloc] initWithData:receiptData encoding:NSUTF8StringEncoding];
    NSString *urlsting = @"";
    
    if ([s rangeOfString:@"Sandbox"].location == NSNotFound)
    {
        urlsting = @"https://buy.itunes.apple.com/verifyReceipt";
    }
    else {
        urlsting = @"https://sandbox.itunes.apple.com/verifyReceipt";
    }
    
    NSURL *url = [NSURL URLWithString:urlsting];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *st =  [receiptData base64EncodedString];
    NSString *json = [NSString stringWithFormat:@"{\"receipt-data\":\"%@\"}", st];
    
    [theRequest setHTTPBody:[json dataUsingEncoding:NSUTF8StringEncoding]];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSString *length = [NSString stringWithFormat:@"%lu", (unsigned long)[json length]];
    [theRequest setValue:length forHTTPHeaderField:@"Content-Length"];
    
    NSHTTPURLResponse* urlResponse = nil;
    NSError *error = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&urlResponse error:&error];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    SBJSON *jsonParser = [SBJSON new];
    NSDictionary* dic = [jsonParser objectWithString:responseString];
    
    NSInteger status = [[dic objectForKey:@"status"] intValue];
    NSDictionary *receiptDic = [dic objectForKey:@"receipt"];
    BOOL retVal = NO;
    
    if (status == 0 && receiptDic)
    {
        NSString *itemId = [receiptDic objectForKey:@"item_id"];
        NSString *productId = [receiptDic objectForKey:@"product_id"];
        
        if ([productId isEqualToString:productIdentifier] && [itemsIDs containsObject:itemId])
        {
            NSString* sicretKey = @"l>(Vi8t6+(L;ofT>98qM8}R(aF2g4od9cjoNXsq7552p47-m8VOhS5p1w]";
            NSString* purchase_GUID = [receiptDic objectForKey:@"transaction_id"];
            NSString* random_seed = [NSString randomStringWithLength:10];
            NSString* key = [InAppPurchaseManager md5:[NSString stringWithFormat:@"%@%@%@", purchase_GUID, random_seed, sicretKey]];
            
            theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://kergamestudio.ru/purchaseChecker/service.php"]];
            [theRequest setHTTPMethod:@"POST"];
            [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            
            
            NSString *post = [NSString stringWithFormat:@"action=check_purchase_GUID&purchase_GUID=%@&random_seed=%@&key=%@", purchase_GUID,random_seed, key];
            
            [theRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[post length]] forHTTPHeaderField:@"Content-Length"];
            [theRequest setHTTPBody:[post dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            NSHTTPURLResponse* urlResponse = nil;
            NSError *error = nil;
            NSData *responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&urlResponse error:&error];
            NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];

            
            SBJSON *jsonParser = [SBJSON new];
            NSDictionary* dic = [jsonParser objectWithString:responseString];
            
            
            if([[dic objectForKey:@"error"] isEqualToString:@""])
            {
                dic = [dic objectForKey:@"response"];
                sicretKey = @"Nz2vai1CN0G0s48R3K1763eI88>98qM8}R6(1E2PkJwRX]";
                key = [InAppPurchaseManager md5:[NSString stringWithFormat:@"%@%@%@", [dic objectForKey:@"result"], [dic objectForKey:@"rand"], sicretKey]];
                if([[dic objectForKey:@"result"] isEqualToString:@"true"] && [[dic objectForKey:@"key"] isEqualToString:key])
                    retVal = YES;
            }
            
            
            if(!retVal)
            {
                if (pPurchaseDel != nullptr)
                    pPurchaseDel->catchTryHack("dont vertify on out server " + std::string([productIdentifier UTF8String]));
                
                return NO;
            }
        }
    }
    //
    if (!retVal) {
        if (pPurchaseDel != nullptr)
            pPurchaseDel->catchTryHack(std::string([productIdentifier UTF8String]));
    }
    return retVal;
}

- (void) setItemsIdForValidation
{
    NSArray* _itemsId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"purchasesIds"];
    if(itemsIDs != nil)
        [itemsIDs release];
    itemsIDs = [NSSet setWithArray:_itemsId];
    [itemsIDs retain];
}
@end
