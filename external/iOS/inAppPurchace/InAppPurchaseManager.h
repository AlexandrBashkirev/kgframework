//
//  InAppPurchaseManager.h
//  PVO
//
//  Created by flaber on 12.04.13.
//  Copyright (c) 2012 kerGamesStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@interface InAppPurchaseManager : NSObject <SKPaymentTransactionObserver, SKProductsRequestDelegate, SKRequestDelegate>
{
    SKProduct *proUpgradeProduct;
    SKProductsRequest *productsRequest;
    
    NSMutableDictionary* products;
    NSSet* itemsIDs;
}

+(InAppPurchaseManager*) instance;

- (void) requestPurchace:(NSString*) _itemId;
- (void) requestProductsData:(NSArray*) _itemsId;
- (void) restoreCompletedTransactions;

- (NSMutableDictionary*) getProducts;


@end
