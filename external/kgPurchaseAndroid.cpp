//
//  kgPurchase.cpp
//  PVO
//
//  Created by flaber on 03.04.14.
//
//

#include "kgPurchase.h"
#include "platform/android/jni/JniHelper.h"
#include "kgUtils.h"
#include "JsonBox.h"

void kgPurchase::requestPurchace(string prodactIdentifer)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKPurchaseConnector", "requestPurchace", "(Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(prodactIdentifer.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
    }
}
void kgPurchase::requestProductsData(vector<string> prodactIdentifers)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKPurchaseConnector", "requestProductsData", "(Ljava/lang/String;)V"))
    {
        JsonBox::Array a;
        for (int i = 0; i < prodactIdentifers.size(); i++)
            a.push_back(JsonBox::Value(prodactIdentifers[i].c_str()));
        
        stringstream ss;
        ss << a;
        
        jstring stringArg1 = t.env->NewStringUTF(ss.str().c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
    }
}

void kgPurchase::restoreCompletedTransactions()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKPurchaseConnector", "restoreCompletedTransactions", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}
kgProduct kgPurchase::getProdact(string prodactIdentifer)
{
    return kgProduct("","","", 0);
}