//
//  kgMarcketing.h
//  PVO
//
//  Created by flaber on 07.04.14.
//
//

#ifndef __PVO__kgMarcketing__
#define __PVO__kgMarcketing__

#include <string>
#include <vector>
#include <map>

using namespace std;

class kgMarcketing
{
    
public:
    
    static kgMarcketing& instance()
	{
		static kgMarcketing obj;
		return obj;
	}
    
    void didFinishLaunching();
    void didBecomeActive();
    
    void logEvent(string cutegory, string event);
    void logEvent(string cutegory, string event, string label, int value);
    void logViewShowed(string viewName);
    void logPurchase(string pruductId, string category, string transactionId, string currencyCode, float cost);
    
    void showMoreGames();
    void showAds(string adsName);
};

#define pMarcketing kgMarcketing::instance()

#endif /* defined(__PVO__kgMarcketing__) */
