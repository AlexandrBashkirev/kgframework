//
//  kgMarcketing.cpp
//  PVO
//
//  Created by flaber on 07.04.14.
//
//

#include "kgMarcketing.h"
#include "platform/android/jni/JniHelper.h"
#include "kgUtils.h"

void kgMarcketing::didFinishLaunching()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKMarcketingConnector", "didFinishLaunching", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}
void kgMarcketing::didBecomeActive()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKMarcketingConnector", "didBecomeActive", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

void kgMarcketing::logEvent(string cutegory, string event)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKMarcketingConnector", "logEvent", "(Ljava/lang/String;Ljava/lang/String;)V"))
    {
        
        jstring stringArg1 = t.env->NewStringUTF(cutegory.c_str());
        jstring stringArg2 = t.env->NewStringUTF(event.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1, stringArg1);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
        t.env->DeleteLocalRef(stringArg2);
    }
}
void kgMarcketing::logEvent(string cutegory, string event, string label, int value)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKMarcketingConnector", "logEvent", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(cutegory.c_str());
        jstring stringArg2 = t.env->NewStringUTF(event.c_str());
        jstring stringArg3 = t.env->NewStringUTF(label.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1, stringArg2, stringArg3, value);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
        t.env->DeleteLocalRef(stringArg2);
        t.env->DeleteLocalRef(stringArg3);
    }
}
void kgMarcketing::logViewShowed(string viewName)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKMarcketingConnector", "logViewShowed", "(Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(viewName.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
    }
}
void kgMarcketing::logPurchase(string pruductId, string category, string transactionId, string currencyCode, float cost)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKMarcketingConnector", "logPurchase", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(pruductId.c_str());
        jstring stringArg2 = t.env->NewStringUTF(category.c_str());
        jstring stringArg3 = t.env->NewStringUTF(transactionId.c_str());
        jstring stringArg4 = t.env->NewStringUTF(currencyCode.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1, stringArg2, stringArg3, stringArg4, cost);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
        t.env->DeleteLocalRef(stringArg2);
        t.env->DeleteLocalRef(stringArg3);
        t.env->DeleteLocalRef(stringArg4);
    }
}
void kgMarcketing::showAds(string adsName)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKMarcketingConnector", "showAds", "(Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(adsName.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
    }
}
void kgMarcketing::showMoreGames()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKMarcketingConnector", "showMoreGames", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}