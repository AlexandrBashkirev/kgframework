//
//  kgGaming.cpp
//  PVO
//
//  Created by flaber on 07.04.14.
//
//

#include <jni.h>
#include "com_flaber_kgNDKPurchaseConnector.h"
#include "kgUtils.h"
#include "JsonBox.h"
#include "kgPurchase.h"


JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_purchaseCompleted(
                                                                                JNIEnv *env,
                                                                                jobject obj,
                                                                                jstring productIdentifier,
                                                                                jstring receipt)
{
    if(pPurchaseDel != nullptr)
    {
        const char *nativeStringPI = env->GetStringUTFChars( productIdentifier, 0);
        string productIdentifierC = nativeStringPI;
        env->ReleaseStringUTFChars(productIdentifier, nativeStringPI);
        
        const char *nativeStringR = env->GetStringUTFChars( receipt, 0);
        string receiptC = nativeStringR;
        env->ReleaseStringUTFChars(receipt, nativeStringR);
        
        log("Java_com_flaber_kgNDKPurchaseConnector_purchaseCompleted %s, %s", productIdentifierC.c_str(), receiptC.c_str());
        pPurchaseDel->purchaseCompleted(productIdentifierC, receiptC);
    }
}

JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_restoreCompleted(
                                                                               JNIEnv *env,
                                                                               jobject obj,
                                                                               jstring productIdentifier,
                                                                               jstring receipt)
{
    if(pPurchaseDel != nullptr)
    {
        const char *nativeStringPI = env->GetStringUTFChars( productIdentifier, 0);
        string productIdentifierC = nativeStringPI;
        env->ReleaseStringUTFChars(productIdentifier, nativeStringPI);
        
        const char *nativeStringR = env->GetStringUTFChars( receipt, 0);
        string receiptC = nativeStringR;
        env->ReleaseStringUTFChars(receipt, nativeStringR);
        
        log("Java_com_flaber_kgNDKPurchaseConnector_restoreCompleted %s, %s", productIdentifierC.c_str(), receiptC.c_str());
        pPurchaseDel->restoreCompleted(productIdentifierC, receiptC);
    }
}

JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_purchaseFailed(
                                                                             JNIEnv *env,
                                                                             jobject obj,
                                                                             jstring error)
{
    if(pPurchaseDel != nullptr)
    {
        const char *nativeString = env->GetStringUTFChars( error, 0);
        string str = nativeString;
        env->ReleaseStringUTFChars(error, nativeString);
        
        log("Java_com_flaber_kgNDKPurchaseConnector_purchaseFailed %s", str.c_str());
        pPurchaseDel->purchaseFailed(str);
    }
}

JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_purchacesInfoGated(
                                                        JNIEnv *env,
                                                        jobject obj,
                                                        jstring products)
{
    if(pPurchaseDel != nullptr)
    {
        const char *nativeString = env->GetStringUTFChars( products, 0);
        string str = nativeString;
        env->ReleaseStringUTFChars(products, nativeString);
        
        log("Java_com_flaber_kgNDKPurchaseConnector_purchacesInfoGated %s", str.c_str());
        
        JsonBox::Value v;
        v.loadFromString(str);
        JsonBox::Array a = v.getArray();
        
        vector<kgProduct> productsV;
        JsonBox::Array::iterator it = a.begin();
        while (it != a.end())
        {
            JsonBox::Value _v;
            _v.loadFromString((*it).getString());
            
            JsonBox::Object _o = _v.getObject();
            
            productsV.push_back(kgProduct(_o["productId"].getString(), _o["price"].getString(), _o["price_currency_code"].getString(), 33));
            it++;
        }
        pPurchaseDel->purchacesInfoGated(productsV);
    }
}

JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_purchacesInfoError(
                                                        JNIEnv *env,
                                                        jobject obj )
{
    log("Java_com_flaber_kgNDKPurchaseConnector_purchacesInfoError");
    if(pPurchaseDel != nullptr)
        pPurchaseDel->purchacesInfoError();
}

JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_closeTransaction(
                                                        JNIEnv *env,
                                                        jobject obj )
{
    log("Java_com_flaber_kgNDKPurchaseConnector_closeTransaction");
    if(pPurchaseDel != nullptr)
        pPurchaseDel->closeTransaction();
}


JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_showWaitOverlay(
                                                        JNIEnv *env,
                                                        jobject obj )
{
    log("Java_com_flaber_kgNDKPurchaseConnector_showWaitOverlay");
    if(pPurchaseDel != nullptr)
        pPurchaseDel->showWaitOverlay();
}

JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_hideWaitOverlay(
                                                        JNIEnv *env,
                                                        jobject obj )
{
    log("Java_com_flaber_kgNDKPurchaseConnector_hideWaitOverlay");
    if(pPurchaseDel != nullptr)
        pPurchaseDel->hideWaitOverlay();
}

JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_catchTryHack(
                                                                           JNIEnv *env,
                                                                           jobject obj,
                                                                           jstring productIdentifier )
{
    if(pPurchaseDel != nullptr)
    {
        const char *nativeString = env->GetStringUTFChars( productIdentifier, 0);
        string str = nativeString;
        env->ReleaseStringUTFChars(productIdentifier, nativeString);
        
        log("Java_com_flaber_kgNDKPurchaseConnector_catchTryHack %s", str.c_str());
        pPurchaseDel->catchTryHack(str);
    }
}
