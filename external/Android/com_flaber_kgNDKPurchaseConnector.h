//
//  kgGaming.h
//  PVO
//
//  Created by flaber on 07.04.14.
//
//

#ifndef __PVO__com_flaber_kgNDKPurchaseConnector__
#define __PVO__com_flaber_kgNDKPurchaseConnector__

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_purchaseCompleted( JNIEnv *env, jobject obj, jstring productIdentifier, jstring receipt);
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_restoreCompleted( JNIEnv *env, jobject obj, jstring productIdentifier, jstring receipt);
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_purchaseFailed( JNIEnv *env, jobject obj, jstring error);
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_purchacesInfoGated( JNIEnv *env, jobject obj, jstring products);
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_purchacesInfoError( JNIEnv *env, jobject obj );
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_closeTransaction( JNIEnv *env, jobject obj );
    
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_showWaitOverlay( JNIEnv *env, jobject obj );
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_hideWaitOverlay( JNIEnv *env, jobject obj );
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKPurchaseConnector_catchTryHack( JNIEnv *env, jobject obj, jstring productIdentifier );
    
#ifdef __cplusplus
}
#endif

#endif /* defined(__PVO__com_flaber_kgNDKPurchaseConnector__) */
