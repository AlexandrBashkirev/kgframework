//
//  kgGaming.h
//  PVO
//
//  Created by flaber on 07.04.14.
//
//

#ifndef __PVO__com_flaber_kgNDKSocialConnector__
#define __PVO__com_flaber_kgNDKSocialConnector__

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postFBComplited( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postFBError( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginFBComplited( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginFBError( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_logoutFBComplited( JNIEnv *env, jobject obj );
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postTWComplited( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postTWError( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginTWComplited( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginTWError( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_logoutTWComplited( JNIEnv *env, jobject obj );
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postVKComplited( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postVKError( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginVKComplited( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginVKError( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_logoutVKComplited( JNIEnv *env, jobject obj );
    
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_showLoading( JNIEnv *env, jobject obj );
    JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_hideLoading( JNIEnv *env, jobject obj );
    
#ifdef __cplusplus
}
#endif

#endif /* defined(__PVO__com_flaber_kgNDKSocialConnector__) */
