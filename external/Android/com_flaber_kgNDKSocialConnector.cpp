//
//  kgGaming.cpp
//  PVO
//
//  Created by flaber on 07.04.14.
//
//

#include <jni.h>
#include "com_flaber_kgNDKSocialConnector.h"
#include "kgUtils.h"
#include "JsonBox.h"
#include "kgSocial.h"


JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postFBComplited( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_postFBComplited");
    if(pSocialDel != nullptr)
        pSocialDel->postFBComplited();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postFBError( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_postFBError");
    if(pSocialDel != nullptr)
        pSocialDel->postFBError();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginFBComplited( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_loginFBComplited");
    if(pSocialDel != nullptr)
        pSocialDel->loginFBComplited();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginFBError( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_loginFBError");
    if(pSocialDel != nullptr)
        pSocialDel->loginFBError();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_logoutFBComplited( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_logoutFBComplited");
    if(pSocialDel != nullptr)
        pSocialDel->logoutFBComplited();
}

JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postTWComplited( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_postTWComplited");
    if(pSocialDel != nullptr)
        pSocialDel->postTWComplited();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postTWError( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_postTWError");
    if(pSocialDel != nullptr)
        pSocialDel->postTWError();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginTWComplited( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_loginTWComplited");
    if(pSocialDel != nullptr)
        pSocialDel->loginTWComplited();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginTWError( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_loginTWError");
    if(pSocialDel != nullptr)
        pSocialDel->loginTWError();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_logoutTWComplited( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_logoutTWComplited");
    if(pSocialDel != nullptr)
        pSocialDel->logoutTWComplited();
}

JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postVKComplited( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_postVKComplited");
    if(pSocialDel != nullptr)
        pSocialDel->postVKComplited();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_postVKError( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_postVKError");
    if(pSocialDel != nullptr)
        pSocialDel->postVKError();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginVKComplited( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_loginVKComplited");
    if(pSocialDel != nullptr)
        pSocialDel->loginVKComplited();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_loginVKError( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_loginVKError");
    if(pSocialDel != nullptr)
        pSocialDel->loginVKError();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_logoutVKComplited( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_logoutVKComplited");
    if(pSocialDel != nullptr)
        pSocialDel->logoutVKComplited();
}

JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_showLoading( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_showLoading");
    if(pSocialDel != nullptr)
        pSocialDel->showLoading();
}
JNIEXPORT void JNICALL Java_com_flaber_kgNDKSocialConnector_hideLoading( JNIEnv *env, jobject obj )
{
    log("Java_com_flaber_kgNDKSocialConnector_hideLoading");
    if(pSocialDel != nullptr)
        pSocialDel->hideLoading();
}

