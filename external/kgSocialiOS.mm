//
//  kgSocial.cpp
//  PVO
//
//  Created by flaber on 12.04.14.
//
//

#include "kgSocial.h"

#import "ASAVkontakteCommunicator.h"
#import "ASAVkontakteUserAccount.h"
#import "ASAVkontakteMethods.h"

#import "ASAFacebookCommunicator.h"
#import "ASAFacebookUserAccount.h"

#import "ASATwitterCommunicator.h"
#import "ASATwitterUserAccount.h"
#import "ASATwitterMethods.h"

#import "KGModal.h"

@interface KGModalPbjDel : NSObject <KGModalDelegate>
-(void)KGModalHide;
@end

@implementation KGModalPbjDel

-(void)KGModalHide
{
    
}

@end

kgSocial::kgSocial()
{
    
}

UIWebView* createWebView()
{
    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.origin.x = frame.size.width*0.1f;
    frame.origin.y = frame.size.height*0.1f;
    frame.size.width = frame.size.width*0.8f;
    frame.size.height = frame.size.height*0.8f;
    
    UIWebView* _webView = [[UIWebView alloc] initWithFrame:frame];
    [_webView setContentScaleFactor:0.8f];
    [_webView setHidden:NO];
    
    [[KGModal sharedInstance] showWithContentView:_webView andAnimated:YES];
    [_webView release];
    return _webView;
}
void kgSocial::loginFB()
{
    ASAFacebookCommunicator* _fb = [[ASAFacebookCommunicator alloc] initWithWebView:createWebView()];
    
    [_fb startOnCancelBlock:^{
            NSLog(@"fb Cancel");
            
            [[KGModal sharedInstance] hideAnimated:true withCompletionBlock:nil];
     
            if(pSocial.getDelegate() != nullptr)
                pSocial.getDelegate()->loginFBError();
        } onErrorBlock:^(NSError *error) {
     
            NSLog(@"fb Error: %@", error);
            [[KGModal sharedInstance] hideAnimated:true withCompletionBlock:nil];
     
            if(pSocial.getDelegate() != nullptr)
                pSocial.getDelegate()->loginFBError();
        } onSuccessBlock:^(ASAFacebookUserAccount *account) {
            NSLog(@"fb account:%@", account);

            [account setSuccessBlock:^(NSDictionary *dictionary) { NSLog(@"response: %@", dictionary); }];
            [account obtainInfo];
            [account save];
     
            [[KGModal sharedInstance] hideAnimated:true withCompletionBlock:nil];
     
            if(pSocial.getDelegate() != nullptr)
                pSocial.getDelegate()->loginFBComplited();
        }
     ];
}
void kgSocial::logoutFB()
{
    ASAFacebookUserAccount* fbAccount = [ASAFacebookUserAccount loadAccaunt];
    if (fbAccount != nil)
    {
        [fbAccount logout];
        [fbAccount release];
        fbAccount = nil;
        
        if(pSocial.getDelegate() != nullptr)
            pSocial.getDelegate()->logoutFBComplited();
    }
}
void kgSocial::postFB(string text, string caption)
{
    if(pSocial.getDelegate() != nullptr)
        pSocial.getDelegate()->showLoading();
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ASAFacebookUserAccount* fbAccount = [ASAFacebookUserAccount loadAccaunt];
        
        [fbAccount setSuccessBlock:^(NSDictionary *dictionary)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
            
                if(pSocial.getDelegate() != nullptr)
                    pSocial.getDelegate()->hideLoading();
            
                if(pSocial.getDelegate() != nullptr)
                    pSocial.getDelegate()->postFBComplited();
            
                NSLog(@"fb post response: %@", dictionary);
            });
        }];
        [fbAccount setErrorBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{

                if(pSocial.getDelegate() != nullptr)
                    pSocial.getDelegate()->hideLoading();
            
                if(pSocial.getDelegate() != nullptr)
                    pSocial.getDelegate()->postFBError();

            });
        }];

        [fbAccount publishToFeedCustomOptions:@{    @"message"     : [NSString stringWithCString:text.c_str()
                                                                                        encoding:NSUTF8StringEncoding],
                                                    @"picture"     : LOGO_LINK,
                                                    @"link"        : PAGE_LINK,
                                                    @"name"        : APP_NAME,
                                                    @"caption"     : @"",
                                                    @"application" : APP_NAME,
                                                    @"description" : [NSString stringWithCString:caption.c_str()
                                                                                        encoding:NSUTF8StringEncoding]
        }];
    });
}
bool kgSocial::isLoginFB()
{
    if([ASAFacebookUserAccount loadAccaunt] == nil)
        return false;
    else
        return true;
}

void kgSocial::loginTW()
{
    ASATwitterCommunicator* _tw = [[ASATwitterCommunicator alloc] initWithWebView:createWebView()];
    
    [_tw startOnCancelBlock:^{
            [[KGModal sharedInstance] hideAnimated:true withCompletionBlock:nil];
            NSLog(@"tw User canceled app authorization...");
     
            if(pSocial.getDelegate() != nullptr)
                pSocial.getDelegate()->loginTWError();
        } onErrorBlock:^(NSError *error) {

            [[KGModal sharedInstance] hideAnimated:true withCompletionBlock:nil];
            NSLog(@"tw error during app authorization...%@", error);
            if(pSocial.getDelegate() != nullptr)
                pSocial.getDelegate()->loginTWError();
        } onSuccessBlock:^(ASATwitterUserAccount *account) {
            NSLog(@"tw Account: %@", account);

            [account save];
            [[KGModal sharedInstance] hideAnimated:true withCompletionBlock:nil];
     
            if(pSocial.getDelegate() != nullptr)
                pSocial.getDelegate()->loginTWComplited();
    }];
}
void kgSocial::logoutTW()
{
    ASATwitterUserAccount* twAccount = [ASATwitterUserAccount loadAccaunt];
    if (twAccount != nil)
    {
        [twAccount logout];
        [twAccount release];
        twAccount = nil;
        
        if(pSocial.getDelegate() != nullptr)
            pSocial.getDelegate()->logoutTWComplited();
    }
}
void kgSocial::postTW(string text)
{
    if(pSocial.getDelegate() != nullptr)
        pSocial.getDelegate()->showLoading();
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ASATwitterUserAccount* twAccount = [ASATwitterUserAccount loadAccaunt];
        
        [twAccount performTwitterMethod:kTWITTER_STATUSES_UPDATE_URL
                             HTTPMethod:@"POST"
                                options:@{@"status": [NSString stringWithCString:text.c_str()
                                                                        encoding:NSUTF8StringEncoding]}
                                success:^(id response) {
         
            dispatch_sync(dispatch_get_main_queue(), ^{
            
                if(pSocial.getDelegate() != nullptr)
                    pSocial.getDelegate()->hideLoading();
            
                if(pSocial.getDelegate() != nullptr)
                    pSocial.getDelegate()->postTWComplited();
            });
         
        } failure:^(NSError *error) {
         
            dispatch_sync(dispatch_get_main_queue(), ^{
            
                if(pSocial.getDelegate() != nullptr)
                    pSocial.getDelegate()->hideLoading();
            
                if(pSocial.getDelegate() != nullptr)
                    pSocial.getDelegate()->postTWError();
            });
         
        }];
    });
}
bool kgSocial::isLoginTW()
{
    if([ASATwitterUserAccount loadAccaunt] == nil)
        return false;
    else
        return true;
}

void kgSocial::loginVK()
{
    ASAVkontakteCommunicator* _vk = [[ASAVkontakteCommunicator alloc] initWithWebView:createWebView()];
    
    [_vk startOnCancelBlock:^{

            [[KGModal sharedInstance] hideAnimated:true withCompletionBlock:nil];
            NSLog(@"vk Cancel");
     
            if(pSocial.getDelegate() != nullptr)
                pSocial.getDelegate()->loginVKError();
        } onSuccessBlock:^(ASAVkontakteUserAccount *account){
            NSLog(@"%@", account);

            [account save];
            [[KGModal sharedInstance] hideAnimated:true withCompletionBlock:nil];
     
            if(pSocial.getDelegate() != nullptr)
                pSocial.getDelegate()->loginVKComplited();
    }];
}
void kgSocial::logoutVK()
{
    ASAVkontakteUserAccount* vkAccount = [ASAVkontakteUserAccount loadAccaunt];
    if (vkAccount != nil)
    {
        [vkAccount logout];
        [vkAccount release];
        vkAccount = nil;
        
        if(pSocial.getDelegate() != nullptr)
            pSocial.getDelegate()->logoutVKComplited();
    }
}
void kgSocial::postVK(string text)
{
    if(pSocial.getDelegate() != nullptr)
        pSocial.getDelegate()->showLoading();
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ASAVkontakteUserAccount* vkAccount = [ASAVkontakteUserAccount loadAccaunt];
        [vkAccount performVKMethod:kVKWallPost
                           options:@{   @"owner_id"   : @(vkAccount.userId),
                                        @"message"    : [NSString stringWithCString:text.c_str()
                                                                           encoding:NSUTF8StringEncoding],
                                        @"attachments" : PAGE_LINK
                                    }
                           success:^(NSDictionary *dictionary)
                                    {
                                        NSLog(@"Server response: %@", dictionary);
         
                                        if(pSocial.getDelegate() != nullptr)
                                            pSocial.getDelegate()->hideLoading();
         
                                        if(pSocial.getDelegate() != nullptr)
                                            pSocial.getDelegate()->postVKComplited();
                                    }
                           failure:^(NSError *error)
                                    {
                                        NSNumber* errorCode = [error.userInfo objectForKey:@"Error code"];
                                        if(errorCode.intValue == 5)
                                            pSocial.loginVK();
                                        else
                                        {
                                            if(pSocial.getDelegate() != nullptr)
                                                pSocial.getDelegate()->hideLoading();
                                            if(pSocial.getDelegate() != nullptr)
                                                pSocial.getDelegate()->postVKError();
                                        }
                                        NSLog(@"vk error : %@", error.localizedDescription);
                                    }];
    });
}
bool kgSocial::isLoginVK()
{
    if([ASAVkontakteUserAccount loadAccaunt] == nil)
        return false;
    else
        return true;
}