//
//  kgGaming.cpp
//  PVO
//
//  Created by flaber on 07.04.14.
//
//

#include "kgGaming.h"
#import "GameCenterView.h"
#import "GameCenterManager.h"

void kgGaming::authenticateLocalUser()
{
    [[GameCenterManager instance] authenticateLocalUser];
}
void kgGaming::reportScore(string lbName, int score)
{
    [[GameCenterManager instance] reportScore:score forCategory:[NSString stringWithCString:lbName.c_str() encoding:[NSString defaultCStringEncoding]]];
}
void kgGaming::showLeaderBord(string lbName)
{
    [GameCenterView showLeaderboard:[NSString stringWithCString:lbName.c_str() encoding:[NSString defaultCStringEncoding]]];
}
void kgGaming::showAchivments()
{
    [GameCenterView showAchivments];
}