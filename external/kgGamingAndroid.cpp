//
//  kgGaming.cpp
//  PVO
//
//  Created by flaber on 07.04.14.
//
//

#include "kgGaming.h"
#include "platform/android/jni/JniHelper.h"
#include "kgUtils.h"

void kgGaming::authenticateLocalUser()
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKGamingConnector", "authenticateLocalUser", "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}
void kgGaming::reportScore(string lbName, int score)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKGamingConnector", "reportScore", "(Ljava/lang/String;I)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(lbName.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1, score);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
    }
}
void kgGaming::showLeaderBord(string lbName)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKGamingConnector", "showLeaderBord", "(Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(lbName.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
    }
}
void kgGaming::showAchivments()
{

}