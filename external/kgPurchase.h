//
//  kgPurchase.h
//  PVO
//
//  Created by flaber on 03.04.14.
//
//

#ifndef __PVO__kgPurchase__
#define __PVO__kgPurchase__

#include <string>
#include <vector>
#include <map>

using namespace std;

struct kgProduct {
    string productIdentifier;
    string locolizedCost;
    string currencyCode;
    float cost;
    kgProduct(string _productIdentifier, string _locolizedCost, string _currencyCode, float _cost)
    {
        productIdentifier = _productIdentifier;
        locolizedCost = _locolizedCost;
        currencyCode = _currencyCode;
        cost = _cost;
    }
};
class kgPurchaseDelegate {
    
public:
    virtual void purchaseCompleted(string productIdentifier, string receipt){};
    virtual void restoreCompleted(string productIdentifier, string receipt){};
    virtual void purchaseFailed(string error){};
    virtual void purchacesInfoGated(vector<kgProduct> products){};
    virtual void purchacesInfoError(){};
    virtual void closeTransaction(){};
    virtual void showWaitOverlay(){};
    virtual void hideWaitOverlay(){};
    virtual void catchTryHack(string productIdentifier){};
    
};

class kgPurchase
{
    kgPurchaseDelegate* delegate;
public:
    
    static kgPurchase& instance()
	{
		static kgPurchase obj;
		return obj;
	}
    
    void setDelegate(kgPurchaseDelegate* _delegate){delegate = _delegate;};
    kgPurchaseDelegate* getDelegate(){return delegate;};
    
    void requestPurchace(string prodactIdentifer);
    void requestProductsData(vector<string> prodactIdentifers);
    void restoreCompletedTransactions();
    
    kgProduct getProdact(string prodactIdentifer);
};

#define pPurchase kgPurchase::instance()
#define pPurchaseDel pPurchase.getDelegate()

#endif /* defined(__PVO__kgPurchase__) */
