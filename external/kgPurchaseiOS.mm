//
//  kgPurchase.cpp
//  PVO
//
//  Created by flaber on 03.04.14.
//
//

#include "kgPurchase.h"
#import "InAppPurchaseManager.h"

void kgPurchase::requestPurchace(string prodactIdentifer)
{
    [[InAppPurchaseManager instance] requestPurchace:[NSString stringWithCString:prodactIdentifer.c_str()
                                                                        encoding:[NSString defaultCStringEncoding]]];
}
void kgPurchase::requestProductsData(vector<string> prodactIdentifers)
{
    NSMutableArray* pis = [NSMutableArray array];
    
    for (int i = 0; i < prodactIdentifers.size(); i++) {
        [pis addObject:[NSString stringWithCString:prodactIdentifers[i].c_str()
                                          encoding:[NSString defaultCStringEncoding]]];
    }
    [[InAppPurchaseManager instance] requestProductsData:pis];
}
void kgPurchase::restoreCompletedTransactions()
{
    [[InAppPurchaseManager instance] restoreCompletedTransactions];
}
kgProduct kgPurchase::getProdact(string prodactIdentifer)
{
    SKProduct* pr = [[[InAppPurchaseManager instance] getProducts] objectForKey:[NSString stringWithCString:prodactIdentifer.c_str()
                                                                                                   encoding:[NSString defaultCStringEncoding]]];
    
    if(pr != nil)
    {
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:pr.priceLocale];
        return kgProduct(string([pr.productIdentifier UTF8String]), string([[numberFormatter stringFromNumber:pr.price] UTF8String]), string([(NSString*)CFLocaleGetValue((CFLocaleRef)pr.priceLocale, kCFLocaleCurrencyCode) UTF8String]), pr.price.doubleValue);
    }
    return kgProduct("","","", 0);
}