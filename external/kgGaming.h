//
//  kgGaming.h
//  PVO
//
//  Created by flaber on 07.04.14.
//
//

#ifndef __PVO__kgGaming__
#define __PVO__kgGaming__

#include <string>
#include <vector>
#include <map>

using namespace std;

class kgGaming
{
    
public:
    
    static kgGaming& instance()
	{
		static kgGaming obj;
		return obj;
	}
    
    void authenticateLocalUser();
    void reportScore(string lbName, int score);
    
    void showLeaderBord(string lbName);
    void showAchivments();
};

#define pGaming kgGaming::instance()

#endif /* defined(__PVO__kgGaming__) */
