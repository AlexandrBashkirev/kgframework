//
//  kgSocial.h
//  PVO
//
//  Created by flaber on 12.04.14.
//
//

#ifndef __PVO__kgSocial__
#define __PVO__kgSocial__

#include <string>
#include <vector>
#include <map>

using namespace std;


class kgSocialDelegate {
    
public:

    virtual void postFBComplited(){};
    virtual void postFBError(){};
    virtual void loginFBComplited(){};
    virtual void loginFBError(){};
    virtual void logoutFBComplited(){};
    
    virtual void postTWComplited(){};
    virtual void postTWError(){};
    virtual void loginTWComplited(){};
    virtual void loginTWError(){};
    virtual void logoutTWComplited(){};
    
    virtual void postVKComplited(){};
    virtual void postVKError(){};
    virtual void loginVKComplited(){};
    virtual void loginVKError(){};
    virtual void logoutVKComplited(){};
    
    virtual void showLoading(){};
    virtual void hideLoading(){};
};

class kgSocial
{
    kgSocialDelegate* delegate;
    kgSocial();
public:
    
    static kgSocial& instance()
	{
		static kgSocial obj;
		return obj;
	}
    
    void setDelegate(kgSocialDelegate* _delegate){delegate = _delegate;};
    kgSocialDelegate* getDelegate(){return delegate;};
    
    void loginFB();
    void logoutFB();
    void postFB(string text, string caption);
    bool isLoginFB();
    
    void loginTW();
    void logoutTW();
    void postTW(string text);
    bool isLoginTW();
    
    void loginVK();
    void logoutVK();
    void postVK(string text);
    bool isLoginVK();
};

#define pSocial kgSocial::instance()
#define pSocialDel pSocial.getDelegate()


#endif /* defined(__PVO__kgSocial__) */
