//
//  kgEventManager.cpp
//  takethetreasure
//
//  Created by flaber on 30.09.13.
//
//

#include "kgUtils.h"
#include "kgEventManager.h"

kgEventListener::~kgEventListener()
{
    map<kgEventType, int>::iterator it = priority.begin();
    while (it != priority.end())
    {
        unlistenEventType(it->first);
        it = priority.begin();
    }
}
kgEventListener::kgEventListener()
{
}

void kgEventListener::listenEventType(kgEventType eventType)
{
    this->listenEventType(eventType, 0);
}
void kgEventListener::listenEventType(kgEventType eventType, int p)
{
    map<kgEventType, int>::iterator it = priority.find(eventType);
    
    if(it != priority.end())
        priority.erase(eventType);
    
    priority.insert(pair<kgEventType, int>(eventType, p));
    
    pEventManager.addListener(eventType, this);
    pEventManager.sortListners(eventType);
}

void kgEventListener::unlistenEventType(kgEventType eventType)
{
    pEventManager.removeListener(eventType, this);
    map<kgEventType, int>::iterator it = priority.find(eventType);
    
    if(it != priority.end())
        priority.erase(eventType);
}
void kgEventListener::setPriority(kgEventType eventType, int p)
{
    map<kgEventType, int>::iterator it = priority.find(eventType);
    
    if(it != priority.end())
    {
        priority.erase(eventType);
        priority.insert(pair<kgEventType, int>(eventType, p));
        
        pEventManager.sortListners(eventType);
    }
}

int kgEventListener::getPriority(kgEventType eventType)
{
    return priority[eventType];
}

///////////////////////////
void kgEventManager::addListener(kgEventType eventType, kgEventListener* listener)
{
    map<kgEventType, vector<kgListnerOneEvent>* >::iterator it = listeners.find(eventType);
    vector<kgListnerOneEvent>* _l;
    
    if (it == listeners.end())
    {
        _l = new vector<kgListnerOneEvent>();
        listeners.insert(pair<kgEventType, vector<kgListnerOneEvent>* >(eventType, _l));
    }
    else
        _l = it->second;
    
    _l->insert(_l->end(), kgListnerOneEvent(listener, eventType));
}

void kgEventManager::removeListener(kgEventType eventType, kgEventListener* listener)
{
    map<kgEventType, vector<kgListnerOneEvent>* >::iterator it = listeners.find(eventType);
    
    if (it != listeners.end())
    {
        vector<kgListnerOneEvent>* _l = it->second;
        _l->erase(remove(_l->begin(), _l->end(), kgListnerOneEvent(listener, eventType)), _l->end());
    }
}

void kgEventManager::sortListners(kgEventType eventType)
{
    map<kgEventType, vector<kgListnerOneEvent>* >::iterator it = listeners.find(eventType);
    
    if (it == listeners.end()) return;
    
    vector<kgListnerOneEvent>* _listeners = it->second;
    
    std::sort( _listeners->begin(), _listeners->end() );
}

void kgEventManager::sendEvent(kgEventType eventType)
{
    sendEvent(new kgEvent(eventType));
}
void kgEventManager::sendEvent(kgEvent* event)
{
    map<kgEventType, vector<kgListnerOneEvent>* >::iterator it = listeners.find(event->eventType);
    
    if (it == listeners.end()) return;
    
    vector<kgListnerOneEvent>* _listeners = it->second;
    
    for (int i = 0; i < _listeners->size(); i++)
    {
        if( (*_listeners)[i].listner->handleEvent(event) )
            break;
    }
    
    delete event;
}