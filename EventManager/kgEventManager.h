//
//  kgEventManager.h
//  takethetreasure
//
//  Created by flaber on 30.09.13.
//
//

#ifndef __takethetreasure__kgEventManager__
#define __takethetreasure__kgEventManager__

#include "kgUtils.h"
#include "kgEvents.h"


class kgEventListener
{
private:

    map<kgEventType, int> priority;
    
protected:
    
    kgEventListener();
    virtual ~kgEventListener();
    
    void listenEventType(kgEventType typr, int priority);
    void listenEventType(kgEventType);
    void unlistenEventType(kgEventType);
    
    void setPriority(kgEventType et, int p);
    int getPriority(kgEventType et);
    
    virtual bool handleEvent(kgEvent* event) {return false;};

    friend class kgEventManager;
    friend class kgListnerOneEvent;
};

class kgListnerOneEvent
{
public:
    kgEventListener* listner;
    kgEventType eventType;
    
    kgListnerOneEvent(kgEventListener* _listner, kgEventType _eventType)
    {
        listner = _listner;
        eventType = _eventType;
    }
    
    bool operator < (const kgListnerOneEvent &second) const
    {
        return ( this->listner->getPriority(this->eventType) < second.listner->getPriority(second.eventType) );
    }
    bool operator == (const kgListnerOneEvent &second) const
    {
        return ( this->eventType == second.eventType && this->listner == second.listner);
    }
};

class kgEventManager
{
    map<kgEventType, vector<kgListnerOneEvent>* > listeners;
    
    void addListener(kgEventType eventType, kgEventListener* listener);
    void removeListener(kgEventType eventType, kgEventListener* listener);
    void sortListners(kgEventType eventType);
    
    friend class kgEventListener;
public:
    static kgEventManager& instance()
	{
		static kgEventManager obj;
		return obj;
	}
    
    void sendEvent(kgEvent* event);
    void sendEvent(kgEventType eventType);
};

#define pEventManager kgEventManager::instance()

#endif /* defined(__takethetreasure__kgEventManager__) */
