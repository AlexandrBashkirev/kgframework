#include "kgGameScene.h"
#include "kgGameObject.h"
#include "kgWindow.h"


////////////
kgGameScene::kgGameScene()
{
    pathFinder = NULL;
    isDisappearOnsecondLayer = false;
}
kgGameScene::~kgGameScene()
{
}
void kgGameScene::gameObjectMoved(kgGameObject* go)
{
    for (int i = 0; i < gameObjects.size(); i++)
    {
        if(gameObjects[i] != go && !go->isDestroyed() && !gameObjects[i]->isDestroyed())
        {
            Rect firstRect = go->boundingBox();
            Rect secondRect = gameObjects[i]->boundingBox();
            bool f = secondRect.size.width == 0 || secondRect.size.height == 0 || firstRect.size.width == 0 || firstRect.size.height == 0;
            if(firstRect.intersectsRect(secondRect) && !f)
            {
                go->intersectWithObject(gameObjects[i]);
                gameObjects[i]->intersectWithObject(go);
            }
        }
    }
}
void kgGameScene::addGameObjectToObj(kgGameObject* go, kgGameObject* toGo, int z)
{
    go->addToGameObject(toGo, z);
    justAddedGameObjects.push_back(go);
}
void kgGameScene::addGameObject(kgGameObject* go, int z)
{
    go->addToScene(this, z);
    justAddedGameObjects.push_back(go);
}
void kgGameScene::addGameObject(kgGameObject* go)
{
    addGameObject(go, 1);
}
Point kgGameScene::percentCoordToWorldCoord(Point percent)
{
    return Point( percent.x*worldSize.width/100.0f, percent.y*worldSize.height/100.0f );
}
void kgGameScene::update(float dt)
{
    time += dt;
    vector<kgGameObject*>::iterator it = gameObjects.begin();
    while (it != gameObjects.end())
    {
        kgGameObject* go = *it;
        
        if(!go->isDestroyed())
            go->update(dt);
        
        it++;
    }
    
    it = gameObjects.begin();
    while (it != gameObjects.end())
    {
        kgGameObject* go = *it;
        
        if(go->isDestroyed())
        {
            go->removeFromParentAndCleanup(true);
            it = gameObjects.erase(it);
        }
        else
            it++;
    }
    it = justAddedGameObjects.begin();
    while (it != justAddedGameObjects.end())
    {
        gameObjects.push_back(*it);
        it++;
    }
    justAddedGameObjects.clear();
}
