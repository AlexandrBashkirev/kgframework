//
//  kgParticleSystemManager.cpp
//  PVO
//
//  Created by flaber on 20.04.14.
//
//

#include "kgParticleSystemManager.h"

void kgParticleSystemManager::setParticleSystem(string fileName, ParticleSystemQuad* p)
{
    vector<ParticleSystemQuad*>* thisTypePart;
    
    auto it = part.find(fileName);
    if(it != part.end())
        thisTypePart = it->second;
    else
    {
        thisTypePart = new vector<ParticleSystemQuad*>();
        part.insert(pair<string, vector<ParticleSystemQuad*>*>(fileName, thisTypePart));
    }
    
    p->retain();
    p->setAutoRemoveOnFinish(false);
    thisTypePart->push_back(p);
}
ParticleSystemQuad* kgParticleSystemManager::getParticleSystem(string fileName, bool autoCreate)
{
    ParticleSystemQuad* p = nullptr;
    vector<ParticleSystemQuad*>* thisTypePart;
    
    auto it = part.find(fileName);
    if(it != part.end())
        thisTypePart = it->second;
    else
    {
        thisTypePart = new vector<ParticleSystemQuad*>();
        part.insert(pair<string, vector<ParticleSystemQuad*>*>(fileName, thisTypePart));
    }
    
    for (int i = 0; i < thisTypePart->size() && p == nullptr; i++)
    {
        if(! (*thisTypePart)[i]->isActive())
        {
            p = (*thisTypePart)[i];
            p->removeFromParent();
            p->resetSystem();
        }
    }
    
    if(p == nullptr && autoCreate)
    {
        p = ParticleSystemQuad::create(fileName);
        p->retain();
        p->setAutoRemoveOnFinish(false);
        thisTypePart->push_back(p);
    }
    
    return p;
}