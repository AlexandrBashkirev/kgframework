//
//  Moveble.cpp
//  takethetreasures
//
//  Created by flaber on 27.02.13.
//
//

#include "kgMovable.h"

void kgMovable::moveTo(float x, float y)
{
    moveTo(Point(x, y));
}
void kgMovable::moveTo(Point pos)
{
    kgAStarPathFinder* pathFinder = scene->getPathFinder();
    
    if(pathFinder == NULL)
    {
        vector< PathMapCoord > path;
        path.push_back( PathMapCoord( pos.x, pos.y) );
        setPath(path);
    }
    else
    {
        PathTileCoord startTilePos = pathFinder->getTilePos(getPosition());
        PathTileCoord endTilePos = pathFinder->getTilePos(pos);
        
        vector< PathTileCoord > tilePath = pathFinder->FindPath(startTilePos, endTilePos);
        
        if(tilePath.size() > 0)
        {
            vector< PathMapCoord > path = pathFinder->convertTilePathToMapPath(tilePath);
            setPath(drawPath("persTest.png", path));
        }
    }
}

vector< PathMapCoord > kgMovable::drawPath(string texName, vector< PathMapCoord > path)
{
    SpriteBatchNode* batchNode = SpriteBatchNode::create(texName.c_str());
    
    for(int j = 0; j < path.size(); j++)
    {
        Sprite* tmp = Sprite::create(texName.c_str());
        tmp->setScale(0.5f);
        tmp->setPosition(Point(path[j].x, path[j].y));
        
        path[j].sprite = tmp;
        
        batchNode->addChild(tmp, 2);
    }
    
    getParent()->addChild(batchNode, 10);
    
    return path;
}
void kgMovable::stop()
{
    while(path.size() > 0)
    {
        if(path[0].sprite != NULL)
        {
            if(path.size() == 1)
                path[0].sprite->getParent()->removeFromParentAndCleanup(true);
            else
                path[0].sprite->removeFromParentAndCleanup(true);
        }
        path.erase(path.begin());
    }
    path.clear();
}
void kgMovable::setPath(vector<PathMapCoord> _path)
{
    if(path.size() > 0)
    {
        if(path[0].sprite != NULL)
            path[0].sprite->getParent()->removeFromParentAndCleanup(true);
    }
    if(_path.size() > 1)
    {
        if(_path[0].sprite != NULL)
            _path[0].sprite->removeFromParentAndCleanup(true);
        _path[0].sprite = NULL;
    }

    path = _path;
}

bool kgMovable::isWalk()
{
    return path.size() > 0;
}
void kgMovable::update(float dt)
{
    if(path.size() > 0)
    {
        Point nextPos = Point(path[0].x, path[0].y);
        Point dir = Point(nextPos.x - getPositionX(), nextPos.y - getPositionY());
        
        float dirLength = dir.getLength();
        
        if(dirLength < speed*dt)
        {
            pathStep(nextPos);
            setPosition(nextPos);
            if(path[0].sprite != NULL)
            {
                if(path.size() == 1)
                    path[0].sprite->getParent()->removeFromParentAndCleanup(true);
                else
                    path[0].sprite->removeFromParentAndCleanup(true);
            }
            path.erase(path.begin());
        }else
        {
            setPosition(Point(dir.x*speed*dt/dirLength + getPositionX(),
                            dir.y*speed*dt/dirLength + getPositionY()));
            
        }
    }
}