//
//  kgFrameAnimation.h
//  takethetreasure
//
//  Created by flaber on 13.01.14.
//
//

#ifndef __takethetreasure__kgFrameAnimatedObject__
#define __takethetreasure__kgFrameAnimatedObject__

#include "cocos2d.h"
using namespace cocos2d;


#include "kgUtils.h"
#include "kgAnimatedObject.h"

class kgFrameAnimatedObject : public kgAnimatedObject
{
private:
    
    //friend class FASprite;
    
    class FASprite : public Sprite
    {
    public:
        kgFrameAnimatedObject* fao;
        
        static Sprite* createWithSpriteFrameName(const char *pszSpriteFrameName)
        {
            SpriteFrame *pFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(pszSpriteFrameName);
            
            return createWithSpriteFrame(pFrame);
        }
        
        static Sprite* createWithSpriteFrame(SpriteFrame *pSpriteFrame)
        {
            FASprite *pobSprite = new FASprite();
            if (pSpriteFrame && pobSprite && pobSprite->initWithSpriteFrame(pSpriteFrame))
            {
                pobSprite->autorelease();
                return pobSprite;
            }
            CC_SAFE_DELETE(pobSprite);
            return NULL;
        }
        
        void animEnded(){fao->_animEnded();};
    };
    
    
    string currentAnim;
    
    void _animEnded();
    
    
    map<string, Animation*> animations;
protected:
    
    FASprite* sprite;
    
    void initAnimWithDictionary(__Dictionary* animInfo, Node* parent);
    void playAnimation(string animName);
    
    void pausePlaying();
    bool isPlaying();
    
    Rect boundingBox();
    
    void updateAnim(float dt);
    void setFrame(string frameName);
public:

    
};


#endif /* defined(__takethetreasure__kgFrameAnimatedObject__) */
