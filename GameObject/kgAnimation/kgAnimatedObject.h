//
//  kgAnimatedObject.h
//  takethetreasure
//
//  Created by flaber on 13.01.14.
//
//

#ifndef __takethetreasure__kgAnimatedObject__
#define __takethetreasure__kgAnimatedObject__


#include "kgUtils.h"

class kgAnimatedObject {
    
protected:
    virtual void animEnded(string animName){};
    virtual void setFrame(string frameName) = 0;
public:
    
    
    
    virtual void initAnim(string animName, Node* parent)
    {
        initAnimWithDictionary(DictionaryFromFile(animName), parent);
    };
    virtual void initAnimWithDictionary(__Dictionary* animInfo, Node* parent) = 0;
    
    virtual void playAnimation(string animName) = 0;
    virtual void pausePlaying() = 0;
    virtual bool isPlaying() = 0;
    virtual Rect boundingBox() = 0;
    virtual void updateAnim(float dt) = 0;
    
    
};

#endif /* defined(__takethetreasure__kgAnimatedObject__) */
