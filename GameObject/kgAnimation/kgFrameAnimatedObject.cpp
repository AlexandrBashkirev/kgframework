//
//  kgFrameAnimatedObject.cpp
//  takethetreasure
//
//  Created by flaber on 13.01.14.
//
//

#include "kgFrameAnimatedObject.h"

void kgFrameAnimatedObject::initAnimWithDictionary(__Dictionary* animInfo, Node* parent)
{
    
    __Array *spriteAtlases = (__Array*)animInfo->objectForKey("atlases");
    if (spriteAtlases != NULL)
        for (int i = 0; i < spriteAtlases->count(); i++)
        {
            string t = ((__String*)spriteAtlases->getObjectAtIndex(i))->getCString();
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(string(t + ".plist").c_str());
        }
    
    __Dictionary *_animations = (__Dictionary*)animInfo->objectForKey("animations");
    if (_animations != NULL)
    {
        DictElement* pElement = NULL;
        CCDICT_FOREACH(_animations, pElement)
        {
            __Dictionary* animInfo = (__Dictionary*)pElement->getObject();
            string animName = pElement->getStrKey();
            string prefix = animInfo->valueForKey("Prefix")->getCString();
            if(prefix.length() != 0)
                prefix = prefix + "_";
            
            Animation* anim = Animation::create();
            
            SpriteFrame* frame;
            int countFrames = 0;
            
            do
            {
                string frameName = string(prefix + animName + "_" + intToString(countFrames+1, 4) + ".png");
                frame = CCSpriteFrameCache::getInstance()->getSpriteFrameByName(frameName.c_str());
                if(frame)
                {
                    anim->addSpriteFrame(frame);
                    countFrames++;
                }
            }while (frame != NULL);
            
            if(countFrames > 0)
            {
                anim->retain();
                anim->setDelayPerUnit(1.0f/animInfo->valueForKey("frameRate")->doubleValue());
                
                animations.insert(pair<string, Animation*>(animName, anim));
            }
            else
                log("Can't create anim %s", animName.c_str());
        }
    }

    sprite = (FASprite*)FASprite::createWithSpriteFrameName(animInfo->valueForKey("startFrame")->getCString());
    sprite->setAnchorPoint(Point(0.5f, 0));
    sprite->setPosition(Point(0,0));
    sprite->fao = this;
    parent->addChild(sprite);
}
void kgFrameAnimatedObject::updateAnim(float dt)
{
    
}
void kgFrameAnimatedObject::_animEnded()
{
    string tmp = currentAnim;
    currentAnim = "";
    this->animEnded(tmp);
}
void kgFrameAnimatedObject::setFrame(string frameName)
{
    sprite->setSpriteFrame(frameName);
}
void kgFrameAnimatedObject::playAnimation(string animName)
{
    map<string, Animation*>::iterator it =  animations.find(animName);
    if(it != animations.end())
    {
        currentAnim = animName;
        
        Animate* animAction = CCAnimate::create(it->second);
        FiniteTimeAction* endAction = CallFunc::create(sprite, callfunc_selector( FASprite::animEnded) );
        sprite->runAction(CCSequence::create(animAction, endAction, NULL));
    }
    else
        log("can't find anim %s", animName.c_str());
}
bool kgFrameAnimatedObject::isPlaying()
{
    return currentAnim.length() > 0;
}
void kgFrameAnimatedObject::pausePlaying()
{
    ;
}
Rect kgFrameAnimatedObject::boundingBox()
{
    return sprite->boundingBox();
}