#ifndef __GameScene_SCENE_H__
#define __GameScene_SCENE_H__

#include "kgUtils.h"
#include "kgUI.h"
#include "kgEventManager.h"

#ifdef BOX2D_USE
#include "Box2D.h"
#endif
class kgGameObject;
class kgAStarPathFinder;


class kgGameScene : public kgView, public kgEventListener
{

    vector<kgGameObject*> justAddedGameObjects;
protected:
    
#ifdef BOX2D_USE
    b2World* world;
#endif
    
    kgAStarPathFinder* pathFinder;
    
    __Dictionary* levelInfo;
    
    Size worldSize;
    
    vector<kgGameObject*> gameObjects;
    
    float time;
public:
    kgGameScene();
    virtual ~kgGameScene();
    
    void update(float dt);

    Point percentCoordToWorldCoord(Point percent);
    Size getWorldSize(){return worldSize;};
    kgAStarPathFinder* getPathFinder(){return pathFinder;};
    
    void gameObjectMoved(kgGameObject* go);
    void addGameObject(kgGameObject* go, int z);
    void addGameObjectToObj(kgGameObject* go, kgGameObject* toGo, int z);
    void addGameObject(kgGameObject* go);
    
    friend class kgGameObject;
    
    float getWorldTime(){return time;};
    
#ifdef BOX2D_USE
    b2World* getWorld(){return world;};
#endif
};

#endif // __GameScene_SCENE_H__
