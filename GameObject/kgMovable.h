//
//  Moveble.h
//  takethetreasures
//
//  Created by mac on 27.02.13.
//
//


#ifndef takethetreasures_kgMoveble_h
#define takethetreasures_kgMoveble_h

#include "cocos2d.h"
#include "kgAStarLibrary.h"
#include "kgGameObject.h"
using namespace cocos2d;

class kgMovable : public kgGameObject
{
    
protected:
    float speed;
    float stepVolume;
    
    vector<PathMapCoord> path;
    
    void update(float dt);
    
    virtual void pathStep(Point pos){};
public:
    vector< PathMapCoord > drawPath( string texName, vector< PathMapCoord > path);
    void setPath(vector<PathMapCoord> _path);
    
    void moveTo(float x, float y);
    void moveTo(Point pos);
    
    void stop();
    
    bool isWalk();
};

#endif
