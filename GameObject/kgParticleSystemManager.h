//
//  kgParticleSystemManager.h
//  PVO
//
//  Created by flaber on 20.04.14.
//
//

#ifndef __PVO__kgParticleSystemManager__
#define __PVO__kgParticleSystemManager__

#include "kgUtils.h"

class kgParticleSystemManager
{
    map<string, vector<ParticleSystemQuad*>* > part;
    
public:
    
    static kgParticleSystemManager& instance()
    {
		static kgParticleSystemManager obj;
		return obj;
	}
    
    ParticleSystemQuad* getParticleSystem(string fileName, bool autoCreate = true);
    void setParticleSystem(string fileName, ParticleSystemQuad*);
};
#define pParticleSystemManager kgParticleSystemManager::instance()

#endif /* defined(__PVO__kgParticleSystemManager__) */
