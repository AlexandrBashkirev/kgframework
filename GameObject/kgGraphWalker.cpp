//
//  kgGraphWalker.cpp
//  takethetreasure
//
//  Created by flaber on 11.01.14.
//
//

#include "kgGraphWalker.h"


void kgGraphWalker::init(__Dictionary* _objData)
{
    kgMovable::init(_objData);
    
    __Array* _graphNodes = (__Array*)_objData->objectForKey("graph");
    if(_graphNodes != NULL)
        for (int i = 0; i < _graphNodes->count(); i++)
        {
            __Dictionary* nodeInfo = (__Dictionary*)_graphNodes->getObjectAtIndex(i);
            kgGraphNode gn;
            gn.pos = PointFromString(nodeInfo->valueForKey("pos")->getCString());
            
            __Array* _edges = (__Array*)nodeInfo->objectForKey("edges");
            for (int j = 0; j < _edges->count(); j++)
                gn.edges.push_back(((__String*)(_edges->getObjectAtIndex(j)))->intValue());
            graph.push_back(gn);
        }
    nextNode = -1;
    
    FloidUorshel();
}
void kgGraphWalker::addToScene(kgGameScene* _scene, int z)
{
    kgGameObject::addToScene(_scene, z);
    
    /*SpriteBatchNode* batchNode = SpriteBatchNode::create("nullImg.png");
    
    for(int j = 0; j < graph.size(); j++)
    {
        Sprite* tmp = Sprite::create("nullImg.png");
        tmp->setColor(Color3B(255,0,0));
        tmp->setPosition(scene->percentCoordToWorldCoord( graph[j].pos ));
        
        graph[j].sprite = tmp;
        
        batchNode->addChild(tmp, 2);
    }
    
    getParent()->addChild(batchNode, 10);*/
}
void kgGraphWalker::update(float dt)
{
    kgMovable::update(dt);
    
    int countPoints = graph.size();
    if(countPoints == 0) return;
    
    if(nextNode > -1 && !isWalk())
    {
        currentNode = nextNode;
        nextNode = -1;
        
        graphPath.erase(graphPath.end()-1);
        if(graphPath.size() == 0)
            pathComplited();
    }
    
    if (!isWalk() && graphPath.size() != 0)
    {
        nextNode = graphPath[graphPath.size()-1];
        moveTo( scene->percentCoordToWorldCoord( graph[ graphPath[graphPath.size()-1] ].pos ) );
    }
}
void kgGraphWalker::folowPoint(Point pos, bool _onlyGraph)
{
    folowingPointPos = pos;
    onlyGraph = _onlyGraph;
    
    pair<int, int> n = getNodesNearestToPoint(folowingPointPos);
    
    if(currentNode != n.first && currentNode != n.second)
    {
        goToNode(n.first);
    }
    else
    {
        /*graphPath.clear();
        stop();
        
        if(!onlyGraph)
            moveTo(folowingPointPos.x, folowingPointPos.y);
        else
        {
            Point p1 = getNodePos(n.first);
            Point p2 = getNodePos(n.second);
            
            float A1 = p1.y - p2.y;
            float B1 = p2.x - p1.x;
            float C1 = p1.x*p2.y - p2.x*p1.y;
            
            Point p = p1 = p2;
            p = Point(-p.y, p.x).normalize();
            
            p1 = folowingPointPos;
            p2 = Point(p.x + folowingPointPos.x, p.y + folowingPointPos.y);
            
            float A2 = p1.y - p2.y;
            float B2 = p2.x - p1.x;
            float C2 = p1.x*p2.y - p2.x*p1.y;
            
            float y = (A2*C1/A1 - C2)/(B2 - A2*B1/A1);
            float x = - (B1*y + C1)/A1;
            
            moveTo(x, y);
        }*/
    }
}
void kgGraphWalker::setToNode(int n)
{
    int countPoints = graph.size();
    if(countPoints == 0) return;
    
    currentNode = n;
    nextNode = -1;
    setPosition(scene->percentCoordToWorldCoord( graph[currentNode].pos ));
}
pair<int, int> kgGraphWalker::getNodesNearestToPoint(Point p)
{
    float miDist = 10000;
    pair<int, int> minDistPoint = pair<int, int>(0,0);
    
    for(int j = 0; j < graph.size(); j++)
    {
        float tmpDist = p.getDistance(scene->percentCoordToWorldCoord( graph[j].pos ));
        if(tmpDist < miDist)
        {
            minDistPoint.second = minDistPoint.first;
            minDistPoint.first = j;
            miDist = tmpDist;
        }
    }
    return minDistPoint;
}
int kgGraphWalker::getNodeNearestToPoint(Point p)
{
    float miDist = 10000;
    int minDistPoint = 0;
    for(int j = 0; j < graph.size(); j++)
    {
        float tmpDist = p.getDistance(scene->percentCoordToWorldCoord( graph[j].pos ));
        if(tmpDist < miDist)
        {
            minDistPoint = j;
            miDist = tmpDist;
        }
    }
    return minDistPoint;
}
void kgGraphWalker::goToRandomNode()
{
    int countPoints = graph.size();
    if(countPoints == 0) return;
    
    int rnd;
    do {
        rnd = rand()%countPoints;
    } while (rnd == currentNode);
    
    goToNode(rnd);
}
void kgGraphWalker::goToNode(int n)
{
    int countPoints = graph.size();
    if(countPoints == 0) return;
    
    graphPath.clear();
    stop();
    if( !isWalk() )
    {
        int tmpNode = n;
        graphPath.push_back(tmpNode);
        do {
            //int prevNode = tmpNode;
            
			if (tmpNode != currentNode )
				tmpNode = p[currentNode][tmpNode];
			
			if (tmpNode != currentNode )
			{
                graphPath.push_back(tmpNode);
            }
        } while (tmpNode != currentNode);
    }
}
void kgGraphWalker::FloidUorshel()
{
	int countPoints = graph.size();
    
    if(countPoints == 0) return;
    
	d = new float*[countPoints];
	p = new int*[countPoints];
	float** _d = new float*[countPoints];
    int** _p = new int*[countPoints];
    
	for(int i = 0; i < countPoints; i++)
	{
		d[i] = new float[countPoints];
		p[i] = new int[countPoints];
		_d[i] = new float[countPoints];
		_p[i] = new int[countPoints];
	}
    
    for (int i = 0; i < countPoints; i++)
    {
        for (int j = 0; j < countPoints; j++)
        {
            d[i][j] = 100500;
            p[i][j] = -1;
        }
    }
    for (int i = 0; i < countPoints; i++)
    {
		for (int j = 0; j < graph[i].edges.size(); j++)
        {
			float dist = 1;//ccpDistance( getSpritePosInChapter(i), getSpritePosInChapter(chapters->graph[i][j]) );
            
            d[graph[i].edges[j]][i] = dist;
            d[i][graph[i].edges[j]] = dist;
            
            p[graph[i].edges[j]][i] = graph[i].edges[j];
            p[i][graph[i].edges[j]] = i;
        }
    }
    
    for (int k = 0; k < countPoints; k++)
    {
        for (int i = 0; i < countPoints; i++)
        {
            for (int j = 0; j < countPoints; j++)
            {
				_d[i][j] = d[i][j] < d[i][k] + d[k][j] ? d[i][j] : d[i][k] + d[k][j];
                _p[i][j] = d[i][j] > d[i][k] + d[k][j] ? p[k][j] : p[i][j];
            }
        }
        
        int** __p = p;
        p = _p;
        _p = __p;
        
        float** __d = d;
        d = _d;
        _d = __d;
    }
    

    for(int i = 0; i < countPoints; i++)
	{
        delete[] _d[i];
        delete[] _p[i];
    }
    delete[] _d;
    delete[] _p;
}
kgGraphWalker::~kgGraphWalker()
{
    int countPoints = graph.size();
    if(countPoints > 0)
    {
        for(int i = 0; i < countPoints; i++)
        {
            delete[] d[i];
            delete[] p[i];
        }
        delete[] d;
        delete[] p;
    }
}