//
//  kgGraphWalker.h
//  takethetreasure
//
//  Created by flaber on 11.01.14.
//
//

#ifndef __takethetreasure__kgGraphWalker__
#define __takethetreasure__kgGraphWalker__

#include "cocos2d.h"
#include "kgAStarLibrary.h"
#include "kgMovable.h"
using namespace cocos2d;

struct kgGraphNode {
    Point pos;
    vector<int> edges;
    Sprite* sprite;
};
class kgGraphWalker : public kgMovable
{
private:
    
    vector< kgGraphNode > graph;
    vector< int > graphPath;
    
    int currentNode;
    int nextNode;
    
    float** d;
    int** p;
    void FloidUorshel();
    
    Point folowingPointPos;
    bool onlyGraph;
protected:
    
    void update(float dt);
    void goToRandomNode();
    pair<int, int> getNodesNearestToPoint(Point p);
    int getNodeNearestToPoint(Point p);
    void goToNode(int n);
    void setToNode(int n);
    
    void folowPoint(Point pos, bool onlyGraph);
    
    int getCurrentNode(){return currentNode;};
    
    virtual void pathComplited(){};
public:
    
    virtual ~kgGraphWalker();
    
    void init(__Dictionary* objData);
    void addToScene(kgGameScene* _scene, int z);
    
    int getCountNodes(){return graph.size();};
    
    Point getNodePos(int n){return graph[n].pos;};
};

#endif /* defined(__takethetreasure__kgGraphWalker__) */
