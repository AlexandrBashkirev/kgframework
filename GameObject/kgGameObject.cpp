//
//  kgGameObject.cpp
//  takethetreasure
//
//  Created by flaber on 30.09.13.
//
//

#include "kgGameObject.h"


kgGameObject* kgGameObject::create(__Dictionary* objData)
{
    CreateGOCallbacks creater = pGOFactory.GetStrCallBacks(objData->valueForKey("type")->getCString());
    kgGameObject* go = (creater)();
    go->init(objData);
    go->type = objData->valueForKey("type")->getCString();
    return go;
}

void kgGameObject::init(__Dictionary* _objData)
{
    objData = _objData;
    objData->retain();
    
    __Array* ar = (__Array*)_objData->objectForKey("events");
    if(ar != NULL)
        for (int i = 0; i < ar->count(); i++)
        {
            __String* t = (__String*)ar->getObjectAtIndex(i);
            listenEventType( (kgEventType)t->intValue() );
        }
};
kgGameObject::kgGameObject()
{
    objData = NULL;
    destroyed = false;
    testSprite = nullptr;
    
#ifdef BOX2D_USE
    m_pBody = NULL;
    usePhysic = true;
#endif
}
kgGameObject::~kgGameObject()
{
    if(objData != NULL)
        objData->release();
    
#ifdef BOX2D_USE
    if(m_pBody != NULL)
        scene->getWorld()->DestroyBody( m_pBody );
#endif
    if(testSprite != nullptr)
    {
        testSprite->removeFromParentAndCleanup(true);
        testSprite = nullptr;
    }
}
void kgGameObject::destroy()
{
    destroyed = true;
}
void kgGameObject::recalcZ()
{
}
#ifdef BOX2D_USE
void kgGameObject::createBody()
{
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    
    m_pBody = scene->getWorld()->CreateBody(&bodyDef);
    
    // Define another box shape for our dynamic body.
    
    boxSizeCoef = objData->valueForKey("boxSizeCoef")->doubleValue();
    
    b2PolygonShape dynamicBox;
    dynamicBox.SetAsBox(getContentSize().width*boxSizeCoef/kPixelsPerMeter/2.0f, getContentSize().height*boxSizeCoef/kPixelsPerMeter/2.0f);//These are mid points for our 1m box
    
    // Define the dynamic body fixture.
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &dynamicBox;
    fixtureDef.density = objData->valueForKey("density")->doubleValue();//1.0f;
    fixtureDef.friction = objData->valueForKey("friction")->doubleValue();//0.4f;
    m_pBody->CreateFixture(&fixtureDef);
    
    m_pBody->SetUserData(this);
}
#endif
void kgGameObject::addToGameObject(kgGameObject* _obj, int z)
{
    scene = _obj->getkgScene();
    _obj->addChild(this, z);
    
#ifdef BOX2D_USE
    if(usePhysic)
        createBody();
#endif
}
void kgGameObject::addToScene(kgGameScene* _scene, int z)
{
    scene = _scene;
    scene->addChild(this, z);
    
#ifdef BOX2D_USE
    if(usePhysic)
        createBody();
#endif
}
#ifdef BOX2D_USE
const kmMat4& kgGameObject::getNodeToParentTransform() const
{
    if(m_pBody != NULL)
    {
        b2Vec2 pos  = m_pBody->GetPosition();
        
        float x = pos.x * kPixelsPerMeter;
        float y = pos.y * kPixelsPerMeter - getContentSize().height*boxSizeCoef/2.0f;
        
        if ( isIgnoreAnchorPointForPosition() ) {
            x += getAnchorPointInPoints().x;
            y += getAnchorPointInPoints().y;
        }
        
        // Make matrix
        float radians = m_pBody->GetAngle();
        float c = cosf(radians);
        float s = sinf(radians);
        
        if( ! getAnchorPointInPoints().equals( Point::ZERO) ){
            x += c*-getAnchorPointInPoints().x + -s*-getAnchorPointInPoints().y;
            y += s*-getAnchorPointInPoints().x + c*-getAnchorPointInPoints().y;
        }
        
        cocos2d::CGAffineToGL(AffineTransformMake( c,  s,
                                                  -s,    c,
                                                  x,    y ), _transform.mat);
        
        return _transform;
    }
    return Node::getNodeToParentTransform();
}
#endif
void kgGameObject::update(float dt)
{
}
void kgGameObject::setPosition(const Point& var)
{
    Node::setPosition(var);
    
#ifdef BOX2D_USE
    if(m_pBody != NULL)
        m_pBody->SetTransform(b2Vec2((var.x + getContentSize().width*(0.5 - getAnchorPoint().x))/kPixelsPerMeter,
                                     (var.y + getContentSize().height*(0.5 - getAnchorPoint().y))/kPixelsPerMeter), m_pBody->GetAngle());
#endif
    
    recalcZ();
}

Rect kgGameObject::boundingBox()
{
    return getBoundingBox();
}
Rect kgGameObject::getBoundingBox()
{
    float parentScale = 1;
    
    Node* node = (Node*)this;
    
    while(node != nullptr)
    {
        parentScale *= node->getScale();
        node = node->getParent();
    };
    
    Point p = convertToWorldSpace(Point(0, 0));
    
    Point ach = getAnchorPoint();
    Size s = getContentSize();
    Rect rc = Rect(p.x + s.width*parentScale*(ach.x-0.5f),
                   p.y + s.height*parentScale*(- ach.y),
                   s.width*parentScale,
                   s.height*parentScale);
    
    return rc;
}