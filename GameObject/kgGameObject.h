//
//  kgGameObject.h
//  balls
//
//  Created by flaber on 08.02.13.
//
//

#ifndef balls_kgGameObject_h
#define balls_kgGameObject_h


#include "kgGameScene.h"
#include "kgUtils.h"


class kgGameObject;
typedef kgGameObject* (*CreateGOCallbacks)();

class kgGameObjectFactory : public kgAbstractFactory<CreateGOCallbacks>
{
public:
	static kgGameObjectFactory& instance()
	{
		static kgGameObjectFactory obj;
		return obj;
	}
};
#define pGOFactory kgGameObjectFactory::instance()

class kgGameObject : public Node, public kgEventListener
{
    
    bool destroyed;
public:
    
    static kgGameObject* create(__Dictionary* objData);
    virtual void init(__Dictionary* _objData);
    kgGameObject();
    virtual ~kgGameObject();
    
    void destroy();
    virtual string getType(){return type;};
    
    virtual void recalcZ();
    void setPosition(const Point& var);
    virtual void addToScene(kgGameScene* _scene, int z);
    virtual void addToGameObject(kgGameObject* _obj, int z);
    
    virtual void intersectWithObject(kgGameObject* go){};
    
    void update(float dt);
    
    Rect boundingBox();
    Rect getBoundingBox();
    
    kgGameScene* getkgScene(){return scene;};
 
#ifdef BOX2D_USE
    virtual void createBody();
    virtual const kmMat4& getNodeToParentTransform() const;
    b2Body* getPhysicBody(){return m_pBody;};
    float boxSizeCoef;
#endif
    
    bool isDestroyed(){return destroyed;};
protected:
    
    Sprite* testSprite;
    
    kgGameScene* scene;
    string type;
    __Dictionary* objData;
    
#ifdef BOX2D_USE
    bool usePhysic;
    b2Body* m_pBody;
#endif
};

#endif
