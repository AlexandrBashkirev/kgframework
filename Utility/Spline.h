//
//  Spline.h
//  FairyCraft
//
//  Created by flaber on 21.01.15.
//
//

#ifndef __FairyCraft__Spline__
#define __FairyCraft__Spline__

#include <stdio.h>
#include <cmath>
#include <vector>

using namespace std;

#include "cocos2d.h"
using namespace cocos2d;

class Spline
{
private:
    // Структура, описывающая сплайн на каждом сегменте сетки
    struct spline_tuple
    {
        float a, b, c, d, x;
    };
    
    spline_tuple *splines; // Сплайн
    std::size_t n; // Количество узлов сетки
    
    void free_mem(); // Освобождение памяти
    
public:
    Spline(); //конструктор
    ~Spline(); //деструктор
    
    // Построение сплайна
    // x - узлы сетки, должны быть упорядочены по возрастанию, кратные узлы запрещены
    // y - значения функции в узлах сетки
    // n - количество узлов сетки
    void build_spline(vector<Point> points);
    
    // Вычисление значения интерполированной функции в произвольной точке
    float f(float x) const;
};

#endif /* defined(__FairyCraft__Spline__) */
