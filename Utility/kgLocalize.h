//
//  kgLocalizet.h
//  PVO
//
//  Created by flaber on 03.04.14.
//
//

#ifndef __PVO__kgLocalize__
#define __PVO__kgLocalize__

#include <string>
#include <vector>
#include <map>

using namespace std;

class kgLocalize
{
    map<string, string> localizationStrings;
    string currentLocal;
    kgLocalize();
    string getDeviceLocal();
    
    
public:
    static kgLocalize& instance()
	{
		static kgLocalize obj;
		return obj;
	}
    
    string getCurrentLocal();
    string getStr(string key);
};
#define pLocalize kgLocalize::instance()
#define LOCALIZED(_key_) pLocalize.getStr(_key_)
#define CURLOCAL() pLocalize.getCurrentLocal()

#endif /* defined(__PVO__kgLocalizet__) */
