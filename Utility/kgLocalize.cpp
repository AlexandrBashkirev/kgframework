//
//  kgLocalizet.cpp
//  PVO
//
//  Created by flaber on 03.04.14.
//
//

#include "kgLocalize.h"
#include "cocos2d.h"
#include "kgUtils.h"
using namespace cocos2d;

void addLocalStrings(map<string, string>* ls, __Dictionary* lsSrc)
{
    __Array* keys = lsSrc->allKeys();
    for (int i = 0; i < keys->count(); i++)
    {
        __String* key = (__String*)keys->getObjectAtIndex(i);
        string _key = key->getCString();
        
        Object* val = lsSrc->objectForKey(_key);
        
        if(dynamic_cast<__Dictionary*>(val) != nullptr)
            addLocalStrings(ls, (__Dictionary*)val);
        else if(dynamic_cast<__String*>(val) != nullptr)
        {
            
            string _value = ((__String*)val)->getCString();
            ls->insert(pair<string, string>(_key, _value));
        }
    }
}
string kgLocalize::getDeviceLocal()
{
    switch (Application::getInstance()->getCurrentLanguage())
    {
        case LanguageType::ENGLISH:
            return "en";
            break;
        case LanguageType::JAPANESE:
            return "ja";
            break;
        case LanguageType::KOREAN:
            return "ko";
            break;
        case LanguageType::RUSSIAN:
            return "ru";
            break;
        case LanguageType::SPANISH:
            return "es";
            break;
        case LanguageType::GERMAN:
            return "de";
            break;
        case LanguageType::ITALIAN:
            return "it";
            break;
        case LanguageType::FRENCH:
            return "fr";
            break;
        case LanguageType::HUNGARIAN:
            return "hu";
            break;
        case LanguageType::PORTUGUESE:
            return "pt";
            break;
        case LanguageType::CHINESE:
            return "zh";
            break;
        case LanguageType::ARABIC:
            return "ar";
            break;
        case LanguageType::NORWEGIAN:
            return "nb";
            break;
        case LanguageType::POLISH:
            return "pl";
            break;
            
        default:
            return "en";
            break;
    }
}
kgLocalize::kgLocalize()
{
    __Dictionary* appSet = DictionaryFromFile("applicationSettings.plist");
    __Array* localization = nullptr;
    if(appSet != nullptr)
        localization = (__Array*)appSet->objectForKey("localization");
    
    string t = getDeviceLocal();
    currentLocal = "en";
    
    if (localization != nullptr)
        for (int i = 0; i < localization->count(); i++)
        {
            __String* s = (__String*)localization->getObjectAtIndex(i);
            if(s->isEqual(__String::create(t)))
            {
                currentLocal = t;
                break;
            }
        }
    
    __Dictionary* local = DictionaryFromFile("localization.plist");
    
    __Dictionary* locals = nullptr;
    if(local != nullptr)
        locals = (__Dictionary*)local->objectForKey(currentLocal);
    
    if (locals != nullptr)
    {
        addLocalStrings(&localizationStrings, locals);
    }
}

string kgLocalize::getCurrentLocal()
{
    return currentLocal;
}
string kgLocalize::getStr(string key)
{
    auto it = localizationStrings.find(key);
    if (it != localizationStrings.end()) {
        return it->second;
    }
    log("kgLocalize Warning: Can't find value for key %s", key.c_str());
    return key;
}
