#include "kgPlatform.h"
#include "kgUtils.h"


string getLevelPath(int world, int level)
{
/*#ifdef EDITOR_MODE
    
    NSString* levelPath = getDocPath( world, level);
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:levelPath])
    {
        [levelPath release];
        string fileName = string("level_" + intToString(world) +"_"+ intToString(level) + ".plist");
        const char *pszPath = CCFileUtils::sharedFileUtils()->fullPathFromRelativePath(fileName.c_str());
        return string(pszPath);
    }
    
    return string([levelPath UTF8String]);
#else*/
    string fileName = string("level_" + toString<int>(world) +"_"+ toString<int>(level) + ".plist");
    return FileUtils::getInstance()-> fullPathForFilename(fileName);

//#endif
}
void openURL(string url)
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithCString:url.c_str()
                                                                                       encoding:[NSString defaultCStringEncoding]]]];
}

unsigned int getTime()
{
    NSDate *date = [NSDate date];
    
    return date.timeIntervalSince1970;
}
void savePhotoFileToAlbum(string pathToFile)
{
    UIImage *myImage = [UIImage imageWithContentsOfFile:[NSString stringWithCString:pathToFile.c_str()
                                                                           encoding:[NSString defaultCStringEncoding]] ];
    UIImageWriteToSavedPhotosAlbum(myImage, nil, nil, nil);
}

void registerNotification()
{
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
}
void closeLocalNotification()
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}
void scheduleLocalNotification(int timeDelay, string text)
{
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:timeDelay];//
    localNotification.alertBody = [NSString stringWithCString:text.c_str() encoding:NSUTF8StringEncoding];
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    [localNotification release];
}