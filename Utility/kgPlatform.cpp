#include "kgPlatform.h"
#include "kgUtils.h"
#include "platform/android/jni/JniHelper.h"
string getLevelPath(int world, int level)
{
    string fileName = string("level_" + toString(world) +"_"+ toString(level) + ".plist");
    return FileUtils::getInstance()-> fullPathForFilename(fileName);
}
void openURL(string url)
{
    JniMethodInfo t;
    
    if (JniHelper::getStaticMethodInfo(t, "com/flaber/kgNDKMarcketingConnector", "openURL", "(Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(url.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
        t.env->DeleteLocalRef(t.classID);
        t.env->DeleteLocalRef(stringArg1);
    }
}
unsigned int getTime()
{
    std::time_t result = std::time(nullptr);
    return result;
}

void savePhotoFileToAlbum(string pathToFile)
{
    
}

void registerNotification()
{
    
}
void closeLocalNotification()
{
    
}
void scheduleLocalNotification(int timeDelay, string text)
{
    
}