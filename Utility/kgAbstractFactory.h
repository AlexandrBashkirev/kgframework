#ifndef _ABSTRACTFACTORY_
#define _ABSTRACTFACTORY_

#include "kgUtils.h"

template <class T>
class kgAbstractFactory
{
protected:
	map<unsigned long, T> m_Callbacks;
    map<string, T> m_StrCallbacks;
public:
	bool RegisterCallBacks(unsigned long _type, T _callback)
	{
        typename map< unsigned long, T >::iterator _iCalBacks;
        
        _iCalBacks =  m_Callbacks.find(_type);
		if(_iCalBacks ==  m_Callbacks.end())
		{
            return m_Callbacks.insert(pair<unsigned long, T>(_type, _callback)).second;
        }
        return true;
	}
	T GetCallBacks(unsigned long _type)
	{
        typename map< unsigned long, T >::iterator _iCalBacks;
        
		_iCalBacks =  m_Callbacks.find(_type);
		if(_iCalBacks ==  m_Callbacks.end())
		{
			return 0;
		}
		return _iCalBacks->second;
	}
    
    bool RegisterStrCallBacks(string _type, T _callback)
	{
        typename map< string, T >::iterator _iCalBacks;
        
        _iCalBacks =  m_StrCallbacks.find(_type);
		if(_iCalBacks ==  m_StrCallbacks.end())
		{
            return m_StrCallbacks.insert(pair<string, T>(_type, _callback)).second;
        }
        return true;
	}
	T GetStrCallBacks(string _type)
	{
        typename map< string, T >::iterator _iCalBacks;
        
		_iCalBacks =  m_StrCallbacks.find(_type);
		if(_iCalBacks ==  m_StrCallbacks.end())
		{
            log("Error GetStrCallBacks can't find type: %s", _type.c_str());
			return 0;
		}
		return _iCalBacks->second;
	}
};

#endif //_ABSTRACTFACTORY_