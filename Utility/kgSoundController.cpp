//
//  SoundController.cpp
//  balls
//
//  Created by mac on 23.02.13.
//
//

#include "kgSoundController.h"


void kgSoundController::playBackgroundMusic(const char* pszFilePath, bool bLoop )
{
    if(activBackMusic.compare( pszFilePath ) != 0)
    {
        activBackMusic = pszFilePath;
        SimpleAudioEngine::playBackgroundMusic(pszFilePath, bLoop);
    }
}
void kgSoundController::stopBackgroundMusic(bool bReleaseData)
{
    activBackMusic = "";
    SimpleAudioEngine::stopBackgroundMusic(bReleaseData);
}