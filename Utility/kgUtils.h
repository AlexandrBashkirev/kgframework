//
//  Utils.h
//  Platformer
//
//  Created by flaber on 06.02.14.
//
//

#ifndef __Platformer__Utils__
#define __Platformer__Utils__

#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include "math.h"
#include <map>
#include "kgAppMacros.h"
#include "kgPlatform.h"
#include <stdlib.h>

#include "cocos2d.h"
using namespace cocos2d;

#include "SimpleAudioEngine.h" 
using namespace CocosDenshion;

using namespace std;

#include "kgSoundController.h"

struct PointInt {
    PointInt(int _x, int _y) : x(_x), y(_y){};
    int x, y;
};
template <class T>
string toString(T a)
{
    stringstream ss;
    ss<<a;
    string str;
    ss>>str;
    
	return str;
}
string intToString(int a, int minLength = 0);
string intToStringThousandTok(int a, int max1 = 10000, int max2 = 1000000);

int sign(float a);
int sign(int a);
bool replace(std::string& str, const std::string& from, const std::string& to);

bool intersects(cocos2d::Point circlePos, float circleR, cocos2d::Rect rect);

void loadColorShader(GLProgram* prog, string shaderName);

Color3B Color3BFromString(const char* pszContent);
Color4B Color4BFromString(const char* pszContent);
__Dictionary* DictionaryFromFile(string fileName);
#endif /* defined(__Platformer__Utils__) */
