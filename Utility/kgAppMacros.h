#ifndef __APPMACROS_H__
#define __APPMACROS_H__

#include "cocos2d.h"

#define screenSize Director::getInstance()->getWinSize()
#define screenCenter Point(screenSize.width/2, screenSize.height/2)

#ifdef LANDSCAPE_MODE
    #define scaleW  (screenSize.width/2048.0f)
    #define scaleH  (screenSize.height/1536.0f)
    #define AspectRatio (screenSize.width/screenSize.height)
#else
    #define scaleW  (screenSize.width/1536.0f)
    #define scaleH  (screenSize.height/2048.0f)
    #define AspectRatio (screenSize.width/screenSize.height)
#endif

#define IS_LONG_SCREEN (AspectRatio < 640.0f/960.0f)

#define ScaleSpriteW(s) s->setScale(scaleW);
#define ScaleSpriteH(s) s->setScale(scaleH);

#define kPixelsPerMeter (32.0f)

#define countWorlds 1
#define countLevelsRow 2
#define countLevelsCol 5

// The font size 24 is designed for small resolution, so we should change it to fit for current design resolution
#define TITLE_FONT_SIZE  (cocos2d::CCEGLView::sharedOpenGLView()->getDesignResolutionSize().width / 480 * 18)


#define EDITOR_MODE

#define IPAD (screenSize.width == 2048 || screenSize.height == 2048 || screenSize.width == 1024 || screenSize.height == 1024)

#define IPHONE (!IPAD)

#define IPHONE5 (screenSize.width == 1136 || screenSize.height == 1136 )
#define IPHONE6 (screenSize.width == 1136 || screenSize.height == 1136 )
#define IPHONE6Plus (screenSize.width == 1920 || screenSize.height == 1920 )

#define DRAW_TEST_INFO

#endif /* __APPMACROS_H__ */
