//
//  SoundController.h
//  balls
//
//  Created by mac on 23.02.13.
//
//

#ifndef balls_SoundController_h
#define balls_SoundController_h

#include "kgUtils.h"

class kgSoundController : public SimpleAudioEngine
{
    string activBackMusic;
public:
    

    void playBackgroundMusic(const char* pszFilePath, bool bLoop = false);
    void stopBackgroundMusic(bool bReleaseData = false);
    
    static kgSoundController& instance()
    {
        static kgSoundController obj;
        return obj;
    }
};
#endif
