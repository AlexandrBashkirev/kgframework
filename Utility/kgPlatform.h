#ifndef __FUNCTIONS_H__
#define __FUNCTIONS_H__

#include "kgUtils.h"
using namespace std;

std::string getLevelPath(int world, int level);
void openURL(std::string url);
unsigned int getTime();

void savePhotoFileToAlbum(string pathToFile);

void registerNotification();
void closeLocalNotification();
void scheduleLocalNotification(int timeDelay, string text);

#endif  // __FUNCTIONS_H__