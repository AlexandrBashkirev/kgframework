//
//  Utils.cpp
//  Platformer
//
//  Created by flaber on 06.02.14.
//
//

#include "kgUtils.h"
#include "kgLocalize.h"

string intToString(int a, int minLength)
{
    string str = toString<int>(a);
    
    while (str.length() < minLength)
    {
        str = "0" + str;
    }
    return str;
}
string intToStringThousandTok(int a, int max1, int max2)
{
    if(a >= max2)
    {
        return intToString(a/1000000)+"kk";
    }
    else if (a >= max1)
    {
        return intToString(a/1000)+"k";
    }
    return intToString(a);
}
void loadColorShader(GLProgram* prog, string shaderName)
{
    prog->initWithVertexShaderFilename( string(shaderName + "_vert.glsl").c_str(), string(shaderName + "_frag.glsl").c_str());
    prog->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
    prog->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORDS);
    prog->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_COLOR, GLProgram::VERTEX_ATTRIB_COLOR);
    prog->link();
    prog->updateUniforms();
}
bool intersects(Point circlePos, float circleR, Rect rect)
{
    rect = Rect(rect.origin.x + rect.size.width/2, rect.origin.y + rect.size.height, rect.size.width, rect.size.height);
    Point circleDistance;
    circleDistance.x = abs(circlePos.x - rect.origin.x);
    circleDistance.y = abs(circlePos.y - rect.origin.y);
    
    if (circleDistance.x > (rect.size.width/2 + circleR)) { return false; }
    if (circleDistance.y > (rect.size.height/2 + circleR)) { return false; }
    
    if (circleDistance.x <= (rect.size.width/2)) { return true; }
    if (circleDistance.y <= (rect.size.height/2)) { return true; }
    
    float cornerDistance_sq = pow((circleDistance.x - rect.size.width/2), 2) + pow((circleDistance.y - rect.size.height/2), 2);
    
    return (cornerDistance_sq <= pow(circleR, 2));
}
bool replace(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}
int sign(float a)
{
    return a > 0 ? 1 : -1;
}
int sign(int a)
{
    return a > 0 ? 1 : -1;
}

static inline void _split(std::string src, const char* token, vector<string>& vect)
{
    int nend=0;
    int nbegin=0;
    while(nend != -1)
    {
        nend = src.find(token, nbegin);
        if(nend == -1)
            vect.push_back(src.substr(nbegin, src.length()-nbegin));
        else
            vect.push_back(src.substr(nbegin, nend-nbegin));
        nbegin = nend + strlen(token);
    }
}
static bool splitWithFormWhisOutLength(const char* pStr, vector<string>& strs)
{
    bool bRet = false;
    
    do
    {
        CC_BREAK_IF(!pStr);
        
        // string is empty
        std::string content = pStr;
        CC_BREAK_IF(content.length() == 0);
        
        int nPosLeft  = content.find('{');
        int nPosRight = content.find('}');
        
        // don't have '{' and '}'
        CC_BREAK_IF(nPosLeft == (int)std::string::npos || nPosRight == (int)std::string::npos);
        // '}' is before '{'
        CC_BREAK_IF(nPosLeft > nPosRight);
        
        std::string pointStr = content.substr(nPosLeft + 1, nPosRight - nPosLeft - 1);
        // nothing between '{' and '}'
        CC_BREAK_IF(pointStr.length() == 0);
        
        int nPos1 = pointStr.find('{');
        int nPos2 = pointStr.find('}');
        // contain '{' or '}'
        CC_BREAK_IF(nPos1 != (int)std::string::npos || nPos2 != (int)std::string::npos);
        
        _split(pointStr, ",", strs);
        for (int i = 0; i < strs.size(); i++)
        {
            if (strs[i].length() == 0)
            {
                strs.clear();
                break;
            }
        }
        
        bRet = true;
    } while (0);
    
    return bRet;
}
Color3B Color3BFromString(const char* pszContent)
{
    Color3B ret = Color3B(0,0,0);
    
    do
    {
        vector<string> strs;
        CC_BREAK_IF(!splitWithFormWhisOutLength(pszContent, strs));
        
        float r  = (float) atof(strs[0].c_str());
        float g = (float) atof(strs[1].c_str());
        float b = (float) atof(strs[2].c_str());
        
        ret = Color3B(r, g, b);
    } while (0);
    
    return ret;
}
Color4B Color4BFromString(const char* pszContent)
{
    Color4B ret = Color4B(0,0,0,1);
    
    do
    {
        vector<string> strs;
        CC_BREAK_IF(!splitWithFormWhisOutLength(pszContent, strs));
        
        float r  = (float) atof(strs[0].c_str());
        float g = (float) atof(strs[1].c_str());
        float b = (float) atof(strs[2].c_str());
        float a = (float) atof(strs[3].c_str());
        
        ret = Color4B(r, g, b, a);
    } while (0);
    
    return ret;
}
__Dictionary* DictionaryFromFile(string fileName)
{
    __Dictionary* t = __Dictionary::createWithContentsOfFileThreadSafe(FileUtils::getInstance()->fullPathForFilename(fileName).c_str());
    t->autorelease();
    return t;
}

string LoadProgSource(string pathToFile, size_t* szFinalLength)
{
    // locals
    FILE* pFileStream = NULL;
    size_t szSourceLength;
    
    if(!(pFileStream = fopen(pathToFile.c_str(), "rb")))
	{
		log("Can't load: %s", pathToFile.c_str());
		return NULL;
	}
    
    // get the length of the source code
    fseek(pFileStream, 0, SEEK_END);
    szSourceLength = ftell(pFileStream);
    fseek(pFileStream, 0, SEEK_SET);
    
    *szFinalLength = szSourceLength;
    
    // allocate a buffer for the source code string and read it in
    char* cSourceString = new char [szSourceLength +  1];
    if (fread((cSourceString), szSourceLength, 1, pFileStream) != 1)
    {
		log("Can't read: %s", pathToFile.c_str());
        fclose(pFileStream);
        free(cSourceString);
        return 0;
    }
    // close the file and return the total length of the combined (preamble + source) string
    fclose(pFileStream);
    if(szFinalLength != 0){ *szFinalLength = szSourceLength; }
    cSourceString[szSourceLength] = '\0';
    
    return string( cSourceString );
}