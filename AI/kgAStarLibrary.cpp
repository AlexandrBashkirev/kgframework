//
//  aStarLibrary.cpp
//  takethetreasures
//
//  Created by mac on 27.02.13.
//
//

#include "kgAStarLibrary.h"
#include "kgUtils.h"


const char walkable = 0, unwalkable = 255;
const int found = 1, nonexistent = 2;

kgAStarPathFinder::kgAStarPathFinder(string mapFileName, int sizeX, int sizeY, float _tileSizeX, float _tileSizeY, Point _centerPos)
{
    mapHeight = sizeY;
    mapWidth = sizeX;
    tileSizeX = _tileSizeX;
    tileSizeY = _tileSizeY;
    centerPos = _centerPos;
    
    walkability  = new char*[mapWidth];
    for (int i = 0; i < mapWidth; i++)
        walkability[i] = new char[mapHeight];
    
    string pathMapFile = FileUtils::getInstance()->fullPathForFilename(mapFileName);
    
    ifstream* mapFile = new ifstream(pathMapFile.c_str(), ios::in | ios::binary);
	
	if(!mapFile->is_open())
	{
		log("error open path map");
		return;
	}
    

    for(int j = mapHeight-1; j >= 0; j--)
        for(int i = 0; i < mapWidth; i++)
        {
            if (!mapFile->eof())
                mapFile->read((char*)&walkability[i][j], sizeof(char));
            else
                log("file path map small");
            
            if(walkability[i][j] > -1)
                walkability[i][j] = unwalkable;
            else
                walkability[i][j] = walkable;
        }
    
    if (!mapFile->eof())
        log("reading not all file path map");
    
    Gcost = new int*[mapWidth+1];
    whichList = new int*[mapWidth+1];
    parentX = new int*[mapWidth+1];
    parentY = new int*[mapWidth+1];
    
    for (int i = 0; i < mapWidth+1; i++) {
        Gcost[i] = new int[mapHeight+1];
        whichList[i] = new int[mapHeight+1];
        parentX[i] = new int[mapHeight+1];
        parentY[i] = new int[mapHeight+1];
    }
    
    openList = new int[mapWidth*mapHeight+2];
    openX = new int[mapWidth*mapHeight+2];
    openY = new int[mapWidth*mapHeight+2];
    Fcost = new int[mapWidth*mapHeight+2];
    Hcost = new int[mapWidth*mapHeight+2];
}
kgAStarPathFinder::~kgAStarPathFinder()
{
    for (int i = 0; i < mapWidth; i++)
        delete walkability[i];
    delete[] walkability;
    
    
    for (int i = 0; i < mapWidth+1; i++)
    {
        delete Gcost[i];
        delete whichList[i];
        delete parentX[i];
        delete parentY[i];
    }
    
    delete[] openList;
    delete[] openX;
    delete[] openY;
    delete[] Fcost;
    delete[] Hcost;
}
Point kgAStarPathFinder::getMapPos(PathTileCoord tilePos)
{
    return Point( tilePos.x*tileSizeX, tilePos.y*tileSizeY);
}
PathTileCoord kgAStarPathFinder::getTilePos(Point pos)
{
    PathTileCoord tmp = PathTileCoord(  (int)((pos.x)/(tileSizeX) + 0.5f),
                                        (int)((pos.y)/(tileSizeY) + 0.5f) +1 );
    
    if (tmp.x < 0)
        tmp.x = 0;
    if (tmp.x >= mapWidth)
        tmp.x = mapWidth -1;
    if (tmp.y < 0)
        tmp.y = 0;
    if (tmp.y >= mapHeight)
        tmp.y = mapHeight -1;
    
    return tmp;
}
void kgAStarPathFinder::addToDraw(Node* parent)
{
    SpriteBatchNode* batchNode = SpriteBatchNode::create("nullImg.png");
    
    for(int i = 0; i < mapWidth; i = i + 1)
        for(int j = 0; j < mapHeight; j = j + 1)
        {
            Sprite* tmp = Sprite::create("nullImg.png");
            tmp->setScaleX(tileSizeX*scaleW);
            tmp->setScaleY(tileSizeY*scaleW);
            
            Point pathCoord = getMapPos(PathTileCoord( i, j ));
            tmp->setPosition( Point(pathCoord.x + tileSizeX*scaleW, pathCoord.y + tileSizeY*scaleW) );
            
            //tmp->setOpacity(255 - (walkability[i][j] + walkability[i+1][j] + walkability[i][j+1] + walkability[i+1][j+1] )/4);
            tmp->setOpacity(walkability[i][j]);
            
            batchNode->addChild(tmp, 2);
        }
    
    parent->addChild(batchNode, 10);
}
vector< PathMapCoord > kgAStarPathFinder::convertTilePathToMapPath(vector< PathTileCoord > tilePath)
{
    vector< PathMapCoord > mapPath;
    
    for(int i = 0; i < tilePath.size(); i++)
    {
        Point pos = getMapPos( tilePath[i] );
        PathMapCoord pmc;
        pmc.x = pos.x;
        pmc.y = pos.y;
        mapPath.insert(mapPath.end(), pmc);
    }
    return mapPath;
}
vector< PathTileCoord > kgAStarPathFinder::FindPath (PathTileCoord startC, PathTileCoord targetC)
{
	int onOpenList=0, parentXval=0, parentYval=0,
	a=0, b=0, m=0, u=0, v=0, temp=0, corner=0, numberOfOpenListItems=0,
	addedGCost=0, tempGcost = 0, path = 0,
	tempx, pathX, pathY,
	newOpenListItemID=0;
    
    int startX = startC.x;
    int startY = startC.y;
    int targetX = targetC.x;
    int targetY = targetC.y;
    
    vector<PathTileCoord> returnPath;
    
    
	if (startX == targetX && startY == targetY)
		return returnPath;
    
    //	If target square is unwalkable, return that it's a nonexistent path.
	if (walkability[targetX][targetY] == unwalkable)
        return returnPath;
    
    //3.Reset some variables that need to be cleared
	if (onClosedList > 1000000) //reset whichList occasionally
	{
		for (int x = 0; x < mapWidth;x++) {
			for (int y = 0; y < mapHeight;y++)
				whichList [x][y] = 0;
		}
		onClosedList = 10;
	}
	onClosedList = onClosedList+2; //changing the values of onOpenList and onClosed list is faster than redimming whichList() array
	onOpenList = onClosedList-1;
	Gcost[startX][startY] = 0; //reset starting square's G value to 0
    
    //4.Add the starting location to the open list of squares to be checked.
	numberOfOpenListItems = 1;
	openList[1] = 1;//assign it as the top (and currently only) item in the open list, which is maintained as a binary heap (explained below)
	openX[1] = startX ; openY[1] = startY;
    
    //5.Do the following until a path is found or deemed nonexistent.
	do
	{
        
        //6.If the open list is not empty, take the first cell off of the list.
        //	This is the lowest F cost cell on the open list.
        if (numberOfOpenListItems != 0)
        {
            
            //7. Pop the first item off the open list.
            parentXval = openX[openList[1]];
            parentYval = openY[openList[1]]; //record cell coordinates of the item
            whichList[parentXval][parentYval] = onClosedList;//add the item to the closed list
            
            //	Open List = Binary Heap: Delete this item from the open list, which
            //  is maintained as a binary heap. For more information on binary heaps, see:
            //	http://www.policyalmanac.org/games/binaryHeaps.htm
            numberOfOpenListItems = numberOfOpenListItems - 1;//reduce number of open list items by 1
            
            //	Delete the top item in binary heap and reorder the heap, with the lowest F cost item rising to the top.
            openList[1] = openList[numberOfOpenListItems+1];//move the last item in the heap up to slot #1
            v = 1;
            
            //	Repeat the following until the new item in slot #1 sinks to its proper spot in the heap.
            do
            {
                u = v;
                if (2*u+1 <= numberOfOpenListItems) //if both children exist
                {
                    //Check if the F cost of the parent is greater than each child.
                    //Select the lowest of the two children.
                    if (Fcost[openList[u]] >= Fcost[openList[2*u]])
                        v = 2*u;
                    if (Fcost[openList[v]] >= Fcost[openList[2*u+1]])
                        v = 2*u+1;
                }
                else
                {
                    if (2*u <= numberOfOpenListItems) //if only child #1 exists
                    {
                        //Check if the F cost of the parent is greater than child #1
                        if (Fcost[openList[u]] >= Fcost[openList[2*u]])
                            v = 2*u;
                    }
                }
                
                if (u != v) //if parent's F is > one of its children, swap them
                {
                    temp = openList[u];
                    openList[u] = openList[v];
                    openList[v] = temp;
                }
                else
                    break; //otherwise, exit loop
                
            }
            while (true);//reorder the binary heap
            
            
            //7.Check the adjacent squares. (Its "children" -- these path children
            //	are similar, conceptually, to the binary heap children mentioned
            //	above, but don't confuse them. They are different. Path children
            //	are portrayed in Demo 1 with grey pointers pointing toward
            //	their parents.) Add these adjacent child squares to the open list
            //	for later consideration if appropriate (see various if statements
            //	below).
            for (b = parentYval-1; b <= parentYval+1; b++)
            {
                for (a = parentXval-1; a <= parentXval+1; a++)
                {
                    
                    //	If not off the map (do this first to avoid array out-of-bounds errors)
                    if (a != -1 && b != -1 && a != mapWidth && b != mapHeight)
                    {
                        
                        //	If not already on the closed list (items on the closed list have
                        //	already been considered and can now be ignored).
                        if (whichList[a][b] != onClosedList)
                        {
                            
                            //	If not a wall/obstacle square.
                            if (walkability [a][b] != unwalkable)
                            {
                                
                                //	Don't cut across corners
                                corner = walkable;
                                if (a == parentXval-1)
                                {
                                    if (b == parentYval-1)
                                    {
                                        if (walkability[parentXval-1][parentYval] == unwalkable
                                            || walkability[parentXval][parentYval-1] == unwalkable) \
                                            corner = unwalkable;
                                    }
                                    else if (b == parentYval+1)
                                    {
                                        if (walkability[parentXval][parentYval+1] == unwalkable
                                            || walkability[parentXval-1][parentYval] == unwalkable)
                                            corner = unwalkable;
                                    }
                                }
                                else if (a == parentXval+1)
                                {
                                    if (b == parentYval-1)
                                    {
                                        if (walkability[parentXval][parentYval-1] == unwalkable
                                            || walkability[parentXval+1][parentYval] == unwalkable)
                                            corner = unwalkable;
                                    }
                                    else if (b == parentYval+1)
                                    {
                                        if (walkability[parentXval+1][parentYval] == unwalkable
                                            || walkability[parentXval][parentYval+1] == unwalkable)
                                            corner = unwalkable;
                                    }
                                }
                                if (corner == walkable)
                                {
                                    
                                    //	If not already on the open list, add it to the open list.
                                    if (whichList[a][b] != onOpenList)
                                    {
                                        
                                        //Create a new open list item in the binary heap.
                                        newOpenListItemID = newOpenListItemID + 1; //each new item has a unique ID #
                                        m = numberOfOpenListItems+1;
                                        openList[m] = newOpenListItemID;//place the new open list item (actually, its ID#) at the bottom of the heap
                                        openX[newOpenListItemID] = a;
                                        openY[newOpenListItemID] = b;//record the x and y coordinates of the new item
                                        
                                        //Figure out its G cost
                                        if (abs(a-parentXval) == 1 && abs(b-parentYval) == 1)
                                            addedGCost = 14;//cost of going to diagonal squares
                                        else
                                            addedGCost = 10;//cost of going to non-diagonal squares
                                        Gcost[a][b] = Gcost[parentXval][parentYval] + addedGCost;
                                        
                                        //Figure out its H and F costs and parent
                                        Hcost[openList[m]] = 10*(abs(a - targetX) + abs(b - targetY));
                                        Fcost[openList[m]] = Gcost[a][b] + Hcost[openList[m]];
                                        parentX[a][b] = parentXval ; parentY[a][b] = parentYval;
                                        
                                        //Move the new open list item to the proper place in the binary heap.
                                        //Starting at the bottom, successively compare to parent items,
                                        //swapping as needed until the item finds its place in the heap
                                        //or bubbles all the way to the top (if it has the lowest F cost).
                                        while (m != 1) //While item hasn't bubbled to the top (m=1)
                                        {
                                            //Check if child's F cost is < parent's F cost. If so, swap them.
                                            if (Fcost[openList[m]] <= Fcost[openList[m/2]])
                                            {
                                                temp = openList[m/2];
                                                openList[m/2] = openList[m];
                                                openList[m] = temp;
                                                m = m/2;
                                            }
                                            else
                                                break;
                                        }
                                        numberOfOpenListItems = numberOfOpenListItems+1;//add one to the number of items in the heap
                                        
                                        //Change whichList to show that the new item is on the open list.
                                        whichList[a][b] = onOpenList;
                                    }
                                    
                                    //8.If adjacent cell is already on the open list, check to see if this
                                    //	path to that cell from the starting location is a better one.
                                    //	If so, change the parent of the cell and its G and F costs.
                                    else //If whichList(a,b) = onOpenList
                                    {
                                        
                                        //Figure out the G cost of this possible new path
                                        if (abs(a-parentXval) == 1 && abs(b-parentYval) == 1)
                                            addedGCost = 14;//cost of going to diagonal tiles
                                        else
                                            addedGCost = 10;//cost of going to non-diagonal tiles
                                        tempGcost = Gcost[parentXval][parentYval] + addedGCost;
                                        
                                        //If this path is shorter (G cost is lower) then change
                                        //the parent cell, G cost and F cost. 		
                                        if (tempGcost < Gcost[a][b]) //if G cost is less,
                                        {
                                            parentX[a][b] = parentXval; //change the square's parent
                                            parentY[a][b] = parentYval;
                                            Gcost[a][b] = tempGcost;//change the G cost			
                                            
                                            //Because changing the G cost also changes the F cost, if
                                            //the item is on the open list we need to change the item's
                                            //recorded F cost and its position on the open list to make
                                            //sure that we maintain a properly ordered open list.
                                            for (int x = 1; x <= numberOfOpenListItems; x++) //look for the item in the heap
                                            {
                                                if (openX[openList[x]] == a && openY[openList[x]] == b) //item found
                                                {
                                                    Fcost[openList[x]] = Gcost[a][b] + Hcost[openList[x]];//change the F cost
                                                    
                                                    //See if changing the F score bubbles the item up from it's current location in the heap
                                                    m = x;
                                                    while (m != 1) //While item hasn't bubbled to the top (m=1)	
                                                    {
                                                        //Check if child is < parent. If so, swap them.	
                                                        if (Fcost[openList[m]] < Fcost[openList[m/2]])
                                                        {
                                                            temp = openList[m/2];
                                                            openList[m/2] = openList[m];
                                                            openList[m] = temp;
                                                            m = m/2;
                                                        }
                                                        else
                                                            break;
                                                    } 
                                                    break; //exit for x = loop
                                                } //If openX(openList(x)) = a
                                            } //For x = 1 To numberOfOpenListItems
                                        }//If tempGcost < Gcost(a,b)
                                        
                                    }//else If whichList(a,b) = onOpenList	
                                }//If not cutting a corner
                            }//If not a wall/obstacle square.
                        }//If not already on the closed list 
                    }//If not off the map
                }//for (a = parentXval-1; a <= parentXval+1; a++){
            }//for (b = parentYval-1; b <= parentYval+1; b++){
            
        }//if (numberOfOpenListItems != 0)
        
        //9.If open list is empty then there is no path.	
        else
        {
            path = nonexistent; break;
        }  
        
        //If target is added to open list then path has been found.
        if (whichList[targetX][targetY] == onOpenList)
        {
            path = found; break;
        }
        
	}
	while (1);//Do until path is found or deemed nonexistent
    
    //10.Save the path if it exists.
	if (path == found)
	{
        pathX = targetX; pathY = targetY;
        do
        {
            tempx = parentX[pathX][pathY];		
            pathY = parentY[pathX][pathY];
            pathX = tempx;
            
            returnPath.insert(returnPath.begin(), PathTileCoord(pathX, pathY));
        }
        while (pathX != startX || pathY != startY);
	}
	return returnPath;
}