#ifndef _ASTAR_PATH_FINDER_
#define _ASTAR_PATH_FINDER_

#include "vector"
#include "string"
using namespace std;

#include "cocos2d.h"
using namespace cocos2d;

struct PathTileCoord
{
    int x;
    int y;
    Sprite* sprite;
    
    PathTileCoord(int _x, int _y)
    {
        x = _x;
        y = _y;
        sprite = NULL;
    }
};

struct PathMapCoord
{
    float x;
    float y;
    Sprite* sprite;
    
    PathMapCoord()
    {
        x = 0;
        y = 0;
        sprite = NULL;
    }
    PathMapCoord(float _x, float _y)
    {
        x = _x;
        y = _y;
        sprite = NULL;
    }
    PathMapCoord(Point p)
    {
        x = p.x;
        y = p.y;
        sprite = NULL;
    }
    
    Point pos()
    {
        return Point(x, y);
    }
    void setPos(Point p)
    {
        x = p.x;
        y = p.y;
    }
};


class kgAStarPathFinder
{
    int mapWidth = 80;
    int mapHeight = 60;
    float tileSizeX;
    float tileSizeY;
    Point centerPos;
    
    int onClosedList = 10;
    
    char** walkability;
    int* openList;
    int** whichList;
    int* openX;
    int* openY;
    int** parentX;
    int** parentY;
    int* Fcost;
    int** Gcost;
    int* Hcost;

    kgAStarPathFinder(){};
    
public:
    
    kgAStarPathFinder(string mapFileName, int sizeX, int sizeY, float tileSizeX, float tileSizeY, Point centerPos);
    ~kgAStarPathFinder();
    
    vector< PathTileCoord > FindPath (PathTileCoord startC, PathTileCoord targetC);
    vector< PathMapCoord > convertTilePathToMapPath(vector< PathTileCoord > tilePath);
    
    void addToDraw(Node* parent);
    
    PathTileCoord getTilePos(Point pos);
    Point getMapPos(PathTileCoord);
};

#endif //_ASTAR_PATH_FINDER_