//
//  kgLabel.cpp
//  Platformer
//
//  Created by flaber on 06.02.14.
//
//

#include "kgLabel.h"

kgLabel * kgLabel::create()
{
    
    kgLabel *pRet = new kgLabel();
    if (pRet && FileUtils::getInstance()->isFileExist("Helvetica"))
    {
        TTFConfig ttfConfig("Helvetica",12,GlyphCollection::DYNAMIC);
        if (pRet->setTTFConfig(ttfConfig))
        {
            pRet->autorelease();
            pRet->setString("");
            return pRet;
        }
    }
    
    CC_SAFE_DELETE(pRet);
    return NULL;
}

kgLabel * kgLabel::create(const char *string, const char *_fontName, float fontSize)
{
    return kgLabel::create(string, _fontName, fontSize,
                              Size::ZERO, TextHAlignment::CENTER, TextVAlignment::CENTER);
}
kgLabel * kgLabel::create(const char *string, const char *_fontName, float fontSize,
                           const Size& dimensions, TextHAlignment hAlignment)

{
    return kgLabel::create(string, _fontName, fontSize, dimensions, hAlignment, TextVAlignment::CENTER);
}
kgLabel * kgLabel::create(const char *string, const char *_fontName, float fontSize,
                           const Size& dimensions, TextHAlignment hAlignment,
                           TextVAlignment vAlignment)
{
    kgLabel *pRet = new kgLabel();
    if (pRet && FileUtils::getInstance()->isFileExist(_fontName))
    {
        TTFConfig ttfConfig(_fontName,fontSize,GlyphCollection::DYNAMIC);
        if (pRet->setTTFConfig(ttfConfig))
        {
            pRet->autorelease();
            pRet->setDimensions(dimensions.width,dimensions.height);
            pRet->setString(string);
            return pRet;
        }
    }
    
    CC_SAFE_DELETE(pRet);
    return NULL;
}

kgLabel::~kgLabel()
{
    
}
bool kgLabel::init(string text, string font, int fontSize)
{
    if (FileUtils::getInstance()->isFileExist(font))
    {
        TTFConfig ttfConfig(font.c_str(), (float)fontSize, GlyphCollection::DYNAMIC);
        if (setTTFConfig(ttfConfig))
        {
            setString(text);
            return true;
        }
    }
    return false;
}
bool kgLabel::init(__Dictionary* data)
{
    string text = string(data->valueForKey("text")->getCString());
    
    string textLocalKey = string(data->valueForKey("textLocalKey")->getCString());
    if(textLocalKey.length() > 0)
        text = LOCALIZED(textLocalKey);
    
    replace(text, "$break", "\n");
    
    fontName = string(data->valueForKey("fontName")->getCString());
    fontSize = data->valueForKey("fontSize")->intValue()*scaleW*2.0f;
    Size dimensions = SizeFromString(data->valueForKey("dimensions")->getCString());
    
    TextHAlignment hAlignment = TextHAlignment::CENTER;
    TextVAlignment vAlignment = TextVAlignment::CENTER;
    
    string hAlignmentStr = string(data->valueForKey("hAlignment")->getCString());
    if(hAlignmentStr.length() > 0)
    {
        if (hAlignmentStr.compare("LEFT") == 0) {
            hAlignment = TextHAlignment::LEFT;
        }else if (hAlignmentStr.compare("CENTER") == 0) {
            hAlignment = TextHAlignment::CENTER;
        }else if (hAlignmentStr.compare("RIGHT") == 0) {
            hAlignment = TextHAlignment::RIGHT;
        }
    }
    
    string vAlignmentStr = string(data->valueForKey("vAlignment")->getCString());
    if(vAlignmentStr.length() > 0)
    {
        if (vAlignmentStr.compare("TOP") == 0) {
            vAlignment = TextVAlignment::TOP;
        }else if (vAlignmentStr.compare("CENTER") == 0) {
            vAlignment = TextVAlignment::CENTER;
        }else if (vAlignmentStr.compare("BOTTOM") == 0) {
            vAlignment = TextVAlignment::BOTTOM;
        }
    }
    
    setAlignment(hAlignment, vAlignment);
    
    if ( FileUtils::getInstance()->isFileExist(fontName))
    {
        TTFConfig ttfConfig(fontName.c_str(),fontSize,GlyphCollection::DYNAMIC);
        if (setTTFConfig(ttfConfig))
        {
            //ret->autorelease();
        }
    }
    setDimensions(dimensions.width*scaleW*2.0f,dimensions.height*scaleH*2.0f);
    setString(text);
    //initWithString(text.c_str(), LOCALIZED(fontName).c_str(), fontSize, dimensions, hAlignment, vAlignment);
    
    setColor(Color3BFromString(data->valueForKey("color")->getCString()));
    
    Point pos = PointFromString(data->valueForKey("position")->getCString());
    if(pos.x != 0 || pos.y != 0)
        setPosition(pos);
    
    kgUINode<Label>::init(data);
    
    /*int borderSize = data->valueForKey("borderSize")->intValue();
    if (borderSize > 0) {
        enableStroke(Color3BFromString(data->valueForKey("borderColor")->getCString()), borderSize, true);
    }*/
    
    return true;
}
void kgLabel::setFontSize(int fs)
{
    fontSize = fs*scaleW*2.0f;
    if ( FileUtils::getInstance()->isFileExist(fontName))
    {
        TTFConfig ttfConfig( fontName.c_str(), fontSize, GlyphCollection::DYNAMIC );
        if (setTTFConfig(ttfConfig))
        {
            //ret->autorelease();
        }
    }
}