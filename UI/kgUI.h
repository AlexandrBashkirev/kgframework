//
//  kgUI.h
//  takethetreasure
//
//  Created by flaber on 15.12.13.
//
//

#ifndef takethetreasure_kgUI_h
#define takethetreasure_kgUI_h

#include "kgWindow.h"
#include "kgViewController.h"
#include "kgScrollView.h"
#include "kgTouchDelegate.h"
#include "kgView.h"
#include "kgButton.h"
#include "kgLabel.h"
#include "kgButtonActivation.h"
#include "kgScaleButton.h"
#include "kgButtonColor.h"
#include "kgSprite.h"
#include "kgProgressBar.h"
#include "kgPopUp.h"
#include "kg9PatchSprite.h"


#endif
