//
//  kgView.cpp
//  takethetreasure
//
//  Created by flaber on 06.12.13.
//
//

#include "kgView.h"
#include "kgButtonInterface.h"
#include "kgUINodeViewHolder.h"
#include "kgViewController.h"

kgView::kgView()
{
    touchDelegateController = this;
    isInQueueForPull = false;
    isDisappearOnsecondLayer = true;
    isAdditionalView = false;
    isAlwaysTouchable = false;
}
__Dictionary* kgView::configFromFile(string filePath)
{
    if (IS_LONG_SCREEN)
    {
        unsigned long n = filePath.find(".");
        string fileName = filePath.substr(0, n);
        
        __Dictionary* config = DictionaryFromFile(fileName+"_LS.plist");
        if(config->count() == 0)
            return DictionaryFromFile(filePath);
        else
            return config;
    }
    return DictionaryFromFile(filePath);
}
kgView* kgView::createFromFile(string filePath)
{
    __Dictionary* config = DictionaryFromFile(filePath);
    
    kgView* view = new kgView();
    
    if (view && view->init(config))
    {
        view->autorelease();
        return view;
    }
    else
    {
        delete view;
        view = NULL;
        return NULL;
    }
}
kgView::~kgView()
{
    
}
bool kgView::init(__Dictionary* data)
{
    if(!kgUINode<Node>::init(data))
        return false;
    
    useLayouts();
    return true;
}


kgViewController* kgView::getViewController()
{
    return viewController;
}

bool kgView::touchBegan(Point pos, int touchId)
{
    bool f = isAlwaysTouchable;
    for (int i = 0; i < buttons.size(); i++)
    {
        if(pButtonTouchValidate->validation(buttons[i]))
            if(buttons[i]->touchBegan(pos, touchId))
                f = true;
    }
    return f;
}
void kgView::touchMoved(Point pos, int touchId)
{
    for (int i = 0; i < buttons.size(); i++)
    {
        if(pButtonTouchValidate->validation(buttons[i]))
            buttons[i]->touchMoved(pos, touchId);
    }
}
void kgView::touchEnded(Point pos, int touchId)
{
    for (int i = 0; i < buttons.size(); i++)
        buttons[i]->touchEnded(pos, touchId);
}
void kgView::touchCancelled(Point pos, int touchId)
{
    for (int i = 0; i < buttons.size(); i++)
        buttons[i]->touchCancelled(pos, touchId);
}