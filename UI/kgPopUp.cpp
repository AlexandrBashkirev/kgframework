//
//  kgPopUp.cpp
//  FairyCraft
//
//  Created by flaber on 15.12.14.
//
//

#include "kgPopUp.h"

bool kgPopUp::init()
{
    isAlwaysTouchable = true;
    back = kgSprite::createWithName("nullImg.png");
    back->setColor(Color3B(0,0,0));
    back->setOpacity(160);
    back->setScale(10000);
    addChild(back, -1);
    
    return true;
}

void kgPopUp::viewWillDisappear()
{
};
void kgPopUp::viewWillAppear()
{
    
    
    setScale(0.5f);
    runAction(Sequence::create(ScaleTo::create(0.15f, 1.1f), ScaleTo::create(0.05f, 1.0f), NULL));
};