//
//  kgWindow.m
//  takethetreasure
//
//  Created by flaber on 06.12.13.
//
//

#include "kgWindow.h"
#include "kgViewController.h"

kgWindow::kgWindow()
{
    if(scene == NULL)
    {
        scene = CCScene::create();
        scene->addChild(this);
    }
    
    CCLayer::init();
    auto listener = EventListenerTouchAllAtOnce::create();
    listener->onTouchesBegan     = [](const std::vector<Touch*>& touches, Event* event) { kgWindow::instance().onTouchesBegan(touches, event);  };
    listener->onTouchesMoved     = [](const std::vector<Touch*>& touches, Event* event) { kgWindow::instance().onTouchesMoved(touches, event);  };
    listener->onTouchesEnded     = [](const std::vector<Touch*>& touches, Event* event) { kgWindow::instance().onTouchesEnded(touches, event);  };
    listener->onTouchesCancelled = [](const std::vector<Touch*>& touches, Event* event) { kgWindow::instance().onTouchesCancelled(touches, event);  };

    // Or the priority of the touch listener is a fixed value
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 100);
}
kgWindow::~kgWindow()
{
    
}

bool kgWindow::onToucheBegan(Touch *touch, Event *unused_event)
{
    return false;
}

void kgWindow::onTouchesBegan(const std::vector<Touch*>& touches, Event *unused_event)
{
    if(viewControllers.size() == 0) return;
    
    for( int i = 0; i < touches.size(); i++ )
    {
        Touch* touch = touches[i];
        
        viewControllers[viewControllers.size()-1]->touchBegan(touch->getLocation(), touch->getID());
    }
}
void kgWindow::onTouchesMoved(const std::vector<Touch*>& touches, Event *unused_event)
{
    if(viewControllers.size() == 0) return;
    
    kgViewController* vc = viewControllers[viewControllers.size()-1];
    
    vc->retain();
    for( int i = 0; i < touches.size(); i++ )
    {
        Touch* touch = touches[i];
        vc->touchMoved(touch->getLocation(), touch->getID());
    }
    vc->release();
}
void kgWindow::onTouchesEnded(const std::vector<Touch*>& touches, Event *unused_event)
{
    if(viewControllers.size() == 0) return;
    
    kgViewController* vc = viewControllers[viewControllers.size()-1];
    
    vc->retain();
    for( int i = 0; i < touches.size(); i++ )
    {
        Touch* touch = touches[i];
        vc->touchEnded(touch->getLocation(), touch->getID());
    }
    vc->release();
}
void kgWindow::onTouchesCancelled(const std::vector<Touch*>& touches, Event *unused_event)
{
    if(viewControllers.size() == 0) return;
    
    kgViewController* vc = viewControllers[viewControllers.size()-1];

    vc->retain();
    for( int i = 0; i < touches.size(); i++ )
    {
        Touch* touch = touches[i];
        vc->touchCancelled(touch->getLocation(), touch->getID());
    }
    vc->release();
}
void kgWindow::pushViewController(kgViewController* vc)
{
    vc->viewContollerWillAppear();
    viewControllers.push_back(vc);
    this->addChild(vc);
    vc->viewContollerDidAppear();
}
void kgWindow::pullViewController(kgViewController* vc)
{
    vc->viewContollerWillDisappear();
    vc->viewContollerDidDisappear();
    vc->removeFromParentAndCleanup(true);
    viewControllers.erase(std::remove(viewControllers.begin(), viewControllers.end(), vc), viewControllers.end());
}
void kgWindow::replaceViewController(kgViewController* vc)
{
    if(viewControllers.size() > 0)
        pullViewController(viewControllers[viewControllers.size()-1]);
    pushViewController( vc );
}
kgViewController* kgWindow::getTopViewController()
{
    if(viewControllers.size() == 0)
        return nullptr;
    return viewControllers[viewControllers.size()-1];
}
void kgWindow::replaceViewController(kgViewController* vc, kgTranslation t)
{
    
}