//
//  kgWindow.h
//  takethetreasure
//
//  Created by flaber on 06.12.13.
//
//

#ifndef __takethetreasure__kgWindow__
#define __takethetreasure__kgWindow__

#include "kgUtils.h"
#include "kgTranslation.h"

class kgViewController;
class kgWindow : public Layer
{
    Scene *scene;
    vector< kgViewController* > viewControllers;
    
    bool onToucheBegan(Touch *touch, Event *unused_event);
    
    
    void onTouchesBegan(const std::vector<Touch*>& touches, Event *unused_event);
    void onTouchesMoved(const std::vector<Touch*>& touches, Event *unused_event);
    void onTouchesEnded(const std::vector<Touch*>& touches, Event *unused_event);
    void onTouchesCancelled(const std::vector<Touch*>& touches, Event *unused_event);
    
    kgWindow();
    ~kgWindow();
    
public:
    
    void pushViewController(kgViewController* vc);
    void pullViewController(kgViewController* vc);
    void replaceViewController(kgViewController* vc);
    void replaceViewController(kgViewController* vc, kgTranslation t);
    
    kgViewController* getTopViewController();
    
    Scene * getScene(){ return scene; };
    
    static kgWindow& instance()
	{
		static kgWindow obj;
		return obj;
	}
};

#endif // __takethetreasure__kgWindow__
