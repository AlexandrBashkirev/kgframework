//
//  kgTouchDelegate.h
//  FairyCraft
//
//  Created by flaber on 25.11.14.
//
//

#ifndef FairyCraft_kgTouchDelegate_h
#define FairyCraft_kgTouchDelegate_h

#include "kgUtils.h"

class kgTouchDelegate
{
protected:
    bool isEnabled;
    
    int touchId;

public:
    kgTouchDelegate(){isEnabled = true;};
    
    virtual bool touchBegan(Point pos, int touchId) = 0;
    virtual void touchMoved(Point pos, int touchId) = 0;
    virtual void touchEnded(Point pos, int touchId) = 0;
    virtual void touchCancelled(Point pos, int touchId) = 0;
    virtual Rect getTouchBox() = 0;
    
    bool getIsEnabled(){return isEnabled;};
    void setIsEnabled(bool e)
    {
        isEnabled = e;
    };
    
    int getTouchId(){return touchId;};
    void setTouchId(int t){touchId = t;};
};

class kgButtonInterface;

typedef void (CCObject::*buttonSelector)(kgButtonInterface*);
#define buttonSelector(_SELECTOR) (buttonSelector)(&_SELECTOR)



struct buttonDelegate {
    Object* target;
    buttonSelector selector;
    
    buttonDelegate(Object* _target, buttonSelector _selector)
    {
        target = _target;
        selector = _selector;
    }
};

class kgView;
class kgTouchDelegateController
{
protected:
    map<string, Node*> strTags;
    kgTouchDelegate* activeBtn;
    vector<kgTouchDelegate*> buttons;
    map<string, buttonDelegate> buttonDelegats;
    
public:
    void callButtonFunc(kgButtonInterface* btn, string signal);
    void registerButtonSelector(string signal, buttonSelector bs);
    void registerButtonSelector(string signal, Ref* target, buttonSelector bs);
    
    void addTouchable(kgTouchDelegate* btn);
    void removeTouchable(kgTouchDelegate* btn);
    
    void registerTag(string st, Node* node);
    void unregisterTag(string st);
    Node* getNodeForTag(string st);
    
    kgButtonInterface* getButtonBySignal(string sig, int tag = -1);
    
    friend class kgView;
};

class ButtonTouchValidate
{
    std::function<bool(kgTouchDelegate*)> validationFunc;
    
public:
    
    ButtonTouchValidate();
    ~ButtonTouchValidate();
    
    static ButtonTouchValidate* instanse();
    
    void dropValidFunc();
    void setValidFunc(std::function<bool(kgTouchDelegate*)> _vf);
    
    bool validation(kgTouchDelegate* b);
};
#define pButtonTouchValidate ButtonTouchValidate::instanse()
#endif
