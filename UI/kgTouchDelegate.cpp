//
//  kgTouchDelegate.cpp
//  FairyCraft
//
//  Created by flaber on 12.12.14.
//
//

#include "kgTouchDelegate.h"
#include "kgButtonInterface.h"

void kgTouchDelegateController::callButtonFunc(kgButtonInterface* btn, string signal)
{
    if(signal.length() == 0)
    {
        CCLOG("registerButtonSelector signal:%s alredy added", signal.c_str());
        return;
    }
    
    map<string, buttonDelegate>::iterator it = buttonDelegats.find(signal);
    if(it != buttonDelegats.end())
        (it->second.target->*it->second.selector)(btn);
}
void kgTouchDelegateController::registerButtonSelector(string signal, buttonSelector bs)
{
    registerButtonSelector( signal, (Ref*)this,  bs);
}
void kgTouchDelegateController::registerButtonSelector(string signal, Ref* target, buttonSelector bs)
{
    map<string, buttonDelegate>::iterator it = buttonDelegats.find(signal);
    if(it == buttonDelegats.end())
        buttonDelegats.insert(pair<string, buttonDelegate>(signal, buttonDelegate(target, bs)));
    else
        CCLOG("registerButtonSelector signal:%s alredy added", signal.c_str());
}
void kgTouchDelegateController::addTouchable(kgTouchDelegate* btn)
{
    buttons.push_back(btn);
}
kgButtonInterface* kgTouchDelegateController::getButtonBySignal(string sig, int tag)
{
    for (int i = 0; i < buttons.size(); i++)
    {
        kgButtonInterface* bi = dynamic_cast<kgButtonInterface*>(buttons[i]);
        if(bi != nullptr)
        {
            if(bi->getSignal().compare(sig) == 0 && (tag == -1 || tag == bi->getTag()))
                return bi;
        }
    }
    return nullptr;
}
void kgTouchDelegateController::removeTouchable(kgTouchDelegate* btn)
{
    buttons.erase(std::remove(buttons.begin(), buttons.end(), btn), buttons.end());
}
void kgTouchDelegateController::registerTag(string st, Node* node)
{
    map<string, Node*>::iterator it = strTags.find(st);
    if(it == strTags.end())
        strTags.insert(pair<string, Node*>(st, node));
    else
        CCLOG("registerTag strTag:%s alredy added", st.c_str());
}
void kgTouchDelegateController::unregisterTag(string st)
{
    strTags.erase(st);
}

Node* kgTouchDelegateController::getNodeForTag(string st)
{
    map<string, Node*>::iterator it = strTags.find(st);
    if(it == strTags.end())
        return NULL;
    else
        return it->second;
}
ButtonTouchValidate::ButtonTouchValidate()
{
    dropValidFunc();
}
ButtonTouchValidate* ButtonTouchValidate::instanse()
{
    static ButtonTouchValidate* inst = new ButtonTouchValidate();
    return inst;
}
void ButtonTouchValidate::dropValidFunc()
{
    setValidFunc([](kgTouchDelegate*){return true;});
}
void ButtonTouchValidate::setValidFunc(std::function<bool(kgTouchDelegate*)> _vf)
{
    validationFunc = _vf;
}
bool ButtonTouchValidate::validation(kgTouchDelegate* b)
{
    return validationFunc(b);
}