//
//  kgLayout.h
//  takethetreasure
//
//  Created by flaber on 24.01.14.
//
//

#ifndef __takethetreasure__kgLayout__
#define __takethetreasure__kgLayout__

#include "kgUtils.h"


struct kgLayout
{
    bool relativToScreen;

    bool noScale;
    
    bool absPos;
    bool percentPos;
    float posX;
    float posY;
    
    bool ancher;
    float AnchX;
    float AnchY;
    
    
    bool absHeightScale;
    bool percentHeightScale;
    bool absWidthScale;
    bool percentWidthScale;
    float height;
    float width;
    
    bool reflectX;
    bool reflectY;
    
    float widthHeightCorelation;
    
    bool dontUseTopScale;
    
    bool onlyBigestScale;
    
    kgLayout(__Dictionary* d);
    
    void use(Node* a, Node* parent);
};


#endif /* defined(__takethetreasure__kgLayout__) */
