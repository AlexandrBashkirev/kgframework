//
//  kgLabel.h
//  Platformer
//
//  Created by flaber on 06.02.14.
//
//

#ifndef __Platformer__kgLabel__
#define __Platformer__kgLabel__

#include "kgUtils.h"
#include "kgUINode.h"


class kgLabel : public kgUINode<Label>
{
    string fontName;
    int fontSize;
public:
    ~kgLabel();
    
    static kgLabel * create();
    static kgLabel * create(const char *string, const char *_fontName, float fontSize);
    static kgLabel * create(const char *string, const char *_fontName, float fontSize,
                               const Size& dimensions, TextHAlignment hAlignment);
    static kgLabel * create(const char *string, const char *_fontName, float fontSize,
                               const Size& dimensions, TextHAlignment hAlignment,
                               TextVAlignment vAlignment);
    
    bool init(__Dictionary* data);
    bool init(string text, string font, int size);
    
    void setFontSize(int fs);
};

namespace
{
	Node* CreateLabel()
	{
        Node* c = (Node*) new kgLabel() ;
        c->autorelease();
		return c;
	}
	const bool insertCreateLabel = pUINodeFactory.RegisterStrCallBacks("kgLabel", CreateLabel);
}

#endif /* defined(__Platformer__kgLabel__) */
