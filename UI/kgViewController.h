//
//  kgViewController.h
//  takethetreasure
//
//  Created by flaber on 06.12.13.
//
//

#ifndef __takethetreasure__kgViewController__
#define __takethetreasure__kgViewController__

#include "cocos2d.h"
using namespace cocos2d;

#include <vector>
using namespace std;

class kgView;
class kgViewController : public Node
{
    vector<kgView*> currentTouchedViews;
    vector<kgView*> justAddedViews;
    vector<kgView* > views;
    
    Vector<Node*> pausedTargats;
    bool _isPaused;
public:
    
    kgViewController();
    ~kgViewController();
    
    static kgViewController* create();
    
    void touchBegan(Point pos, int touchId);
    void touchMoved(Point pos, int touchId);
    void touchEnded(Point pos, int touchId);
    void touchCancelled(Point pos, int touchId);
    
    void pushView(kgView* v);
    void pullView(kgView* v);
    void clearViews();
    
    virtual void viewContollerWillDisappear(){};
    virtual void viewContollerDidDisappear(){};
    virtual void viewContollerWillAppear(){};
    virtual void viewContollerDidAppear(){};
    
    virtual void update(float dt);
    void pause(bool p);
    bool isPaused();
};

#endif /* defined(__takethetreasure__kgViewController__) */
