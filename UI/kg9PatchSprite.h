//
//  kg9PatchSprite.h
//  FairyCraft
//
//  Created by flaber on 15.12.14.
//
//

#ifndef __FairyCraft__kg9PatchSprite__
#define __FairyCraft__kg9PatchSprite__

#include "kgUtils.h"
#include "kgUINode.h"
#include "kgAbstractFactory.h"
#include "kgSprite.h"

class kg9PatchSprite : public kgUINode<Node>
{
    kgSprite* lt;
    kgSprite* ct;
    kgSprite* rt;
    
    kgSprite* lm;
    kgSprite* cm;
    kgSprite* rm;
    
    kgSprite* lb;
    kgSprite* cb;
    kgSprite* rb;
    
    float backImgScale;
    string name;
    
    
public:
    
    void usedLayouts();
    
    static kg9PatchSprite* create(string name, float backImgScale);
    bool init(string name, float backImgScale);
    
    bool init(__Dictionary* info);
};

namespace
{
    Node* Create9PatchSprite()
    {
        Node* c = (Node*) new kg9PatchSprite() ;
        c->autorelease();
        return c;
    }
    const bool insertCreate9PatchSprite = pUINodeFactory.RegisterStrCallBacks("kg9PatchSprite", Create9PatchSprite);
}

#endif /* defined(__FairyCraft__kg9PatchSprite__) */
