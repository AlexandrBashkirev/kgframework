//
//  kg9PatchSprite.cpp
//  FairyCraft
//
//  Created by flaber on 15.12.14.
//
//

#include "kg9PatchSprite.h"

kg9PatchSprite* kg9PatchSprite::create(string name, float backImgScale)
{
    kg9PatchSprite *pobSprite = new kg9PatchSprite();
    if (pobSprite && pobSprite->init(name, backImgScale))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}
void kg9PatchSprite::usedLayouts()
{
    Texture2D::TexParams params;
    params.minFilter = GL_LINEAR;
    params.magFilter = GL_LINEAR;
    params.wrapS = GL_REPEAT;
    params.wrapT = GL_REPEAT;
    
    Size cs = getContentSize();
    float scale = scaleW*backImgScale;
    
    
    // left top
    lt = kgSprite::createWithName(string(name + "_lt.png"));
    lt->setScale(scale);
    lt->setPosition(Point( lt->getContentSize().width*scale/2,
                          cs.height - lt->getContentSize().height*scale/2));
    addChild(lt);
    
    // right top
    rt = kgSprite::createWithName(string(name + "_rt.png"));
    rt->setPosition(Point(cs.width - rt->getContentSize().width*scale/2,
                          cs.height - rt->getContentSize().height*scale/2));
    rt->setScale(scale);
    addChild(rt);
    
    // left bottom
    lb = kgSprite::createWithName(string(name + "_lb.png"));
    lb->setScale(scale);
    lb->setPosition(Point( lb->getContentSize().width*scale/2,
                          lb->getContentSize().height*scale/2));
    addChild(lb);
    
    //  right bottom
    rb = kgSprite::createWithName(string(name + "_rb.png"));
    rb->setScale(scale);
    rb->setPosition(Point(cs.width - rb->getContentSize().width*scale/2,
                          rb->getContentSize().height*scale/2));
    addChild(rb);


    // centr top
    ct = kgSprite::createWithName(string(name + "_ct.png"));
    float repiatX = (cs.width - lt->getContentSize().width*scale*2)/ct->getContentSize().width/scale;
    
    ct->setScale(scale);
    ct->setAnchorPoint(Point(0, 0.5f));
    ct->setPosition(Point(ct->getContentSize().width*scale,
                          cs.height - ct->getContentSize().height*scale/2));
    ct->getTexture()->setTexParameters(params);
    ct->setTextureRect(Rect(0, 0, ct->getContentSize().width*repiatX, ct->getContentSize().height));
    addChild(ct);
    

    //
    // |
    lm = kgSprite::createWithName(string(name + "_lm.png"));
    
    float repeatY = (cs.height - rt->getContentSize().height*scale*2)/lm->getContentSize().height/scale;
    lm->setScale(scale);
    lm->setAnchorPoint(Point(0.5f, 0));
    lm->setPosition(Point(lm->getContentSize().width*scale/2,
                          rt->getContentSize().height*scale));
    lm->getTexture()->setTexParameters(params);
    lm->setTextureRect(Rect(0, 0, lm->getContentSize().width, lm->getContentSize().height*repeatY));
    addChild(lm);
    
    
    //  centr right
    rm = kgSprite::createWithName(string(name + "_rm.png"));
    rm->setScale(scale);
    rm->setAnchorPoint(Point(0.5f, 0));
    rm->setPosition(Point(cs.width - rm->getContentSize().width*scale/2,
                          rt->getContentSize().height*scale));
    rm->getTexture()->setTexParameters(params);
    rm->setTextureRect(Rect(0, 0, rm->getContentSize().width, rm->getContentSize().height*repeatY));
    addChild(rm);
    
    
    //  centr bottom
    cb = kgSprite::createWithName(string(name + "_cb.png"));
    cb->setScale(scale);
    cb->setAnchorPoint(Point(0, 0.5f));
    cb->setPosition(Point(cb->getContentSize().width*scale,
                        cb->getContentSize().height*scale/2));
    cb->getTexture()->setTexParameters(params);
    cb->setTextureRect(Rect(0, 0, cb->getContentSize().width*repiatX, cb->getContentSize().height));
    addChild(cb);
    
    
    //
    // center
    cm = kgSprite::createWithName(string(name + "_cm.png"));
    cm->setScale(scale);
    cm->setAnchorPoint(Point(0, 0));
    cm->setPosition(Point(lb->getContentSize().width*scale,
                          lb->getContentSize().height*scale));
    cm->getTexture()->setTexParameters(params);
    cm->setTextureRect(Rect(0, 0, cm->getContentSize().width*repiatX, cm->getContentSize().height*repeatY));
    addChild(cm);
}
bool kg9PatchSprite::init(string _name, float _backImgScale)
{
    name = _name;
    backImgScale = _backImgScale;
    
    return true;
}

bool kg9PatchSprite::init(__Dictionary* data)
{
    kgUINode<Node>::init(data);
    
    return init(string(data->valueForKey("backImgName")->getCString()),
                data->valueForKey("scale")->doubleValue());
}