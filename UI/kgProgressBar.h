//
//  kgProgressBar.h
//  FairyCraft
//
//  Created by flaber on 08.11.14.
//
//

#ifndef __FairyCraft__kgProgressBar__
#define __FairyCraft__kgProgressBar__

#include "kgUtils.h"
#include "kgUI.h"
#include "kgAbstractFactory.h"

class kgProgressBar : public kgUINode<Sprite>
{
    string backImgName;
    string fillerImgName;
    
    
    
public:
    kgSprite* filler;
    ~kgProgressBar();
    
    bool init(__Dictionary* data);
    bool init(string _backImgName, string _fillerImgName);
    
    void setProgress(int p);
};

namespace
{
    Node* CreateProgressBar()
    {
        Node* c = (Node*) new kgProgressBar() ;
        c->autorelease();
        return c;
    }
    const bool insertCreateProgressBar = pUINodeFactory.RegisterStrCallBacks("kgProgressBar", CreateProgressBar);
}

#endif /* defined(__FairyCraft__kgProgressBar__) */
