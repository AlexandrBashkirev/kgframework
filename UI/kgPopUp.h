//
//  kgPopUp.h
//  FairyCraft
//
//  Created by flaber on 15.12.14.
//
//

#ifndef __FairyCraft__kgPopUp__
#define __FairyCraft__kgPopUp__

#include "kgView.h"
#include "kgSprite.h"

class kgPopUp : public kgView
{
    kgSprite* back;
    
    
public:
    bool init();
    void viewWillDisappear();
    void viewWillAppear();
};

#endif /* defined(__FairyCraft__kgPopUp__) */
