//
//  kgSprite.cpp
//  takethetreasure
//
//  Created by flaber on 18.12.13.
//
//

#include "kgSprite.h"
/*
kgSprite* kgSprite::createWithTexture(Texture2D *pTexture)
{
    kgSprite *pobSprite = new kgSprite();
    if (pobSprite && pobSprite->initWithTexture(pTexture))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}
kgSprite* kgSprite::createWithTexture(Texture2D *pTexture, const Rect& rect)
{
    kgSprite *pobSprite = new kgSprite();
    if (pobSprite && pobSprite->initWithTexture(pTexture, rect))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}
kgSprite* kgSprite::create(const char *pszFileName)
{
    kgSprite *pobSprite = new kgSprite();
    if (pobSprite && pobSprite->initWithFile(pszFileName))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}
kgSprite* kgSprite::create(const char *pszFileName, const Rect& rect)
{
    kgSprite *pobSprite = new kgSprite();
    if (pobSprite && pobSprite->initWithFile(pszFileName, rect))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}
kgSprite* kgSprite::createWithSpriteFrame(SpriteFrame *pSpriteFrame)
{
    kgSprite *pobSprite = new kgSprite();
    if (pSpriteFrame && pobSprite && pobSprite->initWithSpriteFrame(pSpriteFrame))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}
kgSprite* kgSprite::createWithSpriteFrameName(const char *pszSpriteFrameName)
{
    SpriteFrame *pFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(pszSpriteFrameName);
    
#if COCOS2D_DEBUG > 0
    char msg[256] = {0};
    sprintf(msg, "Invalid spriteFrameName: %s", pszSpriteFrameName);
    CCAssert(pFrame != NULL, msg);
#endif
    
    return createWithSpriteFrame(pFrame);
}
*/
kgSprite* kgSprite::createWithName(string spriteName)
{
    kgSprite *pobSprite = new kgSprite();
    if (pobSprite && pobSprite->init(spriteName))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}
kgSprite::~kgSprite()
{
    
}
void kgSprite::setScale(float scale)
{
    Node::setScale( scale);
}
bool kgSprite::init(__Dictionary* data)
{
    kgUINode<Sprite>::init(data);
    
    spriteName = string(data->valueForKey("spriteName")->getCString());
    kgSprite::init(spriteName);
    
    Point pos = PointFromString(data->valueForKey("position")->getCString());
    if(pos.x != 0 || pos.y != 0)
        setPosition(pos);
    
    if(notTextureFiltering)
        getTexture()->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE });
    
    if(string(data->valueForKey("color")->getCString()).length() > 0)
        setColor(Color3BFromString(data->valueForKey("color")->getCString()));
    if(string(data->valueForKey("opacity")->getCString()).length() > 0)
        setOpacity( data->valueForKey("opacity")->intValue() );
    
    return true;
}
bool kgSprite::init(string _spriteName)
{
    kgUINode<Sprite>::init(__Dictionary::create());
    
    spriteName = _spriteName;
    
    SpriteFrame *pFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(_spriteName);
    
    if(pFrame == nullptr)
        initWithFile(_spriteName);
    else
        initWithSpriteFrame(pFrame);
    
    return true;
}

