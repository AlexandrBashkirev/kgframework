//
//  kgScaleButton.h
//  PVO
//
//  Created by flaber on 16.03.14.
//
//

#ifndef __PVO__kgScaleButton__
#define __PVO__kgScaleButton__

#include "kgUtils.h"
#include "kgUINode.h"
#include "kgButtonInterface.h"

class kgScaleButton : public kgButtonInterface
{
    SpriteFrame* normalFrame;
    SpriteFrame* disabledFrame;
    
    float activeScale;
    float normalScaleX;
    float normalScaleY;
    
public:
    
    ~kgScaleButton();
    static kgScaleButton* create(string normalImg, string _signal);
    static kgScaleButton* create(string normalImg, string disabled, string _signal);
    
    bool init(string normalImg, string disabled, string _signal);
    
    void setNormalSprite(string s);
    void setDisabledSprite(string s);
    void setActiveScale(float f);
    
    bool init(__Dictionary* data);
    
    void touchDown(int _touchId);
    void selected(int _touchId);
    void lost(int _touchId);
    void pressed(int _touchId);
};

namespace
{
	Node* CreateScaleButton()
	{
        Node* c = (Node*) new kgScaleButton() ;
        c->autorelease();
		return c;
	}
	const bool insertCreateScaleButton = pUINodeFactory.RegisterStrCallBacks("kgScaleButton", CreateScaleButton);
}

#endif /* defined(__PVO__kgScaleButton__) */
