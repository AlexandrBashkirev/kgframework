//
//  kgButtonInterface.h
//  takethetreasure
//
//  Created by flaber on 17.12.13.
//
//

#ifndef __takethetreasure__kgButtonInterface__
#define __takethetreasure__kgButtonInterface__

#include "kgUINode.h"
#include "cocos2d.h"
#include "kgTouchDelegate.h"

using namespace cocos2d;

class kgButtonInterface : public kgUINode<Sprite>, public kgTouchDelegate
{
    static kgButtonInterface* activeBtn;
    static bool isButtonsActive;
    
protected:
    string signal;
    
    bool _isActive;
    bool isActive();
    void setActive(bool a);
    
    string clickSound;
    
public:

    bool touchBegan(Point pos, int touchId);
    void touchMoved(Point pos, int touchId);
    void touchEnded(Point pos, int touchId);
    void touchCancelled(Point pos, int touchId);
    
    Rect getTouchBox(){return boundingBox();};
    
    
    string getSignal(){return signal;};
    void setSignal(string s){signal = s;};
    
    kgButtonInterface(){};
    virtual ~kgButtonInterface(){};
    
    virtual void touchDown(int _touchId) = 0;
    virtual void selected(int _touchId) = 0;
    virtual void lost(int _touchId) = 0;
    virtual void pressed(int _touchId) = 0;
    
    
    
    void hide()
    {
        setVisible(false);
        isEnabled = false;
    };
    void show()
    {
        setVisible(true);
        isEnabled = true;
    };
    
    static void setButtonsEnabled(bool t);
    
};

#endif /* defined(__takethetreasure__kgButtonInterface__) */
