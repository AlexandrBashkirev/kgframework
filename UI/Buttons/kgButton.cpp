//
//  kgButton.cpp
//  takethetreasure
//
//  Created by flaber on 18.12.13.
//
//

#include "kgButton.h"
#include "kgView.h"

kgButton* kgButton::create(string normalImg, string _signal)
{
    return kgButton::create( normalImg,  "", "", _signal);
}
kgButton* kgButton::create(string normalImg, string selected, string _signal)
{
    return kgButton::create( normalImg,  selected, "", _signal);
}
kgButton* kgButton::create(string normalImg, string selected, string disabled, string _signal)
{
    kgButton *pRet = new kgButton();
    if (pRet && pRet->init( normalImg, selected, disabled, _signal))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}
bool kgButton::init(__Dictionary* data)
{
    kgButtonInterface::init(data);
    
    string normImgName = string(data->valueForKey("normalSprite")->getCString());
    string selImgName = string(data->valueForKey("selectedSprite")->getCString());
    string disabledImgName = string(data->valueForKey("disabledSprite")->getCString());
    string sig = string(data->valueForKey("signal")->getCString());
    
    clickSound = string(data->valueForKey("clickSound")->getCString());
    if(clickSound.length() != 0)
        SimpleAudioEngine::getInstance()->preloadEffect(clickSound.c_str());
    
    init(normImgName, selImgName, disabledImgName, sig);
    
    Point pos = PointFromString(data->valueForKey("position")->getCString());
    if(pos.x != 0 || pos.y != 0)
        setPosition(pos);

    return true;
}
bool kgButton::init(string normalImg, string selected, string disabled, string _signal)
{
    normalFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(normalImg);
    
    if(normalFrame == nullptr)
    {
        normalFrame = SpriteFrame::create(normalImg, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(normalFrame, normalImg);
    }
    
    selectedFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(selected);
    
    if(selectedFrame == nullptr)
    {
        selectedFrame = SpriteFrame::create(selected, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(selectedFrame, selected);
    }
    
    disabledFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(disabled);
    
    if(disabledFrame == nullptr)
    {
        disabledFrame = SpriteFrame::create(disabled, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(disabledFrame, disabled);
    }
    
    kgUINode<Sprite>::initWithSpriteFrame(normalFrame);
    
    if(notTextureFiltering)
    {
        getTexture()->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE });
    }
    signal = _signal;
    /*
    if(selected != "")
    {
        selectedSprite = CCSprite::create(FileUtils::getInstance()->fullPathForFilename(selected).c_str());
        selectedSprite->setVisible(false);
        addChild(selectedSprite);
        
        if(notTextureFiltering)
        {
            selectedSprite->getTexture()->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE });
        }
    }
    
    if(disabled != "")
    {
        disbledSprite = CCSprite::create(FileUtils::getInstance()->fullPathForFilename(disabled).c_str());
        disbledSprite->setVisible(false);
        addChild(disbledSprite);
        
        if(notTextureFiltering)
        {
            disbledSprite->getTexture()->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE });
        }
    }
    */
    return true;
}
void kgButton::setNormalImg(string normalImg)
{
    normalFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(normalImg);
    
    if(normalFrame == nullptr)
    {
        normalFrame = SpriteFrame::create(normalImg, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(normalFrame, normalImg);
    }
}
void kgButton::setSelectedImg(string selectedImg)
{
    selectedFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(selectedImg);
    
    if(selectedFrame == nullptr)
    {
        selectedFrame = SpriteFrame::create(selectedImg, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(selectedFrame, selectedImg);
    }
}
void kgButton::setDisabledImg(string disabledImg)
{
    disabledFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(disabledImg);
    
    if(disabledFrame == nullptr)
    {
        disabledFrame = SpriteFrame::create(disabledImg, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(disabledFrame, disabledImg);
    }
}
void kgButton::touchDown(int _touchId)
{
    touchId = _touchId;
    setSpriteFrame(selectedFrame);
    useChildLayouts();
    
    if(clickSound.length() != 0)
        SimpleAudioEngine::getInstance()->playEffect( clickSound.c_str() );
}
void kgButton::selected(int _touchId)
{
    setSpriteFrame(selectedFrame);
    useChildLayouts();
}
void kgButton::lost(int _touchId)
{
    setSpriteFrame(normalFrame);
    useChildLayouts();
}
void kgButton::pressed(int _touchId)
{
    setSpriteFrame(normalFrame);
    useChildLayouts();
    
    //touchId = -1;
    touchDelegateController->callButtonFunc(this, signal);
}






