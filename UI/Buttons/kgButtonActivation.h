//
//  kgButtonActivation.h
//  takethetreasure
//
//  Created by flaber on 21.12.13.
//
//

#ifndef __takethetreasure__kgButtonActivation__
#define __takethetreasure__kgButtonActivation__

#include "kgUtils.h"
#include "kgUINode.h"
#include "kgButtonInterface.h"

class kgButtonActivation : public kgButtonInterface
{
    //string normalSprite;
    //string activeSprite;
    
    SpriteFrame* normalFrame;
    SpriteFrame* activeFrame;
    
    
    
    bool init(string normalImg, string active, string _signal);
public:
    
    static kgButtonActivation* create(string normalImg, string active, string _signal);
    
    bool init(__Dictionary* data);
    
    void touchDown(int _touchId);
    void selected(int _touchId);
    void lost(int _touchId);
    void pressed(int _touchId);
    
    void setActive(bool a);
    
};

namespace
{
	Node* CreateButtonActivation()
	{
        Node* c = (Node*) new kgButtonActivation() ;
        c->autorelease();
		return c;
	}
	const bool insertCreateButtonA = pUINodeFactory.RegisterStrCallBacks("kgButtonActivation", CreateButtonActivation);
}
#endif /* defined(__takethetreasure__kgButtonActivation__) */
