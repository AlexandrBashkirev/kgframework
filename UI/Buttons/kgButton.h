//
//  kgButton.h
//  takethetreasure
//
//  Created by flaber on 17.12.13.
//
//

#ifndef __takethetreasure__kgButton__
#define __takethetreasure__kgButton__

#include "kgUtils.h"
#include "kgUINode.h"
#include "kgButtonInterface.h"

class kgButton : public kgButtonInterface
{

private:
    
    SpriteFrame* normalFrame;
    SpriteFrame* selectedFrame;
    SpriteFrame* disabledFrame;
    
    
    
    bool init(string normalImg, string selected, string disabled, string _signal);
public:
    
    static kgButton* create(string normalImg, string _signal);
    static kgButton* create(string normalImg, string selected, string _signal);
    static kgButton* create(string normalImg, string selected, string disabled, string _signal);
    
    void setNormalImg(string normalImg);
    void setSelectedImg(string selectedImg);
    void setDisabledImg(string disabledImg);
    
    bool init(__Dictionary* data);
    
    void touchDown(int _touchId);
    void selected(int _touchId);
    void lost(int _touchId);
    void pressed(int _touchId);
    
};

namespace
{
	Node* CreateButton()
	{
        Node* c = (Node*) new kgButton() ;
        c->autorelease();
		return c;
	}
	const bool insertCreateButton = pUINodeFactory.RegisterStrCallBacks("kgButton", CreateButton);
}
#endif /* defined(__takethetreasure__kgButton__) */
