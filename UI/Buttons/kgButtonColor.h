//
//  kgButtonColor.h
//  FairyCraft
//
//  Created by flaber on 20.11.14.
//
//

#ifndef __FairyCraft__kgButtonColor__
#define __FairyCraft__kgButtonColor__

#include "kgUtils.h"
#include "kgUINode.h"
#include "kgButtonInterface.h"

class kgButtonColor : public kgButtonInterface
{
protected:
    
    SpriteFrame* normalFrame;
    SpriteFrame* disabledFrame;
    
    Color3B activeColor;
    
public:
    
    ~kgButtonColor();
    static kgButtonColor* create(string normalImg, string _signal);
    static kgButtonColor* create(string normalImg, string disabled, string _signal);
    
    bool init(string normalImg, string disabled, string _signal);
    
    void setNormalSprite(string s);
    void setDisabledSprite(string s);
    void setActiveColor(Color3B c);
    
    bool init(__Dictionary* data);
    
    void touchDown(int _touchId);
    void selected(int _touchId);
    void lost(int _touchId);
    void pressed(int _touchId);
};

namespace
{
    Node* CreateColorButton()
    {
        Node* c = (Node*) new kgButtonColor() ;
        c->autorelease();
        return c;
    }
    const bool insertCreateColorButton = pUINodeFactory.RegisterStrCallBacks("kgButtonColor", CreateColorButton);
}


#endif /* defined(__FairyCraft__kgButtonColor__) */
