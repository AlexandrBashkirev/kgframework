//
//  kgButtonColor.cpp
//  FairyCraft
//
//  Created by flaber on 20.11.14.
//
//

#include "kgButtonColor.h"

#include "kgView.h"

kgButtonColor* kgButtonColor::create(string normalImg, string _signal)
{
    return kgButtonColor::create( normalImg,  "", _signal);
}
kgButtonColor* kgButtonColor::create(string normalImg, string disabled, string _signal)
{
    kgButtonColor *pRet = new kgButtonColor();
    if (pRet && pRet->init( normalImg, disabled, _signal))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}
kgButtonColor::~kgButtonColor()
{
    
}
bool kgButtonColor::init(__Dictionary* data)
{
    
    kgButtonInterface::init(data);
    
    string normImgName = string(data->valueForKey("normalSprite")->getCString());
    string disabledImgName = string(data->valueForKey("disabledSprite")->getCString());
    string sig = string(data->valueForKey("signal")->getCString());
    
    clickSound = string(data->valueForKey("clickSound")->getCString());
    if(clickSound.length() != 0)
        SimpleAudioEngine::getInstance()->preloadEffect(clickSound.c_str());
    
    init(normImgName, disabledImgName, sig);
    
    if (string(data->valueForKey("activeColor")->getCString()).length() > 0) {
        activeColor = Color3BFromString(data->valueForKey("activeColor")->getCString());
    }
    
    Point pos = PointFromString(data->valueForKey("position")->getCString());
    if(pos.x != 0 || pos.y != 0)
        setPosition(pos);
    
    return true;
}
bool kgButtonColor::init(string normalImg, string disabled, string _signal)
{
    normalFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(normalImg);
    
    if(normalFrame == nullptr)
    {
        normalFrame = SpriteFrame::create(normalImg, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(normalFrame, normalImg);
    }
    
    disabledFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(disabled);
    
    if(disabledFrame == nullptr)
    {
        disabledFrame = SpriteFrame::create(disabled, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(disabledFrame, disabled);
    }
    
    kgUINode<Sprite>::initWithSpriteFrame(normalFrame);
    
    setColor(Color3B(0, 0, 0));
    GLProgram* prog = ShaderCache::getInstance()->getGLProgram("colorTone");
    setGLProgram(prog);
    
    if(notTextureFiltering)
        getTexture()->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE });
    
    signal = _signal;
    
    return true;
}
void kgButtonColor::setNormalSprite(string s)
{
    normalFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(s);
    
    if(normalFrame == nullptr)
    {
        normalFrame = SpriteFrame::create(s, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(normalFrame, s);
    }
    if(isEnabled)
        setTexture(normalFrame->getTexture());
}
void kgButtonColor::setDisabledSprite(string s)
{
    disabledFrame = SpriteFrame::create(s, Rect(0, 0, 1, 1));
    if(!isEnabled)
        setTexture(disabledFrame->getTexture());
}
void kgButtonColor::setActiveColor(Color3B c)
{
    activeColor = c;
}
void kgButtonColor::touchDown(int _touchId)
{
    touchId = _touchId;
    setColor(activeColor);
    
    if(clickSound.length() != 0)
        SimpleAudioEngine::getInstance()->playEffect( clickSound.c_str() );
}
void kgButtonColor::selected(int _touchId)
{
    setColor(activeColor);
}
void kgButtonColor::lost(int _touchId)
{
    setColor(Color3B(0, 0, 0));
}
void kgButtonColor::pressed(int _touchId)
{
    setColor(Color3B(0, 0, 0));
    
    touchDelegateController->callButtonFunc(this, signal);
}