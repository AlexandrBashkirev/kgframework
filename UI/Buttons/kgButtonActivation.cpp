//
//  kgButton.cpp
//  takethetreasure
//
//  Created by flaber on 18.12.13.
//
//

#include "kgButtonActivation.h"
#include "kgView.h"

kgButtonActivation* kgButtonActivation::create(string normalImg, string active, string _signal)
{
    kgButtonActivation *pRet = new kgButtonActivation();
    if (pRet && pRet->init( normalImg, active, _signal))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}
bool kgButtonActivation::init(__Dictionary* data)
{
    kgButtonInterface::init(data);
    
    string normImgName = string(data->valueForKey("normalSprite")->getCString());
    string activeImgName = string(data->valueForKey("activeSprite")->getCString());
    string sig = string(data->valueForKey("signal")->getCString());
    
    clickSound = string(data->valueForKey("clickSound")->getCString());
    if(clickSound.length() != 0)
        SimpleAudioEngine::getInstance()->preloadEffect(clickSound.c_str());
    
    init(normImgName, activeImgName, sig);
    
    Point pos = PointFromString(data->valueForKey("position")->getCString());
    if(pos.x != 0 || pos.y != 0)
        setPosition(pos);

    return true;
}
bool kgButtonActivation::init(string normalImg, string active, string _signal)
{
    
    activeFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(active);
    
    if(activeFrame == nullptr)
    {
        activeFrame = SpriteFrame::create(active, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(activeFrame, active);
    }
    
    normalFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(normalImg);
    
    if(normalFrame == nullptr)
    {
        normalFrame = SpriteFrame::create(normalImg, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(normalFrame, normalImg);
    }
        
    kgUINode<Sprite>::initWithSpriteFrame(normalFrame);
    
    if(notTextureFiltering)
    {
        getTexture()->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE });
    }
    signal = _signal;
    
    return true;
}

void kgButtonActivation::setActive(bool a)
{
    kgButtonInterface::setActive(a);
    setTexture(activeFrame->getTexture());
}
void kgButtonActivation::touchDown(int _touchId)
{
    
        touchDelegateController->callButtonFunc(this, signal);
        
        if(clickSound.length() != 0)
            SimpleAudioEngine::getInstance()->playEffect( clickSound.c_str() );
}
void kgButtonActivation::selected(int _touchId)
{
    
}
void kgButtonActivation::lost(int _touchId)
{

}
void kgButtonActivation::pressed(int _touchId)
{
    
}






