//
//  kgScaleButton.cpp
//  PVO
//
//  Created by flaber on 16.03.14.
//
//

#include "kgScaleButton.h"

#include "kgView.h"

kgScaleButton* kgScaleButton::create(string normalImg, string _signal)
{
    return kgScaleButton::create( normalImg,  "", _signal);
}
kgScaleButton* kgScaleButton::create(string normalImg, string disabled, string _signal)
{
    kgScaleButton *pRet = new kgScaleButton();
    if (pRet && pRet->init( normalImg, disabled, _signal))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}
kgScaleButton::~kgScaleButton()
{
    
}
bool kgScaleButton::init(__Dictionary* data)
{
    normalScaleX = 1;
    normalScaleY = 1;
    activeScale = -0.1f;
    
    kgButtonInterface::init(data);
    
    string normImgName = string(data->valueForKey("normalSprite")->getCString());
    string disabledImgName = string(data->valueForKey("disabledSprite")->getCString());
    string sig = string(data->valueForKey("signal")->getCString());
    
    clickSound = string(data->valueForKey("clickSound")->getCString());
    if(clickSound.length() != 0)
        SimpleAudioEngine::getInstance()->preloadEffect(clickSound.c_str());
    
    init(normImgName, disabledImgName, sig);
    
    if( data->valueForKey("activeScale")->compare("") != 0 )
        activeScale = data->valueForKey("activeScale")->doubleValue();
    
    Point pos = PointFromString(data->valueForKey("position")->getCString());
    if(pos.x != 0 || pos.y != 0)
        setPosition(pos);

    return true;
}
bool kgScaleButton::init(string normalImg, string disabled, string _signal)
{
    normalFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(normalImg);
    
    if(normalFrame == nullptr)
    {
        normalFrame = SpriteFrame::create(normalImg, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(normalFrame, normalImg);
    }
    
    disabledFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(disabled);
    
    if(disabledFrame == nullptr)
    {
        disabledFrame = SpriteFrame::create(disabled, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(disabledFrame, disabled);
    }
    
    kgUINode<Sprite>::initWithSpriteFrame(normalFrame);
    
    if(notTextureFiltering)
        getTexture()->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE });

    signal = _signal;
    
    return true;
}
void kgScaleButton::setNormalSprite(string s)
{
    normalFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(s);
    
    if(normalFrame == nullptr)
    {
        normalFrame = SpriteFrame::create(s, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(normalFrame, s);
    }
    
    if(isEnabled)
        setTexture(normalFrame->getTexture());
}
void kgScaleButton::setDisabledSprite(string s)
{
    disabledFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(s);
    
    if(disabledFrame == nullptr)
    {
        disabledFrame = SpriteFrame::create(s, Rect(0, 0, 1, 1));
        SpriteFrameCache::getInstance()->addSpriteFrame(disabledFrame, s);
    }

    if(!isEnabled)
        setTexture(disabledFrame->getTexture());
}
void kgScaleButton::setActiveScale(float f)
{
    activeScale = f;
}
void kgScaleButton::touchDown(int _touchId)
{
    touchId = _touchId;
    normalScaleX = getScaleX();
    normalScaleY = getScaleY();
    setScaleX(normalScaleX*(1 + activeScale));
    setScaleY(normalScaleY*(1 + activeScale));
    
    if(clickSound.length() != 0)
        SimpleAudioEngine::getInstance()->playEffect( clickSound.c_str() );
}
void kgScaleButton::selected(int _touchId)
{
    normalScaleX = getScaleX();
    normalScaleY = getScaleY();
    setScaleX(normalScaleX*(1 + activeScale));
    setScaleY(normalScaleY*(1 + activeScale));
}
void kgScaleButton::lost(int _touchId)
{
    setScaleX(normalScaleX);
    setScaleY(normalScaleY);
}
void kgScaleButton::pressed(int _touchId)
{
    setScaleX(normalScaleX);
    setScaleY(normalScaleY);

    touchDelegateController->callButtonFunc(this, signal);
}
