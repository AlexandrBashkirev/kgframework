//
//  kgButtonInterface.cpp
//  FairyCraft
//
//  Created by flaber on 25.11.14.
//
//

#include "kgButtonInterface.h"

kgButtonInterface* kgButtonInterface::activeBtn = nullptr;
bool kgButtonInterface::isButtonsActive = true;

void kgButtonInterface::setButtonsEnabled(bool t)
{
    if(t)
    {
        isButtonsActive = true;
    }else
    {
        if(activeBtn)
        {
            activeBtn->lost(activeBtn->getTouchId());
            activeBtn->setActive(false);
            activeBtn = nullptr;
        }
        isButtonsActive = false;
    }
}
bool kgButtonInterface::isActive()
{
    return _isActive;
}
void kgButtonInterface::setActive(bool a)
{
    _isActive = a;
}
bool kgButtonInterface::touchBegan(Point pos, int touchId)
{
    if(getIsEnabled() && !_isActive && isButtonsActive )
        if (getTouchBox().containsPoint(pos))
        {
            touchDown(touchId);
            setActive(true);
            activeBtn = this;
            return true;
        }
    
    return false;
}
void kgButtonInterface::touchMoved(Point pos, int touchId)
{
    if(!isButtonsActive) return;
    if(_isActive)
        if(!getTouchBox().containsPoint(pos) && getTouchId() == touchId)
        {
            lost(touchId);
            setActive(false);
            activeBtn = nullptr;
        }
    
    if(activeBtn == nullptr)
        if(getIsEnabled() && !_isActive)
            if (getTouchBox().containsPoint(pos))
            {
                selected(touchId);
                setActive(true);
                activeBtn = this;
            }
}
void kgButtonInterface::touchEnded(Point pos, int touchId)
{
    if(!isButtonsActive) return;
    if(getTouchId() == touchId && _isActive)
    {
        pressed(touchId);
        setActive(false);
        activeBtn = nullptr;
    }
}
void kgButtonInterface::touchCancelled(Point pos, int touchId)
{
    if(!isButtonsActive) return;
    if(getTouchId() == touchId && _isActive)
    {
        lost(touchId);
        setActive(false);
        activeBtn = nullptr;
    }
}