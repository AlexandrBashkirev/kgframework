//
//  kgView.h
//  takethetreasure
//
//  Created by flaber on 06.12.13.
//
//

#ifndef __takethetreasure__kgView__
#define __takethetreasure__kgView__

#include "kgUtils.h"
#include "kgTouchDelegate.h"
#include "kgUINode.h"

class kgViewController;


class kgView : public kgUINode<Node>, public kgTouchDelegateController
{
    bool isInQueueForPull;
    
    bool _isPause;
    
    kgViewController* viewController;
protected:
    
    bool isAlwaysTouchable;
    bool isDisappearOnsecondLayer;
    bool isAdditionalView;
    friend class kgViewController;
    
public:
    
    static kgView* createFromFile(string filePath);
    static __Dictionary* configFromFile(string filePath);
    
    kgView();
    ~kgView();
    
    bool init(__Dictionary* data);
    
    virtual bool touchBegan(Point pos, int touchId);
    virtual void touchMoved(Point pos, int touchId);
    virtual void touchEnded(Point pos, int touchId);
    virtual void touchCancelled(Point pos, int touchId);
    
    virtual void viewWillDisappear(){};
    virtual void viewWillAppear(){};
    
    virtual void update(float dt){};
    
    virtual void pause(bool p){_isPause = p;};
    
    bool isPause(){return _isPause;};
    
    kgViewController* getViewController();
};


/*namespace
{
	CCNode* CreateView()
	{
		return (CCNode*)( new kgView() );
	}
	const bool insertCreateView = pUINodeFactory.RegisterStrCallBacks("kgView", CreateView);
}*/
#endif /* defined(__takethetreasure__kgView__) */
