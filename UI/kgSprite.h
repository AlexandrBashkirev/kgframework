//
//  kgSprite.h
//  takethetreasure
//
//  Created by flaber on 18.12.13.
//
//

#ifndef __takethetreasure__kgSprite__
#define __takethetreasure__kgSprite__

#include "kgUtils.h"
#include "kgUINode.h"
#include "kgAbstractFactory.h"

class kgSprite : public kgUINode<Sprite>
{
    string spriteName;

    
    //Rect _parentScissorRect;
    //bool _scissorRestored;
public:

    /*static kgSprite* createWithTexture(Texture2D *pTexture);
    static kgSprite* createWithTexture(Texture2D *pTexture, const Rect& rect);
    static kgSprite* create(const char *pszFileName);
    static kgSprite* create(const char *pszFileName, const Rect& rect);
    static kgSprite* createWithSpriteFrame(SpriteFrame *pSpriteFrame);
    static kgSprite* createWithSpriteFrameName(const char *pszSpriteFrameName);*/
    
    ~kgSprite();
    void setScale(float scale);
    bool init(__Dictionary* data);
    bool init(string spriteName);
    
    
    static kgSprite* createWithName(string spriteName);
    
};

namespace
{
	Node* CreateSprite()
	{
        Node* c = (Node*) new kgSprite() ;
        c->autorelease();
		return c;
	}
	const bool insertCreateSprite = pUINodeFactory.RegisterStrCallBacks("kgSprite", CreateSprite);
}
#endif /* defined(__takethetreasure__kgSprite__) */
