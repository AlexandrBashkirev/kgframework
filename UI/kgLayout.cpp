//
//  kgLayout.cpp
//  takethetreasure
//
//  Created by flaber on 24.01.14.
//
//

#include "kgLayout.h"


kgLayout::kgLayout(__Dictionary* d)
{
    relativToScreen = d->valueForKey("relativToScreen")->boolValue();
    
    onlyBigestScale = false;
    if(d->objectForKey("onlyBigestScale") != NULL)
        onlyBigestScale = d->valueForKey("onlyBigestScale")->boolValue();
    
    absHeightScale = d->valueForKey("absHeightScale")->boolValue();
    percentHeightScale = d->valueForKey("percentHeightScale")->boolValue();
    absWidthScale = d->valueForKey("absWidthScale")->boolValue();
    percentWidthScale = d->valueForKey("percentWidthScale")->boolValue();
    width = d->valueForKey("width")->doubleValue();
    height = d->valueForKey("height")->doubleValue();
    
    absPos = d->valueForKey("absPos")->boolValue();
    percentPos = d->valueForKey("percentPos")->boolValue();
    posX = d->valueForKey("posX")->doubleValue();
    posY = d->valueForKey("posY")->doubleValue();
    
    ancher = d->valueForKey("ancher")->boolValue();
    AnchX = d->valueForKey("AnchX")->doubleValue();
    AnchY = d->valueForKey("AnchY")->doubleValue();
    
    noScale = d->valueForKey("noScale")->boolValue();
    
    reflectX = d->valueForKey("reflectX")->boolValue();
    reflectY = d->valueForKey("reflectY")->boolValue();
    
    dontUseTopScale = d->valueForKey("dontUseTopScale")->boolValue();
    
    widthHeightCorelation = 1;
    if(d->objectForKey("widthHeightCorelation") != NULL)
        widthHeightCorelation = d->valueForKey("widthHeightCorelation")->doubleValue();
}
void kgLayout::use(Node* a, Node* parent)
{
    Size parentSize;

    if(parent == NULL || relativToScreen)
        parentSize = screenSize;
    else
        parentSize = parent->getContentSize();
    
    if(ancher)
        a->setAnchorPoint(Point(AnchX, AnchY));

    if(absPos)
        a->setPosition(Point(posX, posY));
    
    if(percentPos)
        a->setPosition(Point(parentSize.width*posX/100.0f, parentSize.height*posY/100.0f));
    
    
    if(noScale)
    {
        if(percentHeightScale && percentWidthScale)
        {
            a->setContentSize(Size(parentSize.width*width/100.0f, parentSize.height*height/100.0f));
        }
        else
        {
            if(percentHeightScale)
                a->setContentSize(Size(parentSize.height*height/100.0f, parentSize.height*height*widthHeightCorelation/100.0f));
            
            if(percentWidthScale)
                a->setContentSize(Size(parentSize.width*width/100.0f, parentSize.width*width*widthHeightCorelation/100.0f));
        }
        
        if(absHeightScale && absWidthScale)
        {
            a->setContentSize(Size(width, height));
        }
        else
        {
            if(absHeightScale)
                a->setContentSize(Size(height, height*widthHeightCorelation));
            
            if(absWidthScale)
                a->setContentSize(Size(width, width*widthHeightCorelation));
        }
    }
    else
    {
        Size s = a->getContentSize();
        
        if(percentHeightScale && percentWidthScale && !onlyBigestScale)
        {
            a->setScaleX(parentSize.width*width/100.0f/s.width * (reflectX ? -1 : 1));
            a->setScaleY(parentSize.height*height/100.0f/s.height * (reflectY ? -1 : 1));
        }
        else
        {
            bool _percentHeightScale = percentHeightScale;
            bool _percentWidthScale = percentWidthScale;
            
            if(onlyBigestScale)
            {
                if(s.width * parentSize.height*height/100.0f/s.height > parentSize.width*width/100.0f)
                {
                    _percentWidthScale = true;
                    _percentHeightScale = false;
                }
                else
                {
                    _percentWidthScale = false;
                    _percentHeightScale = true;
                }
            }
            if(_percentHeightScale)
            {
                a->setScaleX(parentSize.height*height/100.0f/s.height * (reflectX ? -1 : 1));
                a->setScaleY(parentSize.height*height/100.0f/s.height*widthHeightCorelation * (reflectY ? -1 : 1));
            }
            
            if(_percentWidthScale)
            {
                a->setScaleX(parentSize.width*width/100.0f/s.width * (reflectX ? -1 : 1));
                a->setScaleY(parentSize.width*width/100.0f/s.width*widthHeightCorelation * (reflectY ? -1 : 1));
            }
        }
        
        if(absHeightScale && absWidthScale)
        {
            a->setScaleX(width/a->boundingBox().size.width * (reflectX ? -1 : 1));
            a->setScaleY(height/a->boundingBox().size.height * (reflectY ? -1 : 1));
        }
        else
        {
            if(absHeightScale)
            {
                a->setScaleX(height/a->boundingBox().size.height * (reflectX ? -1 : 1));
                a->setScaleY(height/a->boundingBox().size.height*widthHeightCorelation * (reflectY ? -1 : 1));
            }
            
            if(absWidthScale)
            {
                a->setScaleX(width/a->boundingBox().size.width * (reflectX ? -1 : 1));
                a->setScaleY(width/a->boundingBox().size.width*widthHeightCorelation * (reflectY ? -1 : 1));
            }
        }
        
        
        if (dontUseTopScale)
        {
            float sx = 1;
            float sy = 1;
            Node* p = a->getParent();
            
            while (p != nullptr) {
                sx *= p->getScaleX();
                sy *= p->getScaleY();
                p = p->getParent();
            }
            a->setScaleX( a->getScaleX()/sx);
            a->setScaleY( a->getScaleY()/sy);
        }
    }
}