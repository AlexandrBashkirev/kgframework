//
//  kgUINodeViewHolder.h
//  takethetreasure
//
//  Created by flaber on 19.12.13.
//
//

#ifndef takethetreasure_kgUINodeViewHolder_h
#define takethetreasure_kgUINodeViewHolder_h

#include "kgUtils.h"
#include "kgLayout.h"
#include "kgTouchDelegate.h"

//class kgTouchDelegateController;
class kgUINodeViewHolder
{
protected:

    //vector<kgLayout> layouts;
    
    
public:
    
    vector<kgLayout> layouts;
    
    string strTag;
    
    kgTouchDelegateController* touchDelegateController;
    
    virtual ~kgUINodeViewHolder();
    
    virtual void addedToParent(){};
    
    virtual void addButtonToView(Node* btn);
    
    virtual bool init(__Dictionary* data);
    virtual void useLayouts() = 0;
};

#endif
