//
//  kgViewController.cpp
//  takethetreasure
//
//  Created by flaber on 06.12.13.
//
//

#include "kgViewController.h"
#include "kgView.h"

kgViewController::kgViewController()
{
    _isPaused = false;
    scheduleUpdate();
}
kgViewController::~kgViewController()
{
    
}
kgViewController* kgViewController::create()
{
    kgViewController *pRet = new kgViewController();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}
void kgViewController::pause(bool p)
{
    if(_isPaused != p)
    {
        CCLOG("kgViewController::pause %s", p ? "true" : "false");
        for (int i = 0; i < views.size(); i++)
            views[i]->pause(p);
        
        _isPaused = p;
        if(_isPaused)
        {
            pausedTargats = Director::getInstance()->getActionManager()->pauseAllRunningActions();
        }
        else
        {
            Director::getInstance()->getActionManager()->resumeTargets(pausedTargats);
            pausedTargats.clear();
        }
    }
}
bool kgViewController::isPaused()
{
    return _isPaused;
}
void kgViewController::pushView(kgView* v)
{
    v->retain();
    v->isInQueueForPull = false;
    justAddedViews.push_back( v );
    CCLOG("kgViewController::pushView");
}
void kgViewController::pullView(kgView* v)
{
    v->isInQueueForPull = true;
}
void kgViewController::clearViews()
{
    for (int i = 0; i < views.size(); i++)
        views[i]->isInQueueForPull = true;
}
void kgViewController::update(float dt)
{
    if(_isPaused)
        return;
    
    for (int i = 0; i < views.size(); i++)
        if(!views[i]->isPause())
            views[i]->update(dt);
    
    
    auto it = views.begin();
    while (it != views.end())
    {
        kgView* v = *it;
        
        if(v->isInQueueForPull)
        {
            v->viewWillDisappear();
            v->removeFromParentAndCleanup(true);
            views.erase(std::remove(views.begin(), views.end(), v), views.end());
            it = views.begin();
            
            if(views.size() > 0 && justAddedViews.size() == 0 && !v->isAdditionalView)
                if(views[0]->isDisappearOnsecondLayer)
                {
                    views[0]->viewWillAppear();
                    views[0]->setVisible(true);
                }
        }
        else
            it++;
    }
    
    if(justAddedViews.size() > 0)
    {
        auto _justAddedViews = justAddedViews;
        justAddedViews.clear();
        
        auto _it = _justAddedViews.begin();
        while (_it != _justAddedViews.end())
        {
            CCLOG("view added");
            if(views.size() > 0)
                if(views[0]->isDisappearOnsecondLayer)
                {
                    views[0]->viewWillDisappear();
                    views[0]->setVisible(false);
                }
            kgView* v = *_it;
            this->addChild(v, views.size()*200);
            views.insert(views.begin(), v);
            v->viewController = this;
            v->release();
            
            v->viewWillAppear();
            
            _it = _justAddedViews.erase(_it);
        }
    }
}

void kgViewController::touchBegan(Point pos, int touchId)
{
    if(touchId + 1 > currentTouchedViews.size())
        currentTouchedViews.push_back(NULL);
    
    for (int i = 0; i < views.size(); i++) {
        if(views[i]->touchBegan(pos, touchId))
        {
            currentTouchedViews[touchId] = views[i];
            currentTouchedViews[touchId]->retain();
            return;
        }
    }
}
void kgViewController::touchMoved(Point pos, int touchId)
{
    if (currentTouchedViews[touchId] != NULL) {
        currentTouchedViews[touchId]->touchMoved(pos, touchId);
    }
}
void kgViewController::touchEnded(Point pos, int touchId)
{
    if (currentTouchedViews[touchId] != NULL) {
        currentTouchedViews[touchId]->touchEnded(pos, touchId);
        currentTouchedViews[touchId]->release();
    }
    
    currentTouchedViews[touchId] = NULL;
}
void kgViewController::touchCancelled(Point pos, int touchId)
{
    if (currentTouchedViews[touchId] != NULL) {
        currentTouchedViews[touchId]->touchCancelled(pos, touchId);
        currentTouchedViews[touchId]->release();
    }
    
    currentTouchedViews[touchId] = NULL;
}
