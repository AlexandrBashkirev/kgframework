//
//  kgUINode.h
//  takethetreasure
//
//  Created by flaber on 18.12.13.
//
//

#ifndef __takethetreasure__kgUINode__
#define __takethetreasure__kgUINode__

#include "kgUtils.h"
#include "kgLocalize.h"
#include "kgUINodeViewHolder.h"
#include "kgViewController.h"
#include "kgAbstractFactory.h"

typedef Node* (*CreateUINodeCallbacks)();

class kgUINodeFactory : public kgAbstractFactory<CreateUINodeCallbacks>
{
public:
	static kgUINodeFactory& instance()
	{
		static kgUINodeFactory obj;
		return obj;
	}
};
#define pUINodeFactory kgUINodeFactory::instance()



template <class T>
class kgUINode : public T, public kgUINodeViewHolder
{
    string strTag;
    
    bool isClipToBounds;
    bool isRendering;
    
    float left, right, top, bottom;
    
    
protected:
    
    CustomCommand _beforeDrawCommand;
    CustomCommand _afterDrawCommand;
    
    RenderTexture* renderTexture;
    
    void beforeDraw()
    {
        _beforeDrawCommand.init(T::_globalZOrder);
        _beforeDrawCommand.func = CC_CALLBACK_0(kgUINode<T>::onBeforeDraw, this);
        Director::getInstance()->getRenderer()->addCommand(&_beforeDrawCommand);
    }
    
    void onBeforeDraw()
    {
        Rect frame = getBoundingBox();
        auto glview = Director::getInstance()->getOpenGLView();
        
        glEnable(GL_SCISSOR_TEST);
        glview->setScissorInPoints(frame.origin.x + left*frame.size.width,
                                   frame.origin.y + bottom*frame.size.height,
                                   frame.size.width*(right - left),
                                   frame.size.height*(top - bottom));
    }
    
    void afterDraw()
    {
        _afterDrawCommand.init(T::_globalZOrder);
        _afterDrawCommand.func = CC_CALLBACK_0(kgUINode<T>::onAfterDraw, this);
        Director::getInstance()->getRenderer()->addCommand(&_afterDrawCommand);
    }
    
    void onAfterDraw()
    {
        glDisable(GL_SCISSOR_TEST);
    }

    void visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags)
    {
        if (isClipToBounds)
            beforeDraw();
        
        if(renderTexture != nullptr)
            renderTexture->begin();
        
        if(isRendering || renderTexture != nullptr)
            T::visit(renderer, parentTransform, parentFlags);
        
        if (isClipToBounds)
            afterDraw();
        
        if(renderTexture != nullptr)
            renderTexture->end();
    }

protected:
    bool notTextureFiltering;
    bool inited;
    
public:
    
    void setIsRendering(bool _isRendering)
    {
        isRendering = _isRendering;
    }
    void setClipToBounds(bool f)
    {
        isClipToBounds = f;
    }
    void setRect(float _left, float _right, float _top, float _bottom)
    {
        left = _left;
        right = _right;
        top = _top;
        bottom = _bottom;
    }
    void setRenderTexture(RenderTexture* _renderTexture)
    {
        renderTexture = _renderTexture;
    }
    
    kgUINode<T>()
    {
        left = 0;
        right = 1;
        top = 1;
        bottom = 0;
        isRendering = true;
        
        isClipToBounds = false;
        renderTexture = nullptr;
        inited = false;
    }
    static kgUINode<T> * create()
    {
        kgUINode<T> * ret = new (std::nothrow) kgUINode<T>();
        if (ret && ret->init(__Dictionary::create()))
        {
            ret->autorelease();
        }
        else
        {
            CC_SAFE_DELETE(ret);
        }
        return ret;
    }
    
    virtual ~kgUINode(){};
    
    bool init(__Dictionary* data)
    {
        notTextureFiltering = false;
        if(!kgUINodeViewHolder::init(data))
            return false;
        
        T::setAnchorPoint(Point(0.5, 0.5));
        notTextureFiltering = data->valueForKey("notTextureFiltering")->boolValue();
        
        inited = true;
        return true;
    }
    
    virtual void hide(){T::setVisible(false);};
    virtual void show(){T::setVisible(true);};
    virtual bool isShowed(){return T::isVisible();};
    
    
    virtual void usedLayouts(){};
    
    void addChild(Node * child)
    {
        this->addChild(child, child->getZOrder());
    }
    void useLayouts()
    {
        bool isParent = dynamic_cast<kgViewController*>(T::getParent()) == NULL;
        for (int i = 0; i < layouts.size(); i++)
        {
            if(isParent)
                layouts[i].use(this, T::getParent());
            else
                layouts[i].use(this, NULL);
        }
        
        useChildLayouts();
        
        usedLayouts();
    }
    void useChildLayouts()
    {        
        std::vector<Node*>::iterator it = T::getChildren().begin();
        for(int i = 0; i < T::getChildren().size(); i++)
        {
            Node* child = (*it);
            kgUINodeViewHolder* node = dynamic_cast<kgUINodeViewHolder*>(child);
            if(node != NULL)
                node->useLayouts();
            it++;
        }
    }
    void addChild(Node * child, int zOrder)
    {
        addButtonToView(child);
        
        Node::addChild((Node*)child, zOrder);
        
        kgUINodeViewHolder* viewHolder = dynamic_cast<kgUINodeViewHolder*>(child);
        if(viewHolder != NULL)
            viewHolder->touchDelegateController = touchDelegateController ;
        
        kgUINodeViewHolder* uiNode = dynamic_cast<kgUINodeViewHolder*>(child);
        if(uiNode != NULL && inited)
            uiNode->addedToParent();
    }
    Point getCenterGlobalPos()
    {
        Rect rc = getBoundingBox();
        return Point(rc.origin.x + rc.size.width/2, rc.origin.y + rc.size.height/2);
    }
    Rect getBoundingBox() const
    {
        float parentScaleX = 1;
        float parentScaleY = 1;
        
        Node* node = (Node*)this;
        
        while(node != nullptr)
        {
            parentScaleX *= node->getScaleX();
            parentScaleY *= node->getScaleY();
            node = node->getParent();
        };
        
        Point p = Node::convertToWorldSpace(Point(0, 0));
        Point s = Point(Node::getContentSize().width*parentScaleX,
                        Node::getContentSize().height*parentScaleY);
        
        Rect rc = Rect(p.x,// + Node::getContentSize().width*parentScaleX*(0.5 - Node::getAnchorPoint().x),
                       p.y,// + Node::getContentSize().height*parentScaleY*(0.5 - Node::getAnchorPoint().y),
                       s.x,
                       s.y);
        
        return rc;
    }
};

#endif /* defined(__takethetreasure__kgUINode__) */
