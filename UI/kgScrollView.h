//
//  kgScrollView.h
//  FairyCraft
//
//  Created by flaber on 24.11.14.
//
//

#ifndef __FairyCraft__kgScrollView__
#define __FairyCraft__kgScrollView__

#include "kgUtils.h"
#include "kgUI.h"
#include "kgUINode.h"
#include "kgAbstractFactory.h"
#include "kgTouchDelegate.h"
#include "kgSprite.h"

enum ScrollType
{
    HardScroll,
    InertionScroll,
    ToPointScroll
};

class kgScrollViewNode {
    
public:
    int cellId;
    
};

struct ScrollNodeInfo
{
    ScrollNodeInfo(kgScrollViewNode* _sn, Node* _uin)
    {
        sn = _sn;
        uin = _uin;
    }
    kgScrollViewNode* sn;
    Node* uin;
    
    bool operator<(const ScrollNodeInfo &rhs) const
    {
        return sn->cellId < rhs.sn->cellId;
    }
};

class kgScrollView : public kgUINode<Node>, public kgTouchDelegate, public kgTouchDelegateController
{
protected:
    
    bool isClipToBounds;
    
    void visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags);
    
    CustomCommand _beforeDrawCommand;
    CustomCommand _afterDrawCommand;
    
    void beforeDraw();
    void onBeforeDraw();
    void afterDraw();
    void onAfterDraw();
    
    vector<ScrollNodeInfo> activeNodes;
    vector<ScrollNodeInfo> noActivNodes;
    
    Point preTouchPoint;
    Point startTouchPoint;
    Point endDragPos;
    Point maxScroll;
    Point lastV;
    vector<Point> points;
    vector<Point> speeds;
    
    kgSprite* backImg;
    
    float time;
    float lastScrollTime;
    
    float m; // масса
    float k; // коэф трения
    
    kgUINode<Node>* scroll;
    
    bool horizontalScroll;
    bool verticalScroll;
    
    ScrollType scrollType;
    int prevPointId;
    
    void scrollOn(Point delta, float time, bool b = true);
    Point getAvgSpeed();
    Point softScrollBorder;
    
    float animationMovingSpeed;
    
    bool isTouchStarted;
    
    /// moveTo section
    Point targetPos;
    Point startPos;
    float startTime;
    float delay;
    
    bool isTouchScrollEnabled;
    void useNodeInScrollRect(Node* n, Point parentPos, float parentScale);
public:
    
    kgScrollView();
    
    bool init(__Dictionary* data);
    
    virtual bool touchBegan(Point pos, int touchId);
    virtual void touchMoved(Point pos, int touchId);
    virtual void touchEnded(Point pos, int touchId);
    virtual void touchCancelled(Point pos, int touchId);
    
    Rect getTouchBox(){return boundingBox();};
    void addChild(Node * child, int zOrder);
    
    virtual void willDisplayCell(int i, Node* n){};
    virtual int cellCount(){return 0;};
    virtual Point getCellSize(int i){return Point();};
    virtual Node* createCell(){return nullptr;};
    
    void refillDisplaedCell();
    
    ScrollType getScrollTypeFromStr(string type);
    
    void setTouchScrollEnabled(bool f);
    void update(float t);
    void updateMaxScroll();
    
    void usedLayouts();
    
    void setPoints(vector<Point> _points);
    
    int getNearPointId();
    void setToPoint(int);
    void scrollToPoint(int);
    void scrollToPos(Point);
    void setToPos(Point);
    
};

namespace
{
    Node* CreateScrollView()
    {
        Node* c = (Node*) new kgScrollView() ;
        c->autorelease();
        return c;
    }
    const bool insertCreateScrollView = pUINodeFactory.RegisterStrCallBacks("kgScrollView", CreateScrollView);
}

#endif /* defined(__FairyCraft__kgScrollView__) */
