//
//  kgUINode.cpp
//  takethetreasure
//
//  Created by flaber on 18.12.13.
//
//

#include "kgUINodeViewHolder.h"
#include "kgView.h"
#include "kgTouchDelegate.h"



void kgUINodeViewHolder::addButtonToView(Node* child)
{
    kgTouchDelegate* btn = dynamic_cast<kgTouchDelegate*>(child);
    if(btn != nullptr && touchDelegateController != nullptr)
        touchDelegateController->addTouchable(btn);
}

bool kgUINodeViewHolder::init(__Dictionary* data)
{
    __Array* objects = (__Array*)data->objectForKey("objects");
    if(objects != NULL)
    {
        for (int i = 0; i < objects->count(); i++)
        {
            __Dictionary* d = (__Dictionary*)objects->getObjectAtIndex(i);
            string type = string( d->valueForKey("type")->getCString() );
            
            CreateUINodeCallbacks creater = pUINodeFactory.GetStrCallBacks(type);
            
            Node* go = (creater)();
            
            Node* _this = dynamic_cast<Node*>(this);
            _this->addChild(go, 10);
            
            int tag = d->valueForKey("tag")->intValue();
            if(tag != 0)
                go->setTag(tag);
            
            strTag = string(d->valueForKey("strTag")->getCString());
            if(strTag.length() > 0)
                touchDelegateController->registerTag(strTag, go);
            
            kgUINodeViewHolder* _go = dynamic_cast<kgUINodeViewHolder*>(go);
            _go->init(d);
            
            kgUINodeViewHolder* uiNode = dynamic_cast<kgUINodeViewHolder*>(go);
            if(uiNode != NULL)
                uiNode->addedToParent();
        }
        if(objects->count() == 0 && dynamic_cast<kgView*>(this) != nullptr)
            log("view whith zero objects");
    }
    else if(dynamic_cast<kgView*>(this) != nullptr)
        log("view whith out objects");
    
    __Array* _layouts = (__Array*)data->objectForKey("layouts");
    if(_layouts != NULL)
    {
        for (int i = 0; i < _layouts->count(); i++)
        {
            __Dictionary* d = (__Dictionary*)_layouts->getObjectAtIndex(i);
            layouts.push_back(kgLayout(d));
        }
    }
    return true;
}
kgUINodeViewHolder::~kgUINodeViewHolder()
{
    kgTouchDelegate* btn = dynamic_cast<kgTouchDelegate*>(this);
    if(btn != NULL)
        touchDelegateController->removeTouchable(btn);
    
    /*if(strTag.length() > 0)
        view->unregisterTag(strTag);*/
}


