//
//  kgProgressBar.cpp
//  FairyCraft
//
//  Created by flaber on 08.11.14.
//
//

#include "kgProgressBar.h"

kgProgressBar::~kgProgressBar()
{
    
}

bool kgProgressBar::init(__Dictionary* data)
{
    if(!kgUINode<Sprite>::init(data))
        return false;
    
    backImgName = string(data->valueForKey("backImgName")->getCString());
    fillerImgName = string(data->valueForKey("fillerImgName")->getCString());
    
    if(!kgProgressBar::init(backImgName, fillerImgName))
        return false;
    
    Point pos = PointFromString(data->valueForKey("position")->getCString());
    if(pos.x != 0 || pos.y != 0)
        setPosition(pos);
    
    if(notTextureFiltering)
        getTexture()->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE });

    return true;
}

bool kgProgressBar::init(string _backImgName, string _fillerImgName)
{
    backImgName = _backImgName;
    
    SpriteFrame *pFrameBack = SpriteFrameCache::getInstance()->getSpriteFrameByName(backImgName);
    
    if(pFrameBack == nullptr)
        initWithFile(backImgName);
    else
        initWithSpriteFrame(pFrameBack);
    
    fillerImgName = _fillerImgName;
    
    filler = kgSprite::createWithName(fillerImgName);
    filler->setPosition(Point(getContentSize().width/2, getContentSize().height/2));
    filler->setClipToBounds(true);
    filler->setRect(0, 0, 1, 0);
    addChild(filler);
    
    return true;
}

void kgProgressBar::setProgress(int p)
{
    if(p>100)p=100;
    if(p<0)p=0;
    
    filler->setRect(0, p/100.f, 1, 0);
}