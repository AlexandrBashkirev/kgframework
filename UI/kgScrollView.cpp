//
//  kgScrollView.cpp
//  FairyCraft
//
//  Created by flaber on 24.11.14.
//
//

#include "kgScrollView.h"

kgScrollView::kgScrollView()
{
    scroll = NULL;
}
bool kgScrollView::init(__Dictionary* data)
{
    scroll = kgUINode<Node>::create();
    if(scroll == nullptr)
        return false;
    scroll->setAnchorPoint(Point(0,0));
    
    
    kgUINode<Node>::addChild(scroll, 1);
    
    scroll->touchDelegateController = this ;
    if(!kgUINode<Node>::init(data))
        return false;
    
    scroll->setScaleX(1/getScaleX());
    scroll->setScaleY(1/getScaleY());
    
    string backImgName = string(data->valueForKey("backImgName")->getCString());

    backImg = kgSprite::createWithName(backImgName);
    backImg->setAnchorPoint(Point(0,0));
    kgUINode<Node>::addChild(backImg, 0);
    
    setContentSize(backImg->getContentSize());
    
    scrollType = kgScrollView::getScrollTypeFromStr(string(data->valueForKey("scrollType")->getCString()));
    verticalScroll = data->valueForKey("verticalScroll")->boolValue();
    horizontalScroll = data->valueForKey("horizontalScroll")->boolValue();
    isClipToBounds = data->valueForKey("isClipToBounds")->boolValue();
    
    softScrollBorder = Point(screenSize.width*0.1f, screenSize.height*0.1f);
    isTouchStarted = false;
    isTouchScrollEnabled = true;
    
    startTime = -1;
    m = 10;
    k = 20.0f;
    time = 0;
    startTime = 0;
    delay = 0;
    animationMovingSpeed = screenSize.width*1.2f;

    scheduleUpdate();
    
    return true;
}
void kgScrollView::setTouchScrollEnabled(bool f)
{
    isTouchScrollEnabled = f;
}
void kgScrollView::usedLayouts()
{
    if(cellCount() > 0)
    {
        if(verticalScroll)
        {
            float t = 0;
            int i = activeNodes.size();
            do
            {
                t += getCellSize(i).y;
                
                
                Node* n = createCell();
                kgScrollViewNode* __n = dynamic_cast<kgScrollViewNode*>(n);
                __n->cellId = i;
                n->setPosition(Point(getContentSize().width*0.5f,
                                     getContentSize().height - (0.5f + i)*getCellSize(i).y));
                scroll->addChild(n, 2 );
                
                activeNodes.push_back(ScrollNodeInfo(__n, n));
                
                willDisplayCell(i, n);
                
                i++;

            }while(t < getContentSize().height && i < cellCount());
            
            updateMaxScroll();
        }
        else if (horizontalScroll)
        {
            
        }
    }
}
void kgScrollView::addChild(Node * child, int zOrder)
{
    scroll->addChild(child, zOrder);
    
    Point tmpMaxScroll = Point(0,0);
    
    tmpMaxScroll.x = child->getPositionX() + child->getContentSize().width/2;
    tmpMaxScroll.y = child->getPositionY() + child->getContentSize().height/2;
    
    if(tmpMaxScroll.x > maxScroll.x) maxScroll.x = tmpMaxScroll.x;
    if(tmpMaxScroll.y > maxScroll.y) maxScroll.y = tmpMaxScroll.y;
}
void kgScrollView::useNodeInScrollRect(Node* n, Point parentPos, float parentScale)
{
    Point tmpMaxScroll = Point(0,0);
    
    tmpMaxScroll.x = parentPos.x + n->getPositionX()*parentScale + n->getContentSize().width/2;
    tmpMaxScroll.y = parentPos.y + n->getPositionY()*parentScale + n->getContentSize().height/2;
    
    if(tmpMaxScroll.x > maxScroll.x)
        maxScroll.x = tmpMaxScroll.x;
    if(tmpMaxScroll.y > maxScroll.y)
        maxScroll.y = tmpMaxScroll.y;
}
void kgScrollView::updateMaxScroll()
{
    maxScroll.x = 0;
    maxScroll.y = 0;
    
    if(cellCount() == 0)
    {
        Vector<Node*>::iterator it = scroll->getChildren().begin();
        while (it != scroll->getChildren().end())
        {
            Node* n = (*it);
            
            useNodeInScrollRect(n, Point(0,0), 1);
            
            Vector<Node*>::iterator it2 = n->getChildren().begin();
            while (it2 != n->getChildren().end())
            {
                Node* n2 = (*it2);
                useNodeInScrollRect(n2, n->getPosition(), n->getScale());
                it2++;
            }
            
            it++;
        }
    }else
    {
        for (int i = 0; i < cellCount(); i++)
        {
            if(verticalScroll)
            {
                maxScroll.y += getCellSize(i).y;
            }
            else
            {
                maxScroll.y = getCellSize(i).y;
            }
            
            if(horizontalScroll)
            {
                maxScroll.x += getCellSize(i).x;
            }
            else
            {
                maxScroll.x = getCellSize(i).x;
            }
        }
    }
}
bool kgScrollView::touchBegan(Point pos, int touchId)
{
    Rect rc = getTouchBox();
    if (rc.containsPoint(pos))
    {
        preTouchPoint = pos;
        startTouchPoint = pos;
        
        for (int i = 0; i < buttons.size(); i++)
            if( pButtonTouchValidate->validation(buttons[i]) )
               buttons[i]->touchBegan(pos, touchId);
        
        return true;
    }
    return false;
}
void kgScrollView::touchMoved(Point pos, int touchId)
{
    for (int i = 0; i < buttons.size(); i++)
        if( pButtonTouchValidate->validation(buttons[i]) )
            buttons[i]->touchMoved(pos, touchId);
    
    if (!getTouchBox().containsPoint(pos) || !isTouchScrollEnabled) return;
    
        
    Point deltaPoint = pos - preTouchPoint;
    deltaPoint.x /= getScaleX();
    deltaPoint.y /= getScaleY();
    
    
    if((startTouchPoint - pos).length() > 20)
    {
        isTouchStarted = true;
        // отключаем кнопки
        kgButtonInterface::setButtonsEnabled(false);
    }
    scrollOn(deltaPoint, time - lastScrollTime, false);
    
    preTouchPoint = pos;
    lastScrollTime = time;
}
void kgScrollView::touchEnded(Point pos, int touchId)
{
    for (int i = 0; i < buttons.size(); i++)
        buttons[i]->touchEnded(pos, touchId);
    
    if(!isTouchStarted || !isTouchScrollEnabled) return;
    
    endDragPos = scroll->getPosition();
    isTouchStarted = false;
    //активируем кнопки
    kgButtonInterface::setButtonsEnabled(true);
    
    lastV = getAvgSpeed();
    
    if(scrollType == ToPointScroll)
    {
        int nearestPoint = prevPointId;
        
        if( horizontalScroll)
        {
            if(fabs(lastV.x) > 100)
            {
                if(nearestPoint < points.size()-1 && lastV.x < 0)
                    nearestPoint ++;
                if(nearestPoint > 0 && lastV.x > 0)
                    nearestPoint --;
            }
            else
                nearestPoint = getNearPointId();
        }
        
        scrollToPoint(nearestPoint);
    }
}
void kgScrollView::touchCancelled(Point pos, int touchId)
{
    endDragPos = scroll->getPosition();
    isTouchStarted = false;
    kgButtonInterface::setButtonsEnabled(true);
    //активируем кнопки
    
    /*if(scrollType == ToPointScroll)
    {
        Point point = points[prevPointId];
        Point target = Point(point.x, point.y);//Point(-(point.x - rect.size.width/2), (point.y - rect.size.height/2));
        scroll->setPosition(target);
    }
    else{
        if(horizontalScroll)
        {
            if(contentHolder.position.x > 0)
                contentHolder.position = ccp(0, contentHolder.position.y);
            if(fabs(contentHolder.position.x) > maxScroll.x - rect.size.width)
                contentHolder.position = ccp(-(maxScroll.x - rect.size.width), contentHolder.position.y);
        }
        if(verticalScroll)
        {
            if(contentHolder.position.y < 0)
                contentHolder.position = ccp(contentHolder.position.x, 0);
            if(contentHolder.position.y > maxScroll.y - rect.size.height)
                contentHolder.position = ccp(contentHolder.position.x, maxScroll.y - rect.size.height);
        }
    }*/
}
int kgScrollView::getNearPointId()
{
    int nearestPoint = 0;
    float dist = 100500;
    Point point;
    
    Point p = Point(scroll->getPositionX(), scroll->getPositionY());
    
    for (int i = 0; i < points.size(); i++)
    {
        point = points[i];
        
        float _dist = p.getDistance( point);
        
        if(dist > _dist)
        {
            dist = _dist;
            nearestPoint = i;
        }
    }
    
    return nearestPoint;
}
void kgScrollView::setToPos(Point p)
{
    if(scroll != nullptr)
        scroll->setPosition(p);
}
void kgScrollView::setToPoint(int pointid)
{
    Point point = points[pointid];
    Point target = Point(-(point.x - getBoundingBox().size.width/2), (point.y - getBoundingBox().size.height/2));
    scroll->setPosition(target);
    
    prevPointId = pointid;
}
void kgScrollView::scrollToPoint(int pointid)
{
    if(points.size() < pointid+1 || pointid < 0) return;
    
    Point point = points[pointid];

    scrollToPos(Point(point.x, point.y));
    
    prevPointId = pointid;
}
void kgScrollView::scrollToPos(Point point)
{
    targetPos = point;
    startPos = scroll->getPosition();
    lastV = Point(0,0);
    
    float dist = startPos.distance( targetPos );
    startTime = time;
    delay = (dist)*0.5/animationMovingSpeed;
}
Point kgScrollView::getAvgSpeed()
{
    Point avgSpeed = Point(0,0);
    
    for (int i = 0; i < speeds.size(); i++)
    {
        avgSpeed.x += speeds[i].x;
        avgSpeed.y += speeds[i].y;
    }
    float t = 1 - (time - lastScrollTime)/0.5f;
    if(t < 0) t = 0;
    avgSpeed.x *= t/speeds.size();
    avgSpeed.y *= t/speeds.size();
    
    
    if (fabs(avgSpeed.x) <= 5 && fabs(avgSpeed.y) <= 5)
    {
        t = 0;
        speeds.clear();
    }
    
    return avgSpeed;
}
void kgScrollView::scrollOn(Point deltaPoint, float dt, bool b)
{
    if(!verticalScroll)
        deltaPoint.y = 0;
    else if(!horizontalScroll)
        deltaPoint.x = 0;
    
    Point newPos = scroll->getPosition() + deltaPoint;
    
    if(scrollType == HardScroll)
    {
        if(horizontalScroll)
        {
            if(newPos.x > 0)
                newPos.x = 0;
            if(fabs(newPos.x) > maxScroll.x - getBoundingBox().size.width/getScaleX())
                newPos.x = -(maxScroll.x - getBoundingBox().size.width/getScaleX());
        }
        if(verticalScroll)
        {
            if(newPos.y < 0)
                newPos.y = 0;
            if(newPos.y > maxScroll.y - getBoundingBox().size.height/getScaleY())
                newPos.y = maxScroll.y - getBoundingBox().size.height/getScaleY();
        }
    }
    else
    {
        if(dt > 0)
        {
            Point speed = Point(deltaPoint.x/dt, deltaPoint.y/dt);
            speeds.push_back(speed);
            if (speeds.size() > 5)
                speeds.erase(speeds.begin());
        }
        
        if(horizontalScroll)
        {
            
            if(newPos.x > 0)
            {
                float t = 1 - newPos.x/softScrollBorder.x;
                if (t<0) t = 0;
                newPos.x = scroll->getPositionX() + deltaPoint.x*t;
            }
            if(fabs(newPos.x) > maxScroll.x - getBoundingBox().size.width/getScaleX() &&
               maxScroll.x > getBoundingBox().size.width/getScaleX())
            {
                float t = 1 - (fabs(newPos.x) - (maxScroll.x - getBoundingBox().size.width/getScaleX()))/softScrollBorder.x;
                if (t<0) t = 0;
                newPos.x = scroll->getPositionX() + deltaPoint.x*t;
            }
            if(maxScroll.x < getBoundingBox().size.width/getScaleX())
                newPos.x = 0;
            
        }
        if(verticalScroll)
        {
            if(newPos.y < 0)
            {
                float t = 1 + newPos.y/softScrollBorder.y;
                if (t<0) t = 0;
                newPos.y = scroll->getPositionY() + deltaPoint.y*t;
            }
            if(newPos.y > maxScroll.y - getBoundingBox().size.height/getScaleY() && maxScroll.y > getBoundingBox().size.height/getScaleY())
            {
                float t = 1 - (newPos.y - (maxScroll.y - getBoundingBox().size.height/getScaleY()))/softScrollBorder.y;
                if (t<0) t = 0;
                newPos.y = scroll->getPositionY() + deltaPoint.y*t;
            }
            
            if(maxScroll.y < getBoundingBox().size.height/getScaleY())
                newPos.y = 0;
        }
    }
    
    if(b)
    {
        scroll->stopAllActions();
        scroll->runAction(MoveTo::create(dt, newPos));
    }
    else
        scroll->setPosition(newPos);
}
void kgScrollView::refillDisplaedCell()
{
    for (int i = 0; i < activeNodes.size(); i++)
        willDisplayCell(activeNodes[i].sn->cellId, activeNodes[i].uin);
}
void kgScrollView::update(float delta)
{
    time += delta;
    //if(isTouchStarted) return;
    
    updateMaxScroll();
    
    Point F = Point(0,0);
    float _k = k;
    float t;
    float speedCoef = 1;
    
    Point realMaxScroll = Point(maxScroll.x - getBoundingBox().size.width/getScaleX(), maxScroll.y - getBoundingBox().size.height/getScaleY());
    Point newPos = scroll->getPosition();
    
    if( time < startTime + delay)
    {
        newPos = Point(startPos.x + (targetPos.x - startPos.x)*(time - startTime)/delay,
                             startPos.y + (targetPos.y - startPos.y)*(time - startTime)/delay);
    }
    else if(startTime != -1){
        newPos = targetPos;
        startTime = -1;
    }
    else
        switch (scrollType)
        {
            case InertionScroll:
            {
                if(horizontalScroll)
                {
                    if(scroll->getPositionX() > 0)
                    {
                        t = scroll->getPositionX()/softScrollBorder.x;
                        F = Point(-100000*t, 0);
                        
                        speedCoef = scroll->getPositionX()/50;
                        
                        _k = k + 100*t;
                    }
                    
                    if(fabs(scroll->getPositionX()) > maxScroll.x - getBoundingBox().size.width/getScaleX() &&
                       maxScroll.x > getBoundingBox().size.width/getScaleX())
                    {
                        t = (fabs(scroll->getPositionX()) - (maxScroll.x - getBoundingBox().size.width/getScaleX()))/softScrollBorder.x;
                        
                        speedCoef = (fabs(scroll->getPositionX()) - (maxScroll.x - getBoundingBox().size.width/getScaleX()))/50;
                        
                        F = Point(100000*t, 0);
                        _k = k + 100*t;
                    }
                }
                if(verticalScroll)
                {
                    if(scroll->getPositionY() < 0 || maxScroll.y < getBoundingBox().size.height/getScaleY())
                    {
                        t = -scroll->getPositionY()/softScrollBorder.y;
                        F = Point(0, 100000*t);
                        
                        speedCoef = -scroll->getPositionY()/50;
                        
                        _k = k + 100*t;
                    }
                    
                    if(scroll->getPositionY() > maxScroll.y - getBoundingBox().size.height/getScaleY() &&
                       maxScroll.y > getBoundingBox().size.height/getScaleY())
                    {
                        t = (scroll->getPositionY() - fabs(maxScroll.y - getBoundingBox().size.height/getScaleY()))/softScrollBorder.y;
                        
                        speedCoef = (fabs(scroll->getPositionY()) - (maxScroll.y - getBoundingBox().size.height/getScaleY()))/50;
                        
                        F = Point(0, -100000*t);
                        _k = k + 100*t;
                    }
                }
                
                if(speedCoef > 1) speedCoef = 1;
                
                
                F = Point(F.x - lastV.x*_k, F.y - lastV.y*_k);
                Point newV = Point(lastV.x + F.x*delta/m, lastV.y + F.y*delta/m);
                if(newV.length() > 1)
                {
                    newPos = Point(scroll->getPositionX() + newV.x*delta*speedCoef,
                                              scroll->getPositionY() + newV.y*delta*speedCoef);
                    lastV = newV;
                }
                
                break;
            }
                
            default :
                break;
        }
    
    if(fabs(newPos.x) > realMaxScroll.x)
        newPos.x = -(realMaxScroll.x);
    
    scroll->setPosition(newPos);
    
    if(cellCount() > 0)
    {
        /*auto it = activeNodes.begin();

        while (it != activeNodes.end())
        {
            Rect rc = it->uin->getBoundingBox();
            if(!getBoundingBox().intersectsRect(rc))
            {
                it->uin->setVisible(false);
                noActivNodes.push_back((*it));
                it = activeNodes.erase(it);
            }
            else
                it++;
        }*/
        
        if(activeNodes.size() == 0) return;
        
        std::sort(activeNodes.begin(), activeNodes.end());
        
        if(verticalScroll)
        {
            int i = -1;
            float topPos = activeNodes[0].uin->getPositionY() + 0.5f*getCellSize(activeNodes[0].sn->cellId).y - boundingBox().size.height;
            float realPos = scroll->getPositionY();
            if( -topPos > realPos && activeNodes[0].sn->cellId > 0)
            {
                i = activeNodes[0].sn->cellId - 1;
            }
            else if(-topPos < realPos - getCellSize(activeNodes[0].sn->cellId).y )
            {
                activeNodes[0].uin->setVisible(false);
                noActivNodes.push_back(activeNodes[0]);
                activeNodes.erase(activeNodes.begin());
            }
            
            float bottomPos = activeNodes[activeNodes.size()-1].uin->getPositionY() - 0.5f*getCellSize(activeNodes[activeNodes.size()-1].sn->cellId).y - boundingBox().size.height;
            
            realPos = scroll->getPositionY() + boundingBox().size.height;
            if( -bottomPos < realPos && activeNodes[activeNodes.size()-1].sn->cellId < cellCount() - 1)
            {
                i = activeNodes[activeNodes.size()-1].sn->cellId + 1;
            }
            else if(-bottomPos > realPos + getCellSize(activeNodes[activeNodes.size()-1].sn->cellId).y )
            {
                activeNodes[activeNodes.size()-1].uin->setVisible(false);
                noActivNodes.push_back(activeNodes[activeNodes.size()-1]);
                activeNodes.erase(activeNodes.end()-1);
            }
            
            if(i > -1)
            {
                Node* n;
                if(noActivNodes.size() == 0)
                {
                    n = createCell();
                    scroll->addChild(n, 2 );
                }
                else
                {
                    n = noActivNodes[0].uin;
                    n->setVisible(true);
                    noActivNodes.erase(noActivNodes.begin());
                }

                kgScrollViewNode* __n = dynamic_cast<kgScrollViewNode*>(n);
                __n->cellId = i;

                activeNodes.push_back(ScrollNodeInfo(__n, n));
                
                n->setPosition(Point(boundingBox().size.width*0.5f,
                                boundingBox().size.height - (0.5f + i)*getCellSize(i).y));
                willDisplayCell(i, n);
            }
        }
        else
        {
            
        }
    }
}
void kgScrollView::setPoints(vector<Point> _points)
{
    points = _points;
}
ScrollType kgScrollView::getScrollTypeFromStr(string typeStr)
{
    if (typeStr.compare("HardScroll") == 0)
        return HardScroll;
    else if(typeStr.compare("InertionScroll") == 0)
        return InertionScroll;
    else if(typeStr.compare("ToPointScroll") == 0)
        return ToPointScroll;
    
    return HardScroll;
}
void kgScrollView::visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags)
{
    if (isClipToBounds)
        beforeDraw();
    
    Node::visit(renderer, parentTransform, parentFlags);
    
    if (isClipToBounds)
        afterDraw();
}
void kgScrollView::beforeDraw()
{
    _beforeDrawCommand.init(_globalZOrder);
    _beforeDrawCommand.func = CC_CALLBACK_0(kgScrollView::onBeforeDraw, this);
    Director::getInstance()->getRenderer()->addCommand(&_beforeDrawCommand);
}
void kgScrollView::onBeforeDraw()
{
    Rect frame = getBoundingBox();
    auto glview = Director::getInstance()->getOpenGLView();
    
    glEnable(GL_SCISSOR_TEST);
    glview->setScissorInPoints(frame.origin.x,
                               frame.origin.y,
                               frame.size.width,
                               frame.size.height);
}
void kgScrollView::afterDraw()
{
    _afterDrawCommand.init(_globalZOrder);
    _afterDrawCommand.func = CC_CALLBACK_0(kgScrollView::onAfterDraw, this);
    Director::getInstance()->getRenderer()->addCommand(&_afterDrawCommand);
}
void kgScrollView::onAfterDraw()
{
    glDisable(GL_SCISSOR_TEST);
}